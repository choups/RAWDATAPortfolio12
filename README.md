## Project configuration
- Copy the `config.json.example` file and name it `config.json`
- IMPORTANT : in the project, right click on the `config.json` > Properties and set it as an EmbeddedResource
- Leave the initial values to fetch the group 12 remote database or replace the values to let the DataService connect to your own database

## Testing
### WebService
- Run the Webservice project first
- Run the tests in command line with `dotnet test WebServiceTests`

### DataService
- Run the tests in command line with `dotnet test DataServiceTests`

## Version control
- Let's create one branch per trello ticket to avoid merging directly into master.

- For example, with one ticket, you create a new branch and switch to it while you're working on the ticket.
- When you're done. You push all your commits to the remote repository. Then you go to GitLab repo web interface.
- Click on `Merge Requests` > `New Merge Request`. You select your branch as a source branch and the master (or desired) branch as a target branch.
- Then you fill on the form as you want and publish the merge request.

- When the merge request has been reviewed and approved by another teammate, you can directly merge it into the target branch by clicking on the `Merge` button.

- More details [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#from-the-merge-request-list)

### Branch managing commands
- `git branch <branch-name>` to create a new branch
- `git checkout <branch-name>` to switch to the specified branch
- pro-tip: `git checkout -b <branch-name>` to create a branch AND switch to it

- More details on [Git Documentation](https://git-scm.com/doc)
