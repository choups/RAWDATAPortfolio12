﻿using System;
using Xunit;
using DataService;

namespace Tests.DataService {

    public class ContextTests {

        [Fact]
        public void Test_DatabaseConnection() {
            Assert.True(new DataContext().Database.CanConnect());
        }
    }
}
