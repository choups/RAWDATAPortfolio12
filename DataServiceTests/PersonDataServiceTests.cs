﻿using System;
using Xunit;
using DataService;
using System.Linq;

namespace Tests.DataService {

    public class PersonDataServiceTests {
        private readonly ImdbDataService DataService = new();

        [Fact]
        public void Test_GetPersons() {
            var (persons, count) = DataService.GetPagedPersons(1, 25);
            Assert.NotNull(persons);
            Assert.Equal(234484, count);
        }

        [Fact]
        public void Test_GetPerson() {
            var person = DataService.GetPerson("nm0000093");
            Assert.NotNull(person);
            Assert.Equal("Pitt", person.LastName);
            Assert.Equal("Brad", person.FirstName);
            Assert.Equal("1963", person.BirthYear);
        }

        [Fact]
        public void Test_GetWorks() {
            var person = DataService.GetPerson("nm0000093");
            Assert.NotNull(person);
            Assert.NotNull(person.Works);
            Assert.Equal(8, person.Works.Count);
        }

        [Fact]
        public void Test_GetActings() {
            var person = DataService.GetPerson("nm0000093");
            Assert.NotNull(person);
            Assert.NotNull(person.Works);

            var actors = person.Works
                .Where(x => x.Acting != null)
                .Select(x => x.Acting)
                .ToList();

            Assert.Equal(7, actors.Count);
        }

        [Fact]
        public void Test_GetDeath() {
            var person = DataService.GetPerson("nm0113225");
            Assert.NotNull(person);
            Assert.NotNull(person.Death);
            Assert.Equal("2004", person.Death.DeathYear);
        }
    }
}
