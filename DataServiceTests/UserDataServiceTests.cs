﻿using System;
using Xunit;
using DataService;
using System.Collections.Generic;
using System.Linq;

namespace Tests.DataService {

    public class UserDataServiceTests {
        private readonly UserDataService DataService = new();
        private readonly static int UserId = 1;


        private static string GenerateRandomAlphanumericString(int length = 10) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var random = new Random();
            var randomString = new string(Enumerable.Repeat(chars, length)
                                                    .Select(s => s[random.Next(s.Length)]).ToArray());
            return randomString;
        }

        // User Tests
        // This test is 4-in-1 because it's a flow: we create then update then delete the same user

        [Fact]
        public void Test_Add_Then_Delete_User() {

            /**** Create ****/
            // When
            var newUser = DataService.AddUser("azerty", "azertyuiop", "azertyuiopqsdfghjklmwxcvbn");
            var user = DataService.GetUser(newUser.UserId);

            // Then
            Assert.NotNull(newUser);
            Assert.NotNull(user);
            Assert.Equal(newUser.UserId, user.UserId);
            Assert.Equal(newUser.Login, user.Login);
            Assert.Equal(newUser.Password, user.Password);

            /**** Update Login & Password ****/
            // Given
            var newLogin = "qwerty";
            var oldPassword = user.Password;
            var newPassword = GenerateRandomAlphanumericString();

            // When
            DataService.UpdateLogin(user.UserId, newLogin);
            DataService.UpdatePassword(user.UserId, newPassword, user.Salt);

            // Then
            var changedUser = DataService.GetUser(user.UserId);
            Assert.NotNull(changedUser);
            Assert.Equal(newLogin, changedUser.Login);
            Assert.NotEqual(oldPassword, changedUser.Password);


            /**** Delete ****/
            Assert.True(DataService.DeleteUser(user.UserId));
            Assert.Null(DataService.GetUser(user.UserId));
        }


        // Bookmarks Tests

        [Fact]
        public void Test_Add_Then_Delete_TitleBookmark() {
            /**** Create ****/

            // Given
            var titles = new List<string>() {
                "tt8115528", "tt7632970", "tt7365576", "tt7746984", "tt9700778"
            };

            // When
            foreach (string title in titles)
                DataService.AddTitleBookmark(UserId, title, $"myBookmark{UserId}{title}");

            // Then
            var (titleBookmarks, _) = DataService.GetTitleBookmarks(UserId, 0, 10);
            Assert.NotNull(titleBookmarks);
            Assert.Equal(5, titleBookmarks.Count);

            var bookmark = DataService.GetTitleBookmark(UserId, titles[0]);
            Assert.NotNull(bookmark);
            Assert.Equal($"myBookmark{UserId}{titles[0]}", bookmark.Label);
            Assert.NotEqual(default, bookmark.Date);

            /***** Delete *****/

            // When
            var found = titles
                .Select(title => DataService.DeleteTitleBookmark(UserId, title))
                .Aggregate((previous, current) => previous && current);

            // Then
            Assert.True(found);

            foreach (string title in titles)
                Assert.Null(DataService.GetTitleBookmark(UserId, title));
        }

        [Fact]
        public void Test_Add_Then_Delete_PersonBookmark() {
            /**** Create ****/

            // Given
            var persons = new List<string>() {
                "nm0000035", "nm0000041", "nm0000047", "nm0000056", "nm0000077"
            };

            // When
            foreach (string person in persons)
                DataService.AddPersonBookmark(UserId, person, $"myBookmark{UserId}{person}");

            // Then
            var (titleBookmarks, _) = DataService.GetPersonBookmarks(UserId, 0, 10);
            Assert.Equal(5, titleBookmarks.Count);

            var bookmark = DataService.GetPersonBookmark(UserId, persons[0]);
            Assert.NotNull(bookmark);
            Assert.Equal($"myBookmark{UserId}{persons[0]}", bookmark.Label);
            Assert.NotEqual(default, bookmark.Date);


            /***** Delete *****/

            // When
            var found = persons
                .Select(person => DataService.DeletePersonBookmark(UserId, person))
                .Aggregate((previous, current) => previous && current);

            // Then
            Assert.True(found);

            foreach (string person in persons)
                Assert.Null(DataService.GetPersonBookmark(UserId, person));
        }


        // Ratings Tests

        [Fact]
        public void Test_AddRatingWithoutComment_Then_Delete() {
            /***** CREATE *****/

            // Given
            var titleId = "tt7293342";
            var rate = 5;

            // When
            var rating1 = DataService.AddRating(UserId, titleId, rate);

            // Then
            var rating2 = DataService.GetRating(UserId, titleId);
            Assert.NotNull(rating1);
            Assert.NotNull(rating2);
            Assert.Equal(rating1.TitleId, rating2.TitleId);
            Assert.Equal(rating1.UserId, rating2.UserId);
            Assert.Equal(rating1.Rate, rating2.Rate);
            Assert.NotEqual(default, rating2.Date);
            Assert.Null(rating1.Comment);
            Assert.Null(rating2.Comment);


            /***** DELETE *****/

            Assert.True(DataService.DeleteRating(UserId, titleId));
            Assert.Null(DataService.GetRating(UserId, titleId));
        }

        [Fact]
        public void Test_AddRating_Then_Delete() {
            /***** CREATE *****/

            // Given
            var titleId = "tt12026908";
            var rate = 5;
            var comment = "fine episode!";

            // When
            var rating1 = DataService.AddRating(UserId, titleId, rate, comment);

            // Then
            var rating2 = DataService.GetRating(UserId, titleId);
            Assert.NotNull(rating1);
            Assert.NotNull(rating2);
            Assert.Equal(rating1.TitleId, rating2.TitleId);
            Assert.Equal(rating1.UserId, rating2.UserId);
            Assert.Equal(rating1.Rate, rating2.Rate);
            Assert.NotEqual(default, rating2.Date);

            Assert.NotNull(rating1.Comment);
            Assert.NotNull(rating2.Comment);
            Assert.Equal(rating1.Comment.Content, rating2.Comment.Content);
            Assert.NotEqual(default, rating2.Comment.Date);


            /***** DELETE *****/

            DataService.DeleteRating(UserId, titleId);
            Assert.Null(DataService.GetRating(UserId, titleId));
        }

        [Fact]
        public void Test_GetRatingsByUserId() {
            // Given
            var userId = 2;

            // When
            var (ratings, _) = DataService.GetRatingsByUserId(userId, 0, 10);

            // Then
            Assert.NotNull(ratings);
            Assert.Equal(2, ratings.Count);
        }

        [Fact]
        public void Test_GetRatingsByTitleId() {
            // Given
            var titleId = "tt7293342";

            // When
            var (ratings, _) = DataService.GetRatingsByTitleId(titleId, 0, 10);

            // Then
            Assert.NotNull(ratings);
            Assert.Equal(2, ratings.Count);
        }

        [Fact]
        public void Test_GetRating() {
            // Given
            var userId = 3;
            var titleId = "tt7293342";

            // When
            var rating = DataService.GetRating(userId, titleId);

            // Then
            Assert.NotNull(rating);
            Assert.Equal(titleId, rating.TitleId);
            Assert.Equal(userId, rating.UserId);
        }
    }
}
