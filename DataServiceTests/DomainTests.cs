﻿using System;
using Xunit;
using DataService.Domain;
using System.Collections.Generic;

namespace Tests.DataService {

    public class DomainTests {

        [Fact]
        public void Test_Acting() {
            var Acting = new Acting();
            Assert.Null(Acting.TitleId);
            Assert.Null(Acting.PersonId);
            Assert.Null(Acting.Playing);
            Assert.Null(Acting.Job);
        }

        [Fact]
        public void Test_Adult() {
            var Adult = new Adult();
            Assert.Null(Adult.TitleId);
        }

        [Fact]
        public void Test_Award() {
            var Award = new Award();
            Assert.Null(Award.TitleId);
            Assert.Null(Award.Label);
        }

        [Fact]
        public void Test_Comment() {
            var Comment = new Comment();
            Assert.Null(Comment.TitleId);
            Assert.Equal(0, Comment.UserId);
            Assert.Equal(default, Comment.Date);
            Assert.Null(Comment.Content);
        }

        [Fact]
        public void Test_Death() {
            var Death = new Death();
            Assert.Null(Death.PersonId);
            Assert.Null(Death.DeathYear);
        }

        [Fact]
        public void Test_Duration() {
            var Duration = new Duration();
            Assert.Null(Duration.TitleId);
            Assert.Equal(0, Duration.RuntimeMinutes);
        }

        [Fact]
        public void Test_EndedTitle() {
            var EndedTitle = new EndedTitle();
            Assert.Null(EndedTitle.TitleId);
            Assert.Null(EndedTitle.EndYear);
        }

        [Fact]
        public void Test_GeneralRating() {
            var GeneralRating = new GeneralRating();
            Assert.Null(GeneralRating.TitleId);
            Assert.Equal(0, GeneralRating.NumVotes);
            Assert.Equal(0.0, GeneralRating.AverageRating);
        }

        [Fact]
        public void Test_Genre() {
            var Genre = new Genre();
            Assert.Null(Genre.TitleId);
            Assert.Null(Genre.Label);
        }

        [Fact]
        public void Test_Person() {
            var Person = new Person();
            Assert.Null(Person.PersonId);
            Assert.Null(Person.LastName);
            Assert.Null(Person.FirstName);
            Assert.Null(Person.BirthYear);
            Assert.Null(Person.Death);
            Assert.True(Person.Works.Count == 0);
        }

        [Fact]
        public void Test_PersonBookmark() {
            var PersonBookmark = new PersonBookmark();
            Assert.Equal(0, PersonBookmark.UserId);
            Assert.Equal(default, PersonBookmark.Date);
            Assert.Null(PersonBookmark.Label);
            Assert.Null(PersonBookmark.PersonId);
            Assert.Null(PersonBookmark.User);
            Assert.Null(PersonBookmark.Person);
        }

        [Fact]
        public void Test_Poster() {
            var Poster = new Poster();
            Assert.Null(Poster.TitleId);
            Assert.Null(Poster.Url);
        }

        [Fact]
        public void Test_Rating() {
            var Rating = new Rating();
            Assert.Null(Rating.TitleId);
            Assert.Equal(0, Rating.UserId);
            Assert.Equal(0, Rating.Rate);
            Assert.Equal(default, Rating.Date);
            Assert.Null(Rating.Comment);
        }

        [Fact]
        public void Test_SearchHistory() {
            var SearchHistory = new SearchHistory();
            Assert.Equal(0, SearchHistory.UserId);
            Assert.Equal(default, SearchHistory.Date);
            Assert.Equal('\0', SearchHistory.SearchType);
            Assert.Null(SearchHistory.SearchedText);
        }

        [Fact]
        public void Test_Synopsis() {
            var Synopsis = new Synopsis();
            Assert.Null(Synopsis.TitleId);
            Assert.Null(Synopsis.Plot);
        }

        [Fact]
        public void Title() {
            var Title = new Title();
            Assert.Null(Title.TitleId);
            Assert.Null(Title.OriginalTitle);
            Assert.Null(Title.ReleaseYear);
            Assert.Null(Title.MediaType);
            Assert.Null(Title.Duration);
            Assert.Null(Title.Adult);
            Assert.Null(Title.GeneralRating);
            Assert.Null(Title.Synopsis);
            Assert.Null(Title.Award);
            Assert.Null(Title.EndedTitle);
            Assert.Null(Title.Poster);
            Assert.True(Title.Children.Count == 0);
            Assert.True(Title.Akas.Count == 0);
            Assert.True(Title.Genres.Count == 0);
            Assert.True(Title.Staff.Count == 0);
            Assert.True(Title.Children.Count == 0);
        }

        [Fact]
        public void Test_TitleAka() {
            var TitleAka = new TitleAka();
            Assert.Null(TitleAka.TitleId);
            Assert.Null(TitleAka.OtherTitle);
            Assert.Null(TitleAka.CountryCode);
            Assert.Null(TitleAka.LanguageCode);
        }

        [Fact]
        public void Test_TitleBookmark() {
            var TitleBookmark = new TitleBookmark();
            Assert.Equal(0, TitleBookmark.UserId);
            Assert.Equal(default, TitleBookmark.Date);
            Assert.Null(TitleBookmark.Label);
            Assert.Null(TitleBookmark.TitleId);
            Assert.Null(TitleBookmark.Title);
            Assert.Null(TitleBookmark.User);
        }

        [Fact]
        public void Test_TitleChild() {
            var TitleChild = new TitleChild();
            Assert.Null(TitleChild.TitleId);
            Assert.Null(TitleChild.ParentId);
            Assert.Null(TitleChild.Title);
        }

        [Fact]
        public void Test_TVEpisode() {
            var TVEpisode = new TVEpisode();
            Assert.Null(TVEpisode.TitleId);
            Assert.Equal(0, TVEpisode.SeasonNumber);
            Assert.Equal(0, TVEpisode.EpisodeNumber);
        }

        [Fact]
        public void Test_User() {
            var User = new User();
            Assert.Equal(0, User.UserId);
            Assert.Null(User.Login);
            Assert.Null(User.Password);
            Assert.Equal(default, User.Salt);
            Assert.True(User.Ratings.Count == 0);
            Assert.True(User.TitleBookmarks.Count == 0);
            Assert.True(User.PersonBookmarks.Count == 0);
            Assert.True(User.Searches.Count == 0);
        }

        [Fact]
        public void Test_Work() {
            var Work = new Work();
            Assert.Null(Work.TitleId);
            Assert.Null(Work.PersonId);
            Assert.Null(Work.Job);
        }

        [Fact]
        public void Test_SearchTitleResult() {
            var SearchTitleResult = new SearchTitleResult();
            Assert.Null(SearchTitleResult.Id);
            Assert.Equal(0.0, SearchTitleResult.Score);
            Assert.Null(SearchTitleResult.Title);
        }

        [Fact]
        public void Test_SearchPersonResult() {
            var SearchPersonResult = new SearchPersonResult();
            Assert.Null(SearchPersonResult.Id);
            Assert.Equal(0.0, SearchPersonResult.Score);
            Assert.Null(SearchPersonResult.LastName);
            Assert.Null(SearchPersonResult.FirstName);
        }
    }
}
