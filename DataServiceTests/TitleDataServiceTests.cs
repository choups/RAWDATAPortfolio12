﻿using System;
using Xunit;
using DataService;
using System.Linq;
using DataService.Domain;

namespace Tests.DataService {

    public class TitleDataServiceTests {
        private readonly ImdbDataService DataService = new();


        // Title Tests

        [Fact]
        public void Test_GetTitles() {
            var (titles, count) = DataService.GetPagedTitles(1, 25);
            Assert.NotNull(titles);
            Assert.Equal(55076, count);
        }

        [Fact]
        public void Test_GetTitle() {
            var title = DataService.GetTitle("tt8115528");
            Assert.NotNull(title);
            Assert.True(title.OriginalTitle == "Freedom Summer");
            Assert.True(title.ReleaseYear == "2018");
            Assert.True(title.MediaType == "tvEpisode");
        }

        [Fact]
        public void Test_GetStaff() {
            var title = DataService.GetTitle("tt0944947");
            Assert.NotNull(title.Staff);
            Assert.Equal(36, title.Staff.Count);
        }

        [Fact]
        public void Test_GetActors() {
            var title = DataService.GetTitle("tt10276588");
            var actors = title.Staff
                .Where(x => x.Acting != null)
                .Select(x => x.Acting)
                .ToList();

            Assert.NotNull(title.Staff);
            Assert.Equal(3, actors.Count);
        }

        [Fact]
        public void Test_GetAkas() {
            var titleId = "tt0112130";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Akas);
            Assert.Equal(29, title.Akas.Count);
        }

        [Fact]
        public void Test_GetGenres() {
            var titleId = "tt0112130";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Genres);
            Assert.Equal(2, title.Genres.Count);
        }

        [Fact]
        public void Test_GetTitleChildrenProp() {
            var titleId = "tt0944947";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Children);
            Assert.Equal(73, title.Children.Count);

            foreach (TitleChild child in title.Children) {
                Assert.Equal(titleId, child.ParentId);
            }
        }

        [Fact]
        public void Test_GetDuration() {
            var titleId = "tt0112130";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Duration);
            Assert.Equal(327, title.Duration.RuntimeMinutes);
        }

        [Fact]
        public void Test_GetAdult() {
            var titleId = "tt9465630";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Adult);
        }

        [Fact]
        public void Test_GetGeneralRating() {
            var titleId = "tt0112130";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.GeneralRating);
            Assert.Equal(73633, title.GeneralRating.NumVotes);
            Assert.Equal(8.8, Math.Round(title.GeneralRating.AverageRating, 1));
        }

        [Fact]
        public void Test_GetSynopsis() {
            var titleId = "tt0112130";
            var expectedString = "While the arrival of wealthy gentlemen sends her marriage-minded mother into a frenzy, willful and opinionated Elizabeth Bennet matches wits with haughty Mr. Darcy.";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Synopsis);
            Assert.Equal(expectedString.Length, title.Synopsis.Plot.Length);
        }

        [Fact]
        public void Test_GetAward() {
            var titleId = "tt0112130";
            var expectedString = "Won 1 Primetime Emmy. Another 8 wins & 13 nominations.";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Award);
            Assert.Equal(expectedString.Length, title.Award.Label.Length);
        }

        [Fact]
        public void Test_GetEndedTitle() {
            var titleId = "tt0944947";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.EndedTitle);
            Assert.Equal("2019", title.EndedTitle.EndYear);
        }

        [Fact]
        public void Test_GetPoster() {
            var titleId = "tt0944947";
            var expectedUrl = "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg";
            var title = DataService.GetTitle(titleId);
            Assert.NotNull(title.Poster);
            Assert.Equal(expectedUrl.Length, title.Poster.Url.Length);
        }
    }
}
