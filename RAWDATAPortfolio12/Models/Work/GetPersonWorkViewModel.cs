﻿using System;

namespace WebService.Models.Work {

    public class GetPersonWorkViewModel {
        public Uri TitleUri { get; set; }
        public string Job { get; set; }
        public string Playing { get; set; }

        public static GetPersonWorkViewModel FromWork(DataService.Domain.Work work, UrlContext urlContext) {
            return new() {
                TitleUri = urlContext.GetUri(UrlContext.Endpoint.Title, work.TitleId),
                Job = work.Job,
                Playing = work.Acting?.Playing
            };
        }
    }
}
