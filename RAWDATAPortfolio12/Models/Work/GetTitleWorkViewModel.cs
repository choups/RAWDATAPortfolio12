﻿using System;

namespace WebService.Models.Work {

    public class GetTitleWorkViewModel {
        public Uri PersonUrl { get; set; }
        public string Job { get; set; }
        public string Playing { get; set; }

        public static GetTitleWorkViewModel FromWork(DataService.Domain.Work work, UrlContext urlContext) {
            return new GetTitleWorkViewModel() {
                PersonUrl = urlContext.GetUri(UrlContext.Endpoint.Person, work.PersonId),
                Job = work.Job,
                Playing = work.Acting?.Playing
            };
        }
    }
}
