﻿using System;
using System.Collections.Generic;
using WebService.Models.Work;
using System.Linq;

namespace WebService.Models.Person {

    public class GetPersonForPagingViewModel {
        public Uri Url { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthYear { get; set; }
        public string DeathYear { get; set; }

        public static GetPersonForPagingViewModel FromPerson(DataService.Domain.Person person, UrlContext urlContext) {
            return new() {
                Url = urlContext.GetUri(UrlContext.Endpoint.Person, person.PersonId),
                LastName = person.LastName,
                FirstName = person.FirstName,
                BirthYear = person.BirthYear,
                DeathYear = person.Death?.DeathYear
            };
        }
    }
}
