﻿using System;
using System.Collections.Generic;
using WebService.Models.Work;
using System.Linq;

namespace WebService.Models.Person {

    public class PersonViewModel {
        public string PersonId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthYear { get; set; }
        public string DeathYear { get; set; }
        public ICollection<GetPersonWorkViewModel> Works { get; set; }

        public static PersonViewModel FromPerson(DataService.Domain.Person person, UrlContext urlContext) {
            return new() {
                PersonId = person.PersonId,
                LastName = person.LastName,
                FirstName = person.FirstName,
                BirthYear = person.BirthYear,
                DeathYear = person.Death?.DeathYear,
                Works = person.Works.Select(x => GetPersonWorkViewModel.FromWork(x, urlContext)).ToList()
            };
        }
    }
}
