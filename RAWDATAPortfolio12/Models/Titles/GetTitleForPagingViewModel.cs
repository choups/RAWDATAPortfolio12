﻿using System;
using DataService.Domain;

namespace WebService.Models.Titles
{
    public class GetTitleForPagingViewModel
    {
        public Uri Url { get; set; }
        public string OriginalTitle { get; set; }
        public string ReleaseYear { get; set; }
        public string MediaType { get; set; }
        public string PosterUrl { get; set; }
        public bool IsAdult { get; set; }
        public Uri Parent { get; set; }

        public static GetTitleForPagingViewModel FromTitlePaging(Title title, UrlContext urlContext)
        {
            return new GetTitleForPagingViewModel() {
                Url = urlContext.GetUri(UrlContext.Endpoint.Title, title.TitleId),
                OriginalTitle = title.OriginalTitle,
                ReleaseYear = title.ReleaseYear,
                MediaType = title.MediaType,
                PosterUrl = title.Poster?.Url,
                IsAdult = title.Adult != null,
                Parent = title.TitleChild != null ? urlContext.GetUri(UrlContext.Endpoint.Title, title.TitleChild.ParentId) : null
            };
        }
    }
}
