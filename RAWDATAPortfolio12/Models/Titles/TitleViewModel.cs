﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataService.Domain;
using WebService.Models.Work;
using Microsoft.AspNetCore.Http;

namespace WebService.Models.Titles
{
    public class TitleViewModel
    {
        public Uri Url { get; set; }
        public string OriginalTitle { get; set; }
        public string ReleaseYear { get; set; }
        public string MediaType { get; set; }
        public bool IsAdult { get; set; }
        public string EndYear { get; set; }
        public string PosterUrl { get; set; }
        public string Award { get; set; }
        public string Synopsis { get; set; }
        public int? RuntimeMinutes { get; set; }
        public double? GeneralRating { get; set; }

        public IEnumerable<string> Genres { get; set; }
        public IEnumerable<GetTitleWorkViewModel> Works { get; set; }
        public IEnumerable<GetTitleAkaViewModel> TitleAkas { get; set; }
        public IEnumerable<Uri> Children { get; set; }
        public Uri Parent { get; set; }

        public static TitleViewModel FromTitle(Title title, UrlContext urlContext)
        {
            return new TitleViewModel() {
                Url = urlContext.GetUri(UrlContext.Endpoint.Title, title.TitleId),
                OriginalTitle = title.OriginalTitle,
                ReleaseYear = title.ReleaseYear,
                MediaType = title.MediaType,
                PosterUrl = title.Poster?.Url,
                EndYear = title.EndedTitle?.EndYear,
                Award = title.Award?.Label,
                Synopsis = title.Synopsis?.Plot,
                IsAdult = title.Adult != null,
                RuntimeMinutes = title.Duration?.RuntimeMinutes,
                GeneralRating = title.GeneralRating?.AverageRating,
                Genres = title.Genres.Select(x => x.Label),
                Works = title.Staff.Select(x => GetTitleWorkViewModel.FromWork(x, urlContext)),
                TitleAkas = title.Akas.Select(x => GetTitleAkaViewModel.FromTitleAka(x)),
                Children = title.Children.Select(x => urlContext.GetUri(UrlContext.Endpoint.Title, x.TitleId)),
                Parent = title.TitleChild != null ? urlContext.GetUri(UrlContext.Endpoint.Title, title.TitleChild.ParentId) : null
            };
        }
    }
}
