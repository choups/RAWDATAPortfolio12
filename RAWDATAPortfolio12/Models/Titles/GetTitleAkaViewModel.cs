﻿using System;
using DataService.Domain;

namespace WebService.Models.Titles
{
    public class GetTitleAkaViewModel
    {
        public string OtherTitle { get; set; }
        public string CountryCode { get; set; }
        public string LanguageCode { get; set; }

        public static GetTitleAkaViewModel FromTitleAka(TitleAka titleAka)
        {
            return new GetTitleAkaViewModel()
            {
                OtherTitle = titleAka.OtherTitle,
                CountryCode = titleAka.CountryCode,
                LanguageCode = titleAka.LanguageCode
            };
        }
    }
}