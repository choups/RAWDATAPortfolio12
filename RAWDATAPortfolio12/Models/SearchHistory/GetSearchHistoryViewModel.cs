﻿using System;

namespace WebService.Models.SearchHistory {

    public class GetSearchHistoryViewModel {
        public DateTime Date { get; set; }
        public string SearchedText { get; set; }
        public char SearchType { get; set; }

        public static GetSearchHistoryViewModel FromSearchHistory(DataService.Domain.SearchHistory searchHistory) {
            return new() {
                Date = searchHistory.Date,
                SearchedText = searchHistory.SearchedText,
                SearchType = searchHistory.SearchType
            };
        }
    }
}
