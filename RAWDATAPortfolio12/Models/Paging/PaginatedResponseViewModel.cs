﻿using System;
namespace WebService.Models.Paging
{
    public class PaginatedResponseViewModel<T>
    {
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
        public Uri NextPage { get; set; }
        public Uri PreviousPage { get; set; }
        public T Result { get; set; }

        public PaginatedResponseViewModel(T data, int totalResults, int totalPages, Uri nextPage, Uri previousPage)
        {
            TotalResults = totalResults;
            TotalPages = totalPages;
            NextPage = nextPage;
            PreviousPage = previousPage;
            Result = data;
        }
    }
}
