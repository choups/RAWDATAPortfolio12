﻿using System;

namespace WebService.Models.TitleBookmarks {

    public class UpdateTitleBookmarkViewModel {
        public string Label { get; set; }

        public static bool Validate(UpdateTitleBookmarkViewModel titleBookmarkViewModel) {
            return titleBookmarkViewModel.Label != null;
        }
    }
}
