﻿using System;
using DataService.Domain;
using WebService.Models.Titles;

namespace WebService.Models.TitleBookmarks {

    public class GetTitleBookmarkViewModel {
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public string Label { get; set; }
        public Uri TitleUrl { get; set; }


        public static GetTitleBookmarkViewModel FromTitleBookmark(TitleBookmark titleBookmark, UrlContext urlContext) {
            return new GetTitleBookmarkViewModel() {
                UserId = titleBookmark.UserId,
                Date = titleBookmark.Date,
                Label = titleBookmark.Label,
                TitleUrl = urlContext.GetUri(UrlContext.Endpoint.Title, titleBookmark.TitleId)
            };
        }
    }
}
