﻿using System;

namespace WebService.Models.TitleBookmarks {

    public class CreateTitleBookmarkViewModel {
        public string Label { get; set; }
        public string TitleId { get; set; }

        public static bool Validate(CreateTitleBookmarkViewModel titleBookmarkViewModel) {
            return titleBookmarkViewModel.Label != null && titleBookmarkViewModel.TitleId != null;
        }
    }
}
