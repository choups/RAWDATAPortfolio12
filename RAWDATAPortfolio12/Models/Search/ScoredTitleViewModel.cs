﻿using System;
using DataService.Domain;

namespace WebService.Models.Search
{
    public class ScoredTitleViewModel
    {
        public Uri Url { get; set; }
        public string OriginalTitle { get; set; }
        public string ReleaseYear { get; set; }
        public string MediaType { get; set; }
        public bool isAdult { get; set; }
        public string PosterUrl { get; set; }

        public static ScoredTitleViewModel FromTitle(SearchTitleResult searchTitleResult, UrlContext urlContext)
        {
            return new ScoredTitleViewModel()
            {
                Url = urlContext.GetUri(UrlContext.Endpoint.Title, searchTitleResult.Id),
                OriginalTitle = searchTitleResult.Title,
                ReleaseYear = searchTitleResult.ReleaseYear,
                MediaType = searchTitleResult.MediaType,
                isAdult = searchTitleResult.Adult != null,
                PosterUrl = searchTitleResult.PosterUrl
            };
        }
    }
}
