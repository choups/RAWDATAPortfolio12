﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Models.Search
{
    public class ScoredPersonViewModel
    {
        public Uri Url { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthYear { get; set; }
        public string DeathYear { get; set; }

        public static ScoredPersonViewModel FromPerson(DataService.Domain.SearchPersonResult person, UrlContext urlContext)
        {
            return new()
            {
                Url = urlContext.GetUri(UrlContext.Endpoint.Person, person.Id),
                LastName = person.LastName,
                FirstName = person.FirstName,
                BirthYear = person.BirthYear,
                DeathYear = person.DeathYear
            };
        }
    }
}
