﻿using System;

namespace WebService.Models.Ratings {

    public class UpdateRatingViewModel {
        public int Rate { get; set; }
        public string Comment { get; set; }

        public static bool Validate(UpdateRatingViewModel viewModel) {
            return viewModel.Rate != 0;
        }
    }
}
