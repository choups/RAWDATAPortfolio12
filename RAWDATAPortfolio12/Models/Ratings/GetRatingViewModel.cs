﻿using System;
using DataService.Domain;

namespace WebService.Models.Ratings {

    public class GetRatingViewModel {
        public Uri TitleUrl { get; set; }
        public int UserId { get; set; }
        public int Rate { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }


        public static GetRatingViewModel FromRating(Rating rating, UrlContext urlContext) {
            var ratingViewModel = new GetRatingViewModel() {
                TitleUrl = urlContext.GetUri(UrlContext.Endpoint.Title, rating.TitleId),
                UserId = rating.UserId,
                Rate = rating.Rate
            };

            if (rating.Comment != null)
                ratingViewModel.Comment = rating.Comment.Content;

            return ratingViewModel;
        }
    }
}
