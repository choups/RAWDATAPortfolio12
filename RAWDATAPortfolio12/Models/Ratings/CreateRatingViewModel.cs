﻿using System;
using DataService.Domain;

namespace WebService.Models.Ratings {

    public class CreateRatingViewModel {
        public string TitleId { get; set; }
        public int Rate { get; set; }
        public string Comment { get; set; }

        public static bool Validate(CreateRatingViewModel viewModel) {
            return viewModel.TitleId != null && viewModel.Rate > 0;
        }
    }
}
