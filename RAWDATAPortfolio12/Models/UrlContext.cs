﻿using System;
using Microsoft.AspNetCore.Http;

namespace WebService.Models {

    public class UrlContext {
        private readonly HttpContext HttpContext;

        public enum Endpoint {
            Title,
            Person,
            TitleBookmark,
            PersonBookmark,
            Rating
        }

        public UrlContext(HttpContext httpContext) {
            HttpContext = httpContext;
        }

        public Uri GetUri(Endpoint endpoint, string resource = "") {
            var http = HttpContext.Request.IsHttps ? "https" : "http";

            //return new ($"{http}://{HttpContext.Request.Host}{HttpContext.Request.Path}api/{GetEndpointValue(endpoint)}/{resource}");
            return new($"{http}://{HttpContext.Request.Host}/api/{GetEndpointValue(endpoint)}/{resource}");
        }

        private static string GetEndpointValue(Endpoint endpoint) {
            return endpoint switch {
                Endpoint.Title => "titles",
                Endpoint.Person => "persons",
                Endpoint.TitleBookmark => "bookmarks/title",
                Endpoint.PersonBookmark => "bookmarks/person",
                Endpoint.Rating => "ratings",
                _ => throw new Exception("Endpoint not implemented"),
            };
        }
    }
}
