﻿using System;

namespace WebService.Models.PersonBookmarks {

    public class UpdatePersonBookmarkViewModel {
        public string Label { get; set; }

        public static bool Validate(UpdatePersonBookmarkViewModel personBookmarkViewModel) {
            return personBookmarkViewModel.Label != null;
        }
    }
}
