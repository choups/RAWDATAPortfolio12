﻿using System;

namespace WebService.Models.PersonBookmarks {

    public class CreatePersonBookmarkViewModel {
        public string Label { get; set; }
        public string PersonId { get; set; }

        public static bool Validate(CreatePersonBookmarkViewModel personBookmarkViewModel) {
            return personBookmarkViewModel.Label != null && personBookmarkViewModel.PersonId != null;
        }
    }
}
