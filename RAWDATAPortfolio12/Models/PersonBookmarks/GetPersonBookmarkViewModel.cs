﻿using System;
using DataService.Domain;
using WebService.Models.Person;

namespace WebService.Models.PersonBookmarks {

    public class GetPersonBookmarkViewModel {
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public string Label { get; set; }
        public Uri PersonUrl { get; set; }


        public static GetPersonBookmarkViewModel FromPersonBookmark(PersonBookmark personBookmark, UrlContext urlContext) {
            return new GetPersonBookmarkViewModel() {
                UserId = personBookmark.UserId,
                Date = personBookmark.Date,
                Label = personBookmark.Label,
                PersonUrl = urlContext.GetUri(UrlContext.Endpoint.Person, personBookmark.PersonId)
            };
        }
    }
}
