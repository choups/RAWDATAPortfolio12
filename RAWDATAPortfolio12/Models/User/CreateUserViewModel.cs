﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Models.User {

    public class CreateUserViewModel {
        public string Login { get; set; }
        public string Password { get; set; }

        public static bool Validate(CreateUserViewModel viewModel) {
            return viewModel.Login != null && viewModel.Password != null;
        }
    }
}
