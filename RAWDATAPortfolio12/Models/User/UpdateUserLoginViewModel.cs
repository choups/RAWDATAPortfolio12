﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Models.User
{
    public class UpdateUserLoginViewModel
    {
        public string NewLogin { get; set; }
        public string Password { get; set; }
    }
}
