﻿using System;
using Newtonsoft.Json;
using WebService.Models;
using WebService.Models.Paging;
using WebService.Services;
using Microsoft.AspNetCore.Http;

namespace WebService.Helpers
{
    public static class PaginationHelper
    {
        public static PaginatedResponseViewModel<T> CreatePaginatedResponse<T>(T data, int count, PagingParametersFilter pagingParametersFilter, IUriService uriService, string route, IHeaderDictionary header)
        {
            int CurrentPage = pagingParametersFilter.PageNumber;
            int PageSize = pagingParametersFilter.PageSize;

            int TotalPages = (int)Math.Ceiling(count / (double)PageSize);

            var metadata = new
            {
                TotalResults = count,
                PageSize,
                CurrentPage,
                TotalPages,
                HasPrevious = CurrentPage > 1,
                HasNext = CurrentPage < TotalPages
            };
            header.Add("Pagination-Metadata", JsonConvert.SerializeObject(metadata));

            var NextPage =
                (pagingParametersFilter.PageNumber >= 1 && pagingParametersFilter.PageNumber < TotalPages)
                ? uriService.GetPageUri(new PagingParametersFilter(pagingParametersFilter.PageNumber + 1, pagingParametersFilter.PageSize), route)
                : null;
            var PreviousPage =
                (pagingParametersFilter.PageNumber - 1 >= 1 && pagingParametersFilter.PageNumber <= TotalPages)
                ? uriService.GetPageUri(new PagingParametersFilter(pagingParametersFilter.PageNumber - 1, pagingParametersFilter.PageSize), route)
                : null;

            return new PaginatedResponseViewModel<T>(data, count, TotalPages, NextPage, PreviousPage);
        }
    }
}
