﻿using System;
namespace WebService.Helpers {

    public class PagingParametersFilter {
        private const int _maxPageSize = 25;
        private int _pageSize { get; set; }

        public int PageNumber { get; set; }

        public int PageSize {
            get => _pageSize;
            set { _pageSize = value > _maxPageSize ? _maxPageSize : value; }
        }

        public PagingParametersFilter() {
            PageNumber = 1;
            PageSize = _maxPageSize;
        }

        public PagingParametersFilter(int pageNumber, int pageSize) {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;
            PageSize = pageSize > _maxPageSize ? _maxPageSize : pageSize;
        }
    }
}
