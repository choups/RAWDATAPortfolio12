﻿using System;
using Microsoft.AspNetCore.WebUtilities;
using WebService.Helpers;

namespace WebService.Services
{
    public class UriService : IUriService
    {
        private readonly string _baseUri;
        public UriService(string baseUri)
        {
            _baseUri = baseUri;
        }
        public Uri GetPageUri(PagingParametersFilter pagingModel, string route)
        {
            var _endpointUri = new Uri(string.Concat(_baseUri, route));
            var modifiedUri = QueryHelpers.AddQueryString(_endpointUri.ToString(), "pageNumber", pagingModel.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", pagingModel.PageSize.ToString());
            return new Uri(modifiedUri);
        }
    }
}
