﻿using System;
using WebService.Helpers;

namespace WebService.Services
{
    public interface IUriService
    {
        public Uri GetPageUri(PagingParametersFilter pagingModel, string route);
    }
}
