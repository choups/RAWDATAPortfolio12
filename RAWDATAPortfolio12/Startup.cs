﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Identity.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebService.Middleware;
using DataService.Interfaces;
using DataService;
using WebService.Services;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Http;

namespace RAWDATAPortfolio12 {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddMicrosoftIdentityWebApi(Configuration.GetSection("AzureAdB2C"));

            services.AddControllers();
            services.AddSingleton<IBookmarkDataService, UserDataService>();
            services.AddSingleton<IRatingDataService, UserDataService>();
            services.AddSingleton<IUserDataService, UserDataService>();
            services.AddSingleton<ITitleDataService, ImdbDataService>();
            services.AddSingleton<IPersonDataService, ImdbDataService>();
            services.AddSingleton<ISearchDataService, UserDataService>();
            // set up uri helper to use for pagination uris
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUriService>(o =>
            {
                var accessor = o.GetRequiredService<IHttpContextAccessor>();
                var request = accessor.HttpContext.Request;
                var uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new UriService(uri);
            });

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IMDb app", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IMDb app v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseJwtAuth();

            app.UseFileServer();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
