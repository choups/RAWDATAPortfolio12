
define([], () => {
    return {
        titlesView: 'titles',
        personsView: 'persons',
        searchView: 'search',
        bookmarksView: 'bookmarks',
        titlesDetailsView: 'title-details',
        personsDetailsView: 'person-details',
    }
})