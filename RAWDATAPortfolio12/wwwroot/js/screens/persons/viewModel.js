
define(['personService', 'postman'], (personService, postman) => {

    return function() {
        personService.getPersons()
            .then(result => postman.publish('updatePersonData', result))
            .catch(error => console.log(error))

        postman.subscribe('getData', (url) => {
            postman.publish('setLoading', true)
            personService.getPersons(url)
                .then(result => postman.publish('updatePersonData', result))
                .catch(error => console.log(error))
        })

        postman.subscribe('lastView', () => {
            postman.publish('changeView', { name: 'persons' })
        })
    }
})