
define(['titleService', 'postman'], (titleService, postman) => {
    
    return function() {
        titleService.getTitles()
            .then(result => postman.publish('updateTitleData', result))
            .catch(error => console.log(error))

        postman.subscribe('getData', (url) => {
            postman.publish('setLoading', true)
            titleService.getTitles(url)
                .then(result => postman.publish('updateTitleData', result))
                .catch(error => console.log(error))
        })

        postman.subscribe('lastView', () => {
            postman.publish('changeView', { name: 'titles' })
        })
    }
})