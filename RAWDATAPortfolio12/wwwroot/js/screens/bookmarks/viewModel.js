
define(['knockout', 'titleBookmarkService', 'personBookmarkService', 'postman'], (ko, titleBookmarkService, personBookmarkService, postman) => {
    const bookmarkTypes = {
        title: 'titleBookmarks',
        person: 'personBookmarks'
    }

    return function() {
        const titleTab = document.querySelector('#titleBookmarks')
        const personTab = document.querySelector('#personBookmarks')

        const bookmarkType = ko.observable(bookmarkTypes.title)
        const currentComponent = ko.observable('list-titlebookmarks')

        titleBookmarkService.getTitleBookmarks()
            .then(result => postman.publish('updateData', result))
            .catch(error => console.log(error))

        const handleClick = (_, event) => {
            const id = event.currentTarget.id
            let promise

            bookmarkType(id)

            switch (bookmarkType()) {
                case bookmarkTypes.title:
                    promise = titleBookmarkService.getTitleBookmarks()
                    personTab.classList.remove('active')
                    titleTab.classList.add('active')
                    currentComponent('list-titlebookmarks')
                    break
                    
                case bookmarkTypes.person:
                    promise = personBookmarkService.getPersonBookmarks()
                    titleTab.classList.remove('active')
                    personTab.classList.add('active')
                    currentComponent('list-personbookmarks')
                    break
            }

            promise.then(result => postman.publish('updateData', result))
            promise.catch(error => console.log(error))
        }

        postman.subscribe('getData', (url) => {
            postman.publish('setLoading', true)
            let promise
            
            switch (bookmarkType()) {
                case bookmarkTypes.title:
                    promise = titleBookmarkService.getTitleBookmarks(url)
                    break

                case bookmarkTypes.person:
                    promise = personBookmarkService.getPersonBookmarks(url)
                    break
            }

            promise.then(result => postman.publish('updateData', result))
            promise.catch(error => console.log(error))
        })
        
        postman.subscribe('lastView', () => {
            postman.publish('changeView', { name: 'bookmarks' })
        })

        return {
            handleClick,
            currentComponent
        }
    }
})