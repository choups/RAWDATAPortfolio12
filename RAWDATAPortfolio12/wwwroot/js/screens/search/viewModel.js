﻿define(['knockout', 'searchService', 'postman'], (ko, searchService, postman) => {
    const searchTypes = {
        titles: 'titles',
        persons: 'persons'
    }

    return function() {
        const searchTitleButton = document.querySelector('#searchBar #searchTitleButton')
        const searchPersonButton = document.querySelector('#searchBar #searchPersonButton')
        const searchTextInput = document.querySelector('#searchBar input')
        
        const currentComponent = ko.observable(null)
        const lastSearchString = ko.observable(null)
        const selectedSearchType = ko.observable(searchTypes.titles)

        const searchHistory = ko.observableArray([])

        const isViewable = ko.pureComputed(() => lastSearchString() != null)


        const setTitleSearchType = () => {
            searchPersonButton.classList.remove('active')
            searchTitleButton.classList.add('active')
            selectedSearchType(searchTypes.titles)
        }

        const setPersonSearchType = () => {
            searchPersonButton.classList.add('active')
            searchTitleButton.classList.remove('active')
            selectedSearchType(searchTypes.persons)
        }


        // History

        searchService.getSearchHistory()
            .then(history => {
                searchHistory(history)
            })
            .catch(error => console.log(error))

        const addHistory = (searchedText, searchType) => {
            searchHistory.unshift({
                searchedText,
                searchType
            })

            if (searchHistory().length > 5)
                searchHistory.pop()
        }


        // Event handlers
        
        const handleChoiceClick = (_, event) => {
            switch (event.currentTarget.id) {
                case searchTitleButton.id:
                    setTitleSearchType()
                    break

                case searchPersonButton.id:
                    setPersonSearchType()
                    break
            }
        }

        const handleSubmitSearch = () => {
            let event
            let promise
            const searchText = searchTextInput.value

            postman.publish('setLoading', true)

            if (lastSearchString() != null)
                postman.publish('resetList', null)

            switch (selectedSearchType()) {
                case searchTypes.titles:
                    currentComponent('list-titles')
                    event = 'updateTitleData'
                    promise = searchService.searchTitles(searchText)
                    lastSearchString(searchText)
                    addHistory(searchText, 't')

                    postman.subscribe('getData', (url) => {
                        postman.publish('setLoading', true)
                        searchService.searchTitles(lastSearchString(), url)
                            .then(result => {
                                if (result == null) {
                                    postman.publish('resetList', null)

                                } else {
                                    postman.publish('updateTitleData', result)
                                }
                            })
                            .catch(error => console.log(error))
                    })
                    break
                
                case searchTypes.persons:
                    currentComponent('list-persons')
                    event = 'updatePersonData'
                    promise = searchService.searchPersons(searchText)
                    lastSearchString(searchText)
                    addHistory(searchText, 'p')

                    postman.subscribe('getData', (url) => {
                        postman.publish('setLoading', true)
                        searchService.searchPersons(lastSearchString(), url)
                            .then(result => {
                                if (result == null) {
                                    postman.publish('resetList', null)

                                } else {
                                    postman.publish('updatePersonData', result)
                                }
                            })
                            .catch(error => console.log(error))
                    })
                    break
            }

            promise.then(data => {
                if (data == null) {
                    postman.publish('resetList', null)

                } else {
                    postman.publish(event, data)
                }
            })
            promise.catch(error => console.log(error))
        }

        const handleClickHistory = (searchedText, searchType) => {
            searchTextInput.value = searchedText

            switch (searchType) {
                case 't':
                    setTitleSearchType()
                    break

                case 'p':
                    setPersonSearchType()
                    break
            }

            handleSubmitSearch()
        }

        postman.subscribe('lastView', () => {
            postman.publish('changeView', { name: 'search' })
        })

        return {
            searchHistory,
            handleChoiceClick,
            handleSubmitSearch,
            handleClickHistory,
            currentComponent,
            isViewable
        }
    }
})