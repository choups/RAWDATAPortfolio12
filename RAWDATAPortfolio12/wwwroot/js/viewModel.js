﻿
define(["knockout", "postman", "bootstrap"], (ko, postman) => {
    const currentView = ko.observable("titles")
    const currentParams = ko.observable({})

    postman.subscribe("changeView", ({name, params={}}) => {
        currentView(name)
        currentParams(params)
    })

    return {
        currentView,
        currentParams
    }
})