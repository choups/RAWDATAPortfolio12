﻿
require.config({
    baseUrl: 'js',
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        text: "lib/text",
        knockout: "lib/knockout",
        bootstrap: "lib/bootstrap.bundle.min",
        jquery: "lib/jquery.min",
        titleService: "services/titleService",
        personService: "services/personService",
        loginService: "services/loginService",
        searchService: "services/searchService",
        titleBookmarkService: "services/titleBookmarkService",
        personBookmarkService: "services/personBookmarkService",
        ratingService: "services/ratingService",
        postman: "services/postman",
        screenNames: "screenNames",
        utils: "utils"
    }
})



require(["knockout"], (ko) => {
    
    // component registration
    const components = [
        'loading-spinner',
        'navbar',
        'list-nav',
        'list-titles',
        'list-persons',
        'list-titlebookmarks',
        'list-personbookmarks',
        'title-details',
        'person-details',
        'login-modal',
        'register-modal',
        'add-bookmark-modal',
        'update-bookmark-modal',
        'delete-bookmark-modal',
        'rate-stars',
        'add-rating-modal',
        'update-rating-modal',
        'delete-rating-modal'
    ]
    
    components.forEach(componentName => {
        ko.components.register(componentName, {
            viewModel: { require: `components/${componentName}/viewModel` },
            template: { require: `text!components/${componentName}/index.html` }
        })
    })

    
    // screens registration
    const screens = [
        'titles',
        'search',
        'persons',
        'bookmarks'
    ]

    screens.forEach(screenName => {
        ko.components.register(screenName, {
            viewModel: { require: `screens/${screenName}/viewModel` },
            template: { require: `text!screens/${screenName}/index.html` }
        })
    })
})

require(["knockout", "viewModel"], (ko, vm) => {
    ko.applyBindings(vm)
})