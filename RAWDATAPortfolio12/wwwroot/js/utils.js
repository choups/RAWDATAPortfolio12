
define([], () => {
        
    const url2id = (url) => {
        const splitted = url.split('/')
        return splitted[splitted.length-1]
    }

    const groupBy3 = (list) => {
        const result = []
        let tmp = []
    
        list.forEach((elem, index) => {
            tmp.push(elem)
    
            if ((index+1) % 3 == 0 || index == list.length-1) {
                result.push(tmp)
                tmp = []
            }
        })

        return result
    }

    return {
        url2id,
        groupBy3
    }
})