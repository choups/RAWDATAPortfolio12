﻿
define([], () => {

    const saveToken = (login, token) => {
        const millisecondsFor2weeks = 1209600000
        const data = {
            login: login,
            token: token,
            expiry: new Date().getTime() + millisecondsFor2weeks
        }

        localStorage.setItem('token', JSON.stringify(data))
    }

    const destroyToken = () => {
        localStorage.removeItem('token')
    }

    const getLogin = () => {
        const data = JSON.parse(localStorage.getItem('token'))

        if (data && data.expiry > new Date().getTime())
            return data.login

        return null
    }

    const getToken = () => {
        const data = JSON.parse(localStorage.getItem('token'))

        if (data && data.expiry > new Date().getTime())
            return data.token

        return null
    }

    const login = (login, password) => new Promise(async (resolve, reject) => {
        const request = new Request("https://localhost:5001/api/users/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: login,
                password: password
            })
        })

        try {
            const response = await fetch(request)

            if (response.status == 200) {
                const json = await response.json()
                saveToken(json.login, json.token)
                resolve()

            } else {
                reject({ message: "Wrong credentials" })
            }

        } catch (error) {
            reject({ message: "Internal error. Please try again later"})
        }
    })

    const logout = () => {
        destroyToken()
    }

    const register = (login, password) => new Promise(async (resolve, reject) => {
        const request = new Request("https://localhost:5001/api/users/register", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: login,
                password: password
            })
        })

        try {
            const response = await fetch(request)

            if (response.status == 201) {
                resolve()

            } else if (response.status == 409) {
                reject({ message: "This username already exists" })

            } else {
                reject({ message: "Something went wrong. Please try again"})
            }

        } catch (error) {
            reject({ message: "Internal error. Please try again later"})
        }
    })

    return {
        login,
        logout,
        getToken,
        getLogin,
        register
    }
})