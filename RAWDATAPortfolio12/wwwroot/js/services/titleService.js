﻿
define([], () => {
    const defaultUrl = "https://localhost:5001/api/titles?pageSize=9"

    const getTitles = (url=defaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getTitle = (url) => new Promise(async (resolve, reject) => {
        const request = new Request(url)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getTitleRatings = (url) => new Promise(async (resolve, reject) => {
        const request = new Request(`${url}/rating`)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getSimilarTitles = (url) => new Promise(async (resolve, reject) => {
        const request = new Request(`${url}/similar?pageSize=10`)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)
            
        } catch (error) {
            reject(error)
        }
    })

    return {
        getTitles,
        getTitle,
        getTitleRatings,
        getSimilarTitles
    }
})