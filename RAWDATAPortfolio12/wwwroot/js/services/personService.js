﻿
define([], () => {
    const defaultUrl = "https://localhost:5001/api/persons?pageSize=9"

    const getPersons = (url=defaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getPerson = (url) => new Promise(async (resolve, reject) => {
        const request = new Request(url)

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    return {
        getPersons,
        getPerson
    }
})