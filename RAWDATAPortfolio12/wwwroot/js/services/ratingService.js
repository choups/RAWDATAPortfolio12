
define(['loginService'], (loginService) => {

    const getUserRatings = () => new Promise(async (resolve, reject) => {
        const request = new Request('https://localhost:5001/api/users/rating', {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getUserRating = (tconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/rating/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)

            if (response.status === 200) {
                const json = await response.json()
                resolve(json)
            
            } else {
                resolve(null)
            }

        } catch (error) {
            console.log(error)
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })
    
    const createUserRating = (tconst, rate, comment) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/rating`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                titleId: tconst,
                rate: rate,
                comment: comment
            })
        })

        try {
            const response = await fetch(request)

            if (response.status === 201)
                resolve()
            else
                reject({ message: response.statusText })

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const updateUserRating = (tconst, rate, comment=null) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/rating/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'PUT',
            body: JSON.stringify({
                rate: rate,
                comment: comment
            })
        })

        try {
            const response = await fetch(request)

            if (response.status === 200)
                resolve()
            else
                reject({ message: response.statusText })

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const deleteUserRating = (tconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/rating/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            },
            method: 'DELETE'
        })

        try {
            const response = await fetch(request)

            if (response.status === 200)
                resolve()
            else
                reject({ message: response.statusText })

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    return {
        getUserRatings,
        getUserRating,
        createUserRating,
        updateUserRating,
        deleteUserRating
    }
})