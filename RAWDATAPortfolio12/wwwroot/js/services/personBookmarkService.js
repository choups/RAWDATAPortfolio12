
define(['loginService'], (loginService) => {
    const defaultUrl = 'https://localhost:5001/api/users/bookmark/person?pageSize=10'

    const getPersonBookmarks = (url=defaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const personBookmarkExists = (nconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/person/${nconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)
            resolve(response.status !== 204)

        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })
    
    const createPersonBookmark = (nconst, label) => new Promise(async (resolve, reject) => {
        const request = new Request('https://localhost:5001/api/users/bookmark/person', {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                label: label,
                personId: nconst
            })
        })

        try {
            const response = await fetch(request)

            if (response.status === 201)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const updatePersonBookmark = (nconst, label) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/person/${nconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'PUT',
            body: JSON.stringify({
                label: label
            })
        })

        try {
            const response = await fetch(request)
            
            if (response.status === 200)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const deletePersonBookmark = (nconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/person/${nconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'DELETE'
        })

        try {
            const response = await fetch(request)
            
            if (response.status === 200)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    return {
        getPersonBookmarks,
        createPersonBookmark,
        updatePersonBookmark,
        deletePersonBookmark,
        personBookmarkExists
    }
})