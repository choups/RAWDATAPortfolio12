﻿
define(['loginService'], (loginService) => {
    const titlesDefaultUrl = "https://localhost:5001/api/titles?pageSize=9"
    const personsDefaultUrl = "https://localhost:5001/api/persons?pageSize=9"

    const searchTitles = (searchString, url=titlesDefaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + loginService.getToken()
            },
            body: JSON.stringify({
                StringSearch: searchString
            })
        })

        try {
            const response = await fetch(request)
            
            if (response.status === 200) {
                const json = await response.json()
                resolve(json)
            
            } else {
                resolve(null)
            }

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const searchPersons = (searchString, url=personsDefaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + loginService.getToken()
            },
            body: JSON.stringify({
                StringSearch: searchString
            })
        })

        try {
            const response = await fetch(request)

            if (response.status === 200) {
                const json = await response.json()
                resolve(json)
            
            } else {
                resolve(null)
            }

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const getSearchHistory = () => new Promise(async (resolve, reject) => {
        const request = new Request("https://localhost:5001/api/users/history?pageSize=5", {
            headers: {
                'Authorization': 'Bearer ' + loginService.getToken()
            }
        })

        try {
            const response = await fetch(request)

            if (response.status === 200) {
                const json = await response.json()
                resolve(json.result)

            } else {
                resolve([])
            }

        } catch (error) {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    return {
        searchTitles,
        searchPersons,
        getSearchHistory
    }
})