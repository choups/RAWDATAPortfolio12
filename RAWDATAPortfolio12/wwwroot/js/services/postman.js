﻿
define([], () => {
    const subscribers = []

    const subscribe = (event, callback) => {
        const subscriber = { event, callback }
        const index = subscribers.findIndex(x => x.event === event)

        if (index > -1)
            subscribers.splice(index, 1)

        subscribers.push(subscriber)
    }

    const publish = (event, data) => {
        subscribers.forEach(x => {
            if (x.event === event)
                x.callback(data)
        })
    }

    return {
        subscribe,
        publish
    }
})