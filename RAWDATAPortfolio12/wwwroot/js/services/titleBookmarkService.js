
define(['loginService'], (loginService) => {
    const defaultUrl = 'https://localhost:5001/api/users/bookmark/title?pageSize=10'

    const getTitleBookmarks = (url=defaultUrl) => new Promise(async (resolve, reject) => {
        const request = new Request(url, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)
            const json = await response.json()
            resolve(json)

        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const titleBookmarkExists = (tconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/title/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`
            }
        })

        try {
            const response = await fetch(request)
            resolve(response.status !== 204)

        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })
    
    const createTitleBookmark = (tconst, label) => new Promise(async (resolve, reject) => {
        const request = new Request('https://localhost:5001/api/users/bookmark/title', {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                label: label,
                titleId: tconst
            })
        })

        try {
            const response = await fetch(request)

            if (response.status === 201)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const updateTitleBookmark = (tconst, label) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/title/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'PUT',
            body: JSON.stringify({
                label: label
            })
        })

        try {
            const response = await fetch(request)
            
            if (response.status === 200)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    const deleteTitleBookmark = (tconst) => new Promise(async (resolve, reject) => {
        const request = new Request(`https://localhost:5001/api/users/bookmark/title/${tconst}`, {
            headers: {
                'Authorization': `Bearer: ${loginService.getToken()}`,
                'Content-Type': 'application/json'
            },
            method: 'DELETE'
        })

        try {
            const response = await fetch(request)
            
            if (response.status === 200)
                resolve()

            else
                reject({ message: response.statusText })
        
        } catch {
            reject({ message: 'An internal error occurred. Please try again later' })
        }
    })

    return {
        getTitleBookmarks,
        createTitleBookmark,
        updateTitleBookmark,
        deleteTitleBookmark,
        titleBookmarkExists
    }
})