﻿
define(['knockout', 'postman', 'utils'], (ko, postman, utils) => {
    
    return function() {
        const totalPages = ko.observable(0)
        const prevTitlesUrl = ko.observable(null)
        const nextTitlesUrl = ko.observable(null)
        const titles = ko.observableArray([])
        
        const isLoading = ko.observable(true)
        
        postman.subscribe('updateTitleData', (data) => {
            totalPages(data.totalPages)
            prevTitlesUrl(data.previousPage)
            nextTitlesUrl(data.nextPage)

            titles(utils.groupBy3(data.result))

            isLoading(false)
        })
        
        postman.subscribe('setLoading', (bool) => isLoading(bool))

        postman.subscribe('resetList', () => {
            totalPages(0)
            prevTitlesUrl(null)
            nextTitlesUrl(null)
            titles([])
            postman.publish('resetCurrentPage', null)
            isLoading(false)
        })

        const seeDetails = (url) => {
            postman.publish('changeView', {
                name: 'title-details',
                params: { url }
            })
        }

        return {
            totalPages,
            prevTitlesUrl,
            nextTitlesUrl,
            titles,
            seeDetails,
            isLoading
        }
    }
})
