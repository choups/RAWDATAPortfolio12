
define(['knockout', 'postman', 'loginService', 'screenNames'], (ko, postman, loginService, screenNames) => {

    const getGreeting = () => {
        const hour = new Date().getHours()
        let greeting

        if (hour >= 6 && hour < 12)
            greeting = "Good morning"
        
        else if (hour >= 12 && hour < 18)
            greeting = "Good afternoon"

        else
            greeting = "Good evening"

        return greeting
    }
    
    return function(params) {
        const changeToTitleView = () => postman.publish('changeView', { name: screenNames.titlesView })
        const changeToPersonView = () => postman.publish('changeView', { name: screenNames.personsView })
        const changeToBookmarkView = () => postman.publish('changeView', { name: screenNames.bookmarksView })
        const changeToSearchView = () => postman.publish('changeView', { name: screenNames.searchView })

        const titleNavActiveStatus = ko.pureComputed(() => params.currentView() === screenNames.titlesView || params.currentView() === screenNames.titlesDetailsView ? 'active' : '')
        const personsNavActiveStatus = ko.pureComputed(() => params.currentView() === screenNames.personsView || params.currentView() === screenNames.personsDetailsView ? 'active' : '')
        const bookmarksNavActiveStatus = ko.pureComputed(() => params.currentView() === screenNames.bookmarksView ? 'active' : '')
        const searchNavActiveStatus = ko.pureComputed(() => params.currentView() === screenNames.searchView ? 'active' : '')

        const login = ko.observable(loginService.getLogin())
        const token = ko.observable(loginService.getToken())

        const isLoggedStatus = ko.pureComputed(() => token() == null)
        const welcomeMessage = ko.pureComputed(() => login() == null ? "" : `${getGreeting()}, ${login()}`)

        const handleLogoutClick = () => {
            loginService.logout()
            login(loginService.getLogin())
            token(loginService.getToken())
        }

        return {
            changeToTitleView,
            changeToPersonView,
            changeToSearchView,
            changeToBookmarkView,
            titleNavActiveStatus,
            personsNavActiveStatus,
            searchNavActiveStatus,
            bookmarksNavActiveStatus,
            isLoggedStatus,
            handleLogoutClick,
            token,
            login,
            welcomeMessage
        }
    }
})