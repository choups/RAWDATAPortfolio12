
define(['knockout', 'postman'], (ko, postman) => {
    return function(params) {
        const currentPage = ko.observable(0);

        const prevPage = async () => {
            const current = currentPage()
            if (current > 0) {
                currentPage(current - 1)
                postman.publish('getData', params.prevUrl())
            }
        }

        const nextPage = async () => {
            const current = currentPage()
            if (current < params.totalPages()) {
                currentPage(current + 1)
                postman.publish('getData', params.nextUrl())
            }
        }

        const prevButtonClickableStatus = ko.pureComputed(() => (params.prevUrl() == null || params.isLoading()) ? "disabled" : "")
        const nextButtonClickableStatus = ko.pureComputed(() => (params.nextUrl() == null || params.isLoading()) ? "disabled" : "")

        postman.subscribe('resetCurrentPage', () => currentPage(0))

        return {
            prevPage,
            nextPage,
            currentPage,
            prevButtonClickableStatus,
            nextButtonClickableStatus,
        }
    }
})