
define(['bootstrap', 'postman'], (bootstrap, postman) => {

    return function(params) {
        const modal = document.querySelector('#updateBookmarkModal')
        
        const labelInput = document.querySelector('#updateBookmarkModal #labelInput')
        const errorText = document.querySelector("#updateBookmarkModal #errorText")

        modal.addEventListener('shown.bs.modal', () => {
            labelInput.value = params.label()
        })

        const handleSubmit = async () => {
            try {
                await postman.publish('updateBookmark', labelInput.value)

                bootstrap.Modal.getInstance(modal).hide()

            } catch (error) {
                errorText.innerHTML = error.message
            }
        }

        return {
            handleSubmit
        }
    }
})