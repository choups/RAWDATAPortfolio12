
define(['knockout', 'titleService', 'titleBookmarkService', 'personService', 'ratingService', 'postman', 'utils'], (ko, titleService, titleBookmarkService, personService, ratingService, postman, utils) => {
    
    return function(params) {
        const url = ko.observable(params.url)

        const originalTitle = ko.observable(null)
        const releaseYear = ko.observable(null)
        const mediaType = ko.observable(null)
        const isAdult = ko.observable(null)
        const endYear = ko.observable(null)
        const posterUrl = ko.observable(null)
        const awards = ko.observable(null)
        const synopsis = ko.observable(null)
        const runtimeMinutes = ko.observable(null)
        const generalRating = ko.observable(null)
        const genresList = ko.observableArray([])
        const works	= ko.observableArray([])
        const titleAkas = ko.observableArray([])
        const children = ko.observableArray([])
        const lastRatings = ko.observableArray([])
        
        const userRating = ko.observable(null)
        const userComment = ko.observable(null)
        
        const parent = ko.observable(null)
        const parentTitle = ko.observable(null)
        
        const hasParent = ko.pureComputed(() => parent() != null)
        const hasPoster = ko.pureComputed(() => posterUrl() != null)
        const hasWorks = ko.pureComputed(() => works().length > 0)
        const hasAkas = ko.pureComputed(() => titleAkas().length > 0)
        const hasChildren = ko.pureComputed(() => children().length > 0)
        const hasGeneralRating = ko.pureComputed(() => generalRating() != null)
        const hasLastRatings = ko.pureComputed(() => lastRatings().length > 0)
        const isRated = ko.pureComputed(() => userRating() != null)
        
        const isLoading = ko.observable(true)
        const isBookmarked = ko.observable(false)

        const similarTitlesLoading = ko.observable(true);
        const similarTitles = ko.observable([]);

        const genres = ko.pureComputed(() => {
            if (genresList().length == 0) return 'N/A'
            return genresList().reduce((acc, curr) => `${acc}, ${curr}`)
        })

        titleService.getTitle(url())
            .then(async title => {
                updateBookmarked()
                updateRate()
                
                if (title.parent != null) {
                    const parentData = await titleService.getTitle(title.parent)
                    parentTitle(parentData.originalTitle)
                }
    
                parent(title.parent)
                originalTitle(title.originalTitle)
                releaseYear(title.releaseYear)
                mediaType(title.mediaType)
                isAdult(title.isAdult)
                endYear(title.endYear || 'N/A')
                posterUrl(title.posterUrl)
                awards(title.award || 'N/A')
                synopsis(title.synopsis || 'N/A')
                runtimeMinutes(title.runtimeMinutes || 'N/A')
                generalRating(4.6)
                genresList(title.genres)
                titleAkas(title.titleAkas)
                children(title.children)

                title.works.forEach(async work => {
                    try {
                        const person = await personService.getPerson(work.personUrl)
                        
                        const data = {
                            lastName: person.lastName,
                            firstName: person.firstName,
                            job: (() => {
                                return (work.playing) ? `${work.job} - ${work.playing}` : work.job
                            })()
                        }
                        
                        works.push(data)
                    
                    } catch (error) {
                        console.log(error)
                    }
                })

                try {
                    const data = await titleService.getTitleRatings(url())
                    lastRatings(data.result)

                } catch (error) {
                    console.log(error)
                }
    
                isLoading(false)
            })
            .catch(error => console.log(error))

        // Create bookmark
        postman.subscribe('createBookmark', (label) => new Promise(async (resolve, reject) => {
            const tconst = utils.url2id(url())

            try {
                await titleBookmarkService.createTitleBookmark(tconst, label)
                updateBookmarked()
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        // Bookmarked status
        const updateBookmarked = async () => {
            const tconst = utils.url2id(url())
            
            try {
                const result = await titleBookmarkService.titleBookmarkExists(tconst)
                isBookmarked(result)

            } catch (error) {
                console.log(error.message)
                isBookmarked(false)
            }
        }

        // Update rate
        const updateRate = async () => {
            const tconst = utils.url2id(url())

            try {
                const result = await ratingService.getUserRating(tconst)

                if (result == null) {
                    userRating(null)
                    userComment(null)
                    postman.publish('reset-rate-stars', null)

                } else {
                    userRating(result.rate)
                    userComment(result.comment)
                }

            } catch (error) {
                console.log(error)
                isRated(false)
            }
        }

        // Ratings functions
        postman.subscribe('createRating', ({rate, comment}) => new Promise(async (resolve, reject) => {
            const tconst = utils.url2id(url())

            try {
                await ratingService.createUserRating(tconst, rate, comment)
                updateRate()
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        postman.subscribe('updateRating', ({rate, comment}) => new Promise(async (resolve, reject) => {
            const tconst = utils.url2id(url())

            try {
                await ratingService.updateUserRating(tconst, rate, comment)
                updateRate()
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        postman.subscribe('deleteRating', () => new Promise(async (resolve, reject) => {
            const tconst = utils.url2id(url())

            try {
                await ratingService.deleteUserRating(tconst)
                updateRate()
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        //Similar Titles
        titleService.getSimilarTitles(url())
            .then(data => {
                similarTitles(data.result);
                similarTitlesLoading(false);
            })
            .catch(error => console.log(error))

        // Navigation
        const returnToLastView = () => postman.publish('lastView', null)

        const seeSimilarTitleDetails = (url) => {
            postman.publish('changeView', {
                name: 'title-details',
                params: { url }
            })
        }

        return {
            url,
            originalTitle,
            releaseYear,
            mediaType,
            isAdult,
            endYear,
            posterUrl,
            awards,
            synopsis,
            runtimeMinutes,
            generalRating,
            genres,
            works,
            titleAkas,
            children,
            hasPoster,
            hasParent,
            hasWorks,
            hasAkas,
            hasChildren,
            hasGeneralRating,
            hasLastRatings,
            parentTitle,
            isLoading,
            isBookmarked,
            isRated,
            userRating,
            userComment,
            updateBookmarked,
            returnToLastView,
            seeSimilarTitleDetails,
            similarTitles,
            similarTitlesLoading
        }
    }
})