
define(['knockout', 'postman', 'titleBookmarkService', 'utils'], (ko, postman, titleBookmarkService, utils) => {
    return function() {
        const totalPages = ko.observable(0)
        const prevUrl = ko.observable(null)
        const nextUrl = ko.observable(null)
        const bookmarks = ko.observableArray([])
        
        const isLoading = ko.observable(true)
        
        postman.subscribe('updateData', (data) => {
            data.result.forEach(bookmark => {
                bookmark.date = new Date(bookmark.date).toLocaleDateString()
            })

            data.result.sort((a, b) => a.date < b.date)

            totalPages(data.totalPages)
            prevUrl(data.previousPage)
            nextUrl(data.nextPage)
            bookmarks(data.result)
            
            isLoading(false)
        })

        // Update bookmark
        const putId = ko.observable(null)
        const putLabel = ko.observable(null)

        const updatePutData = (url, label) => {
            const tconst = utils.url2id(url)
            putId(tconst)
            putLabel(label)
        }

        postman.subscribe('updateBookmark', (label) => new Promise(async (resolve, reject) => {
            const tconst = putId()
            const index = bookmarks().findIndex(bookmark => bookmark.titleUrl.includes(tconst))

            const newBookmark = (() => {
                const bookmark = Object.create(bookmarks()[index]) // clone object
                bookmark.label = label
                return bookmark
            })()

            bookmarks.replace(bookmarks()[index], newBookmark)

            try {
                await titleBookmarkService.updateTitleBookmark(tconst, label)
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        // Delete bookmark
        const deleteId = ko.observable(null)

        const updateDeleteId = (url) => {
            const tconst = utils.url2id(url)
            deleteId(tconst)
        }
        
        postman.subscribe('deleteBookmark', async () => {
            const tconst = deleteId()
            bookmarks.remove(bookmark => bookmark.titleUrl.includes(tconst))

            try {
                await titleBookmarkService.deleteTitleBookmark(tconst)

            } catch (error) {
                console.log(error)
            }
        })

        // Details
        const seeDetails = (url) => {
            postman.publish('changeView', {
                name: 'title-details',
                params: { url }
            })
        }

        return {
            totalPages,
            prevUrl,
            nextUrl,
            bookmarks,
            updateDeleteId,
            seeDetails,
            putLabel,
            updatePutData,
            isLoading
        }
    }
})