
define(['loginService', 'bootstrap'], (loginService, bootstrap) => {
    return function(params) {
        const loginInput = document.querySelector('#loginModal #loginInput')
        const passwordInput = document.querySelector('#loginModal #passwordInput')
        const errorText = document.querySelector("#loginModal #errorText")
        const modal = document.querySelector('#loginModal')

        const resetForm = () => {
            loginInput.value = ""
            passwordInput.value = ""
        }

        const handleSubmit = async () => {
            try {
                await loginService.login(loginInput.value, passwordInput.value)
                params.login(loginService.getLogin())
                params.token(loginService.getToken())
                bootstrap.Modal.getInstance(modal).hide()

            } catch (error) {
                errorText.innerHTML = error.message
            }
            
            resetForm()
        }

        return {
            handleSubmit
        }
    }
})