
define(['knockout', 'bootstrap', 'postman'], (ko, bootstrap, postman) => {

    return function() {
        const modal = document.querySelector('#deleteBookmarkModal')
        
        const question = ko.observable(null)

        const deleteBookmark = () => {
            postman.publish('deleteBookmark', null)
            bootstrap.Modal.getInstance(modal).hide()
        }

        return {
            question,
            deleteBookmark
        }
    }
})