
define(['knockout', 'postman'], (ko, postman) => {
    
    return function(params) {
        const buttonList = Array.from(document.querySelectorAll('#rate-stars .btn'))

        const button1ActiveStatus = ko.pureComputed(() => params.rating() >= 1 ? 'active' : '')
        const button2ActiveStatus = ko.pureComputed(() => params.rating() >= 2 ? 'active' : '')
        const button3ActiveStatus = ko.pureComputed(() => params.rating() >= 3 ? 'active' : '')
        const button4ActiveStatus = ko.pureComputed(() => params.rating() >= 4 ? 'active' : '')
        const button5ActiveStatus = ko.pureComputed(() => params.rating() >= 5 ? 'active' : '')

        const handleClick = (_, event) => {
            const clickedButton = event.currentTarget
            const index = buttonList.findIndex(button => button.id === clickedButton.id)

            if (index > -1)
                params.rating(index+1)
        }

        return {
            handleClick,
            button1ActiveStatus,
            button2ActiveStatus,
            button3ActiveStatus,
            button4ActiveStatus,
            button5ActiveStatus
        }
    }
})