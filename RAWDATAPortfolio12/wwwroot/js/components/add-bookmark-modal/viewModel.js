
define(['bootstrap', 'postman'], (bootstrap, postman) => {

    return function() {
        const modal = document.querySelector('#addBookmarkModal')
        
        const labelInput = document.querySelector('#addBookmarkModal #labelInput')
        const submitButton = document.querySelector('#addBookmarkModal #submit')
        const errorText = document.querySelector("#addBookmarkModal #errorText")
        
        const colorInput = (input) => {
            if (input.value.length == 0) {
                input.classList.remove('valid')
                input.classList.add('notvalid')

            } else {
                input.classList.remove('notvalid')
                input.classList.add('valid')
            }
        }

        const resetForm = () => {
            labelInput.value = ""
            labelInput.classList.remove('valid')
            submitButton.disabled = true
        }

        const handleChange = (_, event) => {
            colorInput(event.currentTarget)
            submitButton.disabled = labelInput.value.length == 0
        }

        const handleSubmit = async () => {
            if (labelInput.value.length > 0) {
                try {
                    await postman.publish('createBookmark', labelInput.value)
                    bootstrap.Modal.getInstance(modal).hide()
                
                } catch (error) {
                    errorText.innerHTML = error.message
                }

                resetForm()
            }
        }

        return {
            handleChange,
            handleSubmit
        }
    }
})