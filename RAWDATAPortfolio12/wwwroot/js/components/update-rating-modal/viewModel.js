
define(['knockout', 'postman', 'bootstrap'], (ko, postman, bootstrap) => {
    return function(params) {
        const modal = document.querySelector('#updateRatingModal')
        
        const rating = ko.observable(null)
        const textArea = document.querySelector('#updateRatingModal #commentTextarea')
        const errorText = document.querySelector("#updateRatingModal #errorText")
        
        const resetForm = () => {
            rating(0)
            textArea.value = ""
        }

        const handleSubmit = async () => {
            if (rating() != null) {
                try {
                    await postman.publish('updateRating', {
                        rate: rating(),
                        comment: textArea.value.length > 0 ? textArea.value : null
                    })

                    bootstrap.Modal.getInstance(modal).hide()
                
                } catch (error) {
                    errorText.innerHTML = error.message
                }

                resetForm()
            }
        }

        modal.addEventListener('show.bs.modal', () => {
            rating(params.userRating())
            textArea.value = params.userComment()
        })

        return {
            handleSubmit,
            rating
        }
    }
})