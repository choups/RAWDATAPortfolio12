
define(['loginService', 'bootstrap'], (loginService, bootstrap) => {

    return function() {
        const registerModal = document.querySelector('#registerModal')
        const loginModal = document.querySelector('#loginModal')

        const loginInput = document.querySelector('#registerModal #loginInput')
        const passwordInput = document.querySelector('#registerModal #passwordInput')
        const errorText = document.querySelector("#registerModal #errorText")

        const resetForm = () => {
            loginInput.value = ""
            passwordInput.value = ""
        }

        const handleSubmit = async () => {
            // We only want this event to be triggered if the register form has been submitted
            registerModal.addEventListener('hidden.bs.modal', () => {
                new bootstrap.Modal(loginModal).show()
            })

            try {
                await loginService.register(loginInput.value, passwordInput.value)
                bootstrap.Modal.getInstance(registerModal).hide()

            } catch (error) {
                errorText.innerHTML = error.message
            }
            
            resetForm()
        }

        return {
            handleSubmit
        }
    }
})