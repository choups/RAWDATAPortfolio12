﻿
define(['knockout', 'postman', 'utils'], (ko, postman, utils) => {
    
    return function() {
        const totalPages = ko.observable(0)
        const prevPersonsUrl = ko.observable(null)
        const nextPersonsUrl = ko.observable(null)
        const persons = ko.observableArray([])

        const isLoading = ko.observable(true)

        postman.subscribe('updatePersonData', (data) => {
            totalPages(data.totalPages)
            prevPersonsUrl(data.previousPage)
            nextPersonsUrl(data.nextPage)

            const personTmp = data.result.map(person => {
                person.fullName = `${person.lastName} ${person.firstName}`
                
                if (person.birthYear == "" || person.birthYear == null)
                    person.birthYear = 'N/A'

                return person
            })

            persons(utils.groupBy3(personTmp))

            isLoading(false)
        })

        postman.subscribe('setLoading', (bool) => isLoading(bool))

        postman.subscribe('resetList', () => {
            totalPages(0)
            prevPersonsUrl(null)
            nextPersonsUrl(null)
            persons([])
            postman.publish('resetCurrentPage', null)
            isLoading(false)
        })

        const seeDetails = (url) => {
            postman.publish('changeView', {
                name: 'person-details',
                params: { url }
            })
        }

        return {
            totalPages,
            prevPersonsUrl,
            nextPersonsUrl,
            persons,
            seeDetails,
            isLoading
        }
    }
})
