
define(['knockout', 'postman', 'bootstrap'], (ko, postman, bootstrap) => {
    return function() {
        const modal = document.querySelector('#addRatingModal')
        
        const rating = ko.observable(null)
        const textArea = document.querySelector('#addRatingModal #commentTextarea')
        const errorText = document.querySelector("#addRatingModal #errorText")
        
        const resetForm = () => {
            rating(0)
            textArea.value = ""
        }

        const handleSubmit = async () => {

            if (rating() != null) {
                try {
                    await postman.publish('createRating', {
                        rate: rating(),
                        comment: textArea.value.length > 0 ? textArea.value : null
                    })

                    bootstrap.Modal.getInstance(modal).hide()
                
                } catch (error) {
                    errorText.innerHTML = error.message
                }

                resetForm()
            }
        }

        return {
            handleSubmit,
            rating
        }
    }
})