
define(['knockout', 'personService', 'personBookmarkService', 'titleService', 'postman', 'utils'], (ko, personService, personBookmarkService, titleService, postman, utils) => {
    
    return function(params) {
        const url = ko.observable(params.url)

        const lastName = ko.observable(null)
        const firstName = ko.observable(null)
        const birthYear = ko.observable(null)
        const deathYear = ko.observable(null)
        const works = ko.observableArray([])

        const isLoading = ko.observable(true)
        const isBookmarked = ko.observable(false)
        const hasWorks = ko.pureComputed(() => works().length > 0)

        personService.getPerson(url())
            .then(async person => {
                updateBookmarked()

                lastName(person.lastName)
                firstName(person.firstName)
                birthYear(person.birthYear || 'N/A')
                deathYear(person.deathYear || 'N/A')
                
                person.works.forEach(async work => {
                    try {
                        const title = await titleService.getTitle(work.titleUri)

                        if (title.parent != null) {
                            const parentData = await titleService.getTitle(title.parent)
                            title.originalTitle = `${parentData.originalTitle} - ${title.originalTitle}`
                        }
                        
                        const data = {
                            title: title.originalTitle,
                            releaseYear: title.releaseYear,
                            job: (() => {
                                return (work.playing) ? `${work.job} - ${work.playing}` : work.job
                            })()
                        }
                        
                        works.push(data)
                    
                    } catch (error) {
                        console.log(error)
                    }
                })

                isLoading(false)
            })
            .catch(error => console.log(error))

        // Create bookmark
        postman.subscribe('createBookmark', (label) => new Promise(async (resolve, reject) => {
            const nconst = utils.url2id(url())

            try {
                await personBookmarkService.createPersonBookmark(nconst, label)
                updateBookmarked()
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        // Bookmarked status
        const updateBookmarked = async () => {
            const nconst = utils.url2id(url())
            
            try {
                const result = await personBookmarkService.personBookmarkExists(nconst)
                isBookmarked(result)

            } catch (error) {
                alert(error.message)
                isBookmarked(false)
            }
        }

        // Navigation
        const returnToLastView = () => postman.publish('lastView', null)

        return {
            url,
            lastName,
            firstName,
            birthYear,
            deathYear,
            works,
            hasWorks,
            isLoading,
            isBookmarked,
            updateBookmarked,
            returnToLastView
        }
    }
})