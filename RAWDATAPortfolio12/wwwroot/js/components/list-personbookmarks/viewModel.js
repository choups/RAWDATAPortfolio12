
define(['knockout', 'postman', 'personBookmarkService', 'utils'], (ko, postman, personBookmarkService, utils) => {
    return function() {
        const totalPages = ko.observable(0)
        const prevUrl = ko.observable(null)
        const nextUrl = ko.observable(null)
        const bookmarks = ko.observableArray([])
        
        const isLoading = ko.observable(true)
        
        // Data        
        postman.subscribe('updateData', (data) => {
            data.result.forEach(bookmark => {
                bookmark.date = new Date(bookmark.date).toLocaleDateString()
            })

            data.result.sort((a, b) => a.date < b.date)
            
            totalPages(data.totalPages)
            prevUrl(data.previousPage)
            nextUrl(data.nextPage)
            bookmarks(data.result)

            isLoading(false)
        })

        // Update bookmark
        const putId = ko.observable(null)
        const putLabel = ko.observable(null)

        const updatePutData = (url, label) => {
            const nconst = utils.url2id(url)
            putId(nconst)
            putLabel(label)
        }

        postman.subscribe('updateBookmark', (label) => new Promise(async (resolve, reject) => {
            const nconst = putId()
            const index = bookmarks().findIndex(bookmark => bookmark.personUrl.includes(nconst))

            const newBookmark = (() => {
                const bookmark = Object.create(bookmarks()[index]) // clone object
                bookmark.label = label
                return bookmark
            })()

            bookmarks.replace(bookmarks()[index], newBookmark)

            try {
                await personBookmarkService.updatePersonBookmark(nconst, label)
                resolve()

            } catch (error) {
                reject(error)
            }
        }))

        // Delete bookmark
        const deleteId = ko.observable(null)

        const updateDeleteData = (url) => {
            const tconst = utils.url2id(url)
            deleteId(tconst)
        }

        postman.subscribe('deleteBookmark', async () => {
            const nconst = deleteId()
            bookmarks.remove(bookmark => bookmark.personUrl.includes(nconst))

            try {
                await personBookmarkService.deletePersonBookmark(nconst)

            } catch (error) {
                console.log(error)
            }
        })

        // Details
        const seeDetails = (url) => {
            postman.publish('changeView', {
                name: 'person-details',
                params: { url }
            })
        }

        return {
            totalPages,
            prevUrl,
            nextUrl,
            bookmarks,
            putLabel,
            updatePutData,
            updateDeleteData,
            seeDetails,
            isLoading
        }
    }
})