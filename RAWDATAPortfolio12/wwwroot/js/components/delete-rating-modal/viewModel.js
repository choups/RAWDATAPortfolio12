
define(['knockout', 'bootstrap', 'postman'], (ko, bootstrap, postman) => {

    return function() {
        const modal = document.querySelector('#deleteRatingModal')
        
        const question = ko.observable(null)

        const deleteRating = () => {
            postman.publish('deleteRating', null)
            bootstrap.Modal.getInstance(modal).hide()
        }

        return {
            question,
            deleteRating
        }
    }
})