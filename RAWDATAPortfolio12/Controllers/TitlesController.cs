﻿using System;
using WebService.Models.Titles;
using WebService.Helpers;
using Microsoft.AspNetCore.Mvc;
using DataService.Interfaces;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using WebService.Services;
using WebService.Models;
using WebService.Models.Search;
using System.Collections.Generic;

namespace WebService.Controllers {

    [ApiController]
    [Route("api/titles")]
    public class TitlesController : Controller {
        private readonly ITitleDataService dataService;
        private readonly IUriService uriService;

        public TitlesController(ITitleDataService dataService, IUriService uriService) {
            this.dataService = dataService;
            this.uriService = uriService;
        }

        // GET /api/titles/<id>
        // get single title from id
        [HttpGet("{titleId}", Name = nameof(GetTitle))]
        public IActionResult GetTitle(string titleId) {
            var title = dataService.GetTitle(titleId);
            var urlContext = new UrlContext(HttpContext);

            return title == null ? NotFound() : Ok(TitleViewModel.FromTitle(title, urlContext));
        }

        // GET /api/titles/<id>/similar
        // get similar titles from title id
        [HttpGet("{Id}/similar")]
        public IActionResult GetSimilarTitles(string Id, [FromQuery] PagingParametersFilter pagingParametersFilter)
        {
            if (dataService.GetTitle(Id) == null)
                return NotFound();

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (titles, count) = dataService.GetSimilarTitles(Id, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var titlesViewModels = titles.Select(x => ScoredTitleViewModel.FromTitle(x, urlContext));

            return titles == null ? NotFound() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<ScoredTitleViewModel>>(titlesViewModels, count, pagingParametersFilter, uriService, route, header));
        }

        // GET /api/titles
        // get a list of all titles
        [HttpGet]
        public IActionResult GetTitles([FromQuery] PagingParametersFilter pagingParametersFilter) {
            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (titles, count) = dataService.GetPagedTitles(pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var titlesViewModels = titles.Select(x => GetTitleForPagingViewModel.FromTitlePaging(x, urlContext));

            return titles == null ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetTitleForPagingViewModel>>(titlesViewModels, count, pagingParametersFilter, uriService, route, header));
        }
    }
}
