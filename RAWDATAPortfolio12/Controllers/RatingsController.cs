﻿using System;
using Microsoft.AspNetCore.Mvc;
using DataService.Interfaces;
using WebService.Models.Ratings;
using DataService.Domain;
using System.Linq;
using WebService.Attributes;
using WebService.Models;
using System.Collections.Generic;
using WebService.Services;
using WebService.Helpers;

namespace WebService.Controllers {

    [Route("api")]
    public class RatingsController: Controller {
        private readonly IRatingDataService dataService;
        private readonly IUriService uriService;

        public RatingsController(IRatingDataService dataService, IUriService uriService) {
            this.dataService = dataService;
            this.uriService = uriService;
        }


        // GET /api/users/rating
        // get user ratings
        [Authorization]
        [HttpGet("users/rating")]
        public IActionResult GetRatingsByUserId([FromQuery] PagingParametersFilter pagingParametersFilter) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (ratings, count) = dataService.GetRatingsByUserId(userId, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var ratingViewModels = ratings
                .Select(x => GetRatingViewModel.FromRating(x, urlContext));

            return ratings == null ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetRatingViewModel>>(ratingViewModels, count, pagingParametersFilter, uriService, route, header));
        }


        // GET /api/titles/rating/title/<id>
        // get ratings by title id
        [HttpGet("titles/{titleId}/rating")]
        public IActionResult GetRatingsByTitleId([FromRoute] string titleId, [FromQuery] PagingParametersFilter pagingParametersFilter) {
            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);


            var (ratings, count) = dataService.GetRatingsByTitleId(titleId, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var ratingViewModels = ratings
                .Select(rating => GetRatingViewModel.FromRating(rating, urlContext));

            return ratings == null ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetRatingViewModel>>(ratingViewModels, count, pagingParametersFilter, uriService, route, header));
        }


        // GET /api/users/rating/<id>
        // get single rating by id
        [Authorization]
        [HttpGet("users/rating/{titleId}")]
        public IActionResult GetRating([FromRoute] string titleId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;
            var urlContext = new UrlContext(HttpContext);

            var rating = dataService.GetRating(userId, titleId);

            return rating != null ? Ok(GetRatingViewModel.FromRating(rating, urlContext)) : NoContent();
        }


        // POST /api/users/rating
        // create single rating
        [Authorization]
        [HttpPost("users/rating")]
        public IActionResult CreateRating([FromBody] CreateRatingViewModel viewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!CreateRatingViewModel.Validate(viewModel))
                return BadRequest();

            Rating rating = dataService.GetRating(userId, viewModel.TitleId);

            if (rating != null)
                return Conflict();

            var urlContext = new UrlContext(HttpContext);

            if (viewModel.Comment == null)
                rating = dataService.AddRating(userId, viewModel.TitleId, viewModel.Rate);
            else
                rating = dataService.AddRating(userId, viewModel.TitleId, viewModel.Rate, viewModel.Comment);

            return rating != null ? Created("", GetRatingViewModel.FromRating(rating, urlContext)) : NotFound();
        }


        // PUT /api/users/rating/<id>
        // update single rating by id
        [Authorization]
        [HttpPut("users/rating/{titleId}")]
        public IActionResult UpdateRating([FromRoute] string titleId, [FromBody] UpdateRatingViewModel viewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!UpdateRatingViewModel.Validate(viewModel))
                return BadRequest();

            var found = dataService.UpdateRating(userId, titleId, viewModel.Rate, viewModel.Comment);

            return found ? Ok() : NotFound();
        }


        // DELETE /api/users/rating/<id>
        // delete single rating by id
        [Authorization]
        [HttpDelete("users/rating/{titleId}")]
        public IActionResult DeleteRating([FromRoute] string titleId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            return dataService.DeleteRating(userId, titleId) ? Ok() : NotFound();
        }
    }
}
