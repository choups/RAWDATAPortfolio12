﻿using System;
using WebService.Helpers;
using Microsoft.AspNetCore.Mvc;
using DataService.Interfaces;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Routing;
using WebService.Services;
using WebService.Models.Person;
using WebService.Models;
using System.Collections.Generic;

namespace WebService.Controllers {

    [ApiController]
    [Route("api/persons")]
    public class PersonsController: Controller {
        private readonly IPersonDataService dataService;
        private readonly IUriService uriService;

        public PersonsController(IPersonDataService dataService, IUriService uriService) {
            this.dataService = dataService;
            this.uriService = uriService;
        }


        // GET /api/persons/<id>
        // get single person by id
        [HttpGet("{personId}", Name = nameof(GetPerson))]
        public IActionResult GetPerson(string personId) {
            var person = dataService.GetPerson(personId);
            var urlContext = new UrlContext(HttpContext);

            return person == null ? NotFound() : Ok(PersonViewModel.FromPerson(person, urlContext));
        }


        // GET /api/persons/
        // get persons
        [HttpGet]
        public IActionResult GetPersons([FromQuery] PagingParametersFilter pagingParametersFilter) {

            var route = Request.Path.Value;
            var header = Response.Headers;
            var urlContext = new UrlContext(HttpContext);

            var (persons, count) = dataService.GetPagedPersons(pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var personsViewModels = persons.Select(x => GetPersonForPagingViewModel.FromPerson(x, urlContext));

            return persons == null ? NotFound() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetPersonForPagingViewModel>>(personsViewModels, count, pagingParametersFilter, uriService, route, header));
        }
    }
}
