﻿using System;
using DataService.Domain;
using DataService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WebService.Models;
using WebService.Helpers;
using WebService.Models.Search;
using WebService.Services;
using System.Collections.Generic;

namespace WebService.Controllers
{
    [ApiController]
    [Route("api")]
    public class SearchController : Controller
    {
        private readonly ISearchDataService dataService;
        private readonly IUriService uriService;

        public SearchController(ISearchDataService dataService, IUriService uriService)
        {
            this.dataService = dataService;
            this.uriService = uriService;
        }
        
        [HttpPost("titles")]
        public IActionResult SearchTitles([FromBody] SearchStringViewModel searchModel, [FromQuery] PagingParametersFilter pagingParametersFilter)
        {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user?.UserId ?? -1;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (titles, count) = dataService.GetTitleFromString(searchModel.StringSearch, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize, userId);
            var titlesViewModels = titles.Select(x => ScoredTitleViewModel.FromTitle(x, urlContext));

            return titles == null ? NotFound() : (titles.Count == 0 ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<ScoredTitleViewModel>>(titlesViewModels, count, pagingParametersFilter, uriService, route, header)));
        } 


        [HttpPost("persons")]
        public IActionResult SearchPersons([FromBody] SearchStringViewModel searchModel, [FromQuery] PagingParametersFilter pagingParametersFilter)
        {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user?.UserId ?? -1;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (persons, count) = dataService.GetPersonFromString(searchModel.StringSearch, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize, userId);
            var personViewModel = persons.Select(x => ScoredPersonViewModel.FromPerson(x, urlContext));

            return persons == null ? NotFound() : (persons.Count == 0 ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<ScoredPersonViewModel>>(personViewModel, count, pagingParametersFilter, uriService, route, header)));
        }
    }
}
