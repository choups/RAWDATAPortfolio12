﻿using Microsoft.AspNetCore.Mvc;
using System;
using WebService.Models.User;
using WebService.Services;
using DataService.Interfaces;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using WebService.Attributes;
using DataService.Domain;
using System.Linq;
using WebService.Models.SearchHistory;
using WebService.Models;
using System.Collections.Generic;
using WebService.Helpers;
using Newtonsoft.Json;
using WebService.Models.Search;

namespace WebService.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : Controller {
        public readonly IUserDataService dataService;
        private readonly IConfiguration configuration;
        private readonly IUriService uriService;

        public UsersController(IUserDataService dataService, IConfiguration configuration, IUriService uriService) {
            this.dataService = dataService;
            this.configuration = configuration;
            this.uriService = uriService;
        }


        // POST /api/users/register
        // create new user
        [HttpPost("register")]
        public IActionResult Register(CreateUserViewModel viewModel) {
            if (!CreateUserViewModel.Validate(viewModel))
                return BadRequest();

            if (dataService.GetUserByLogin(viewModel.Login) != null)
                return Conflict();

            if (!int.TryParse(configuration.GetSection("Auth:PasswordSize").Value, out int pwdSize) || pwdSize == 0)
                throw new ArgumentException("No Password Size");

            var salt = PasswordService.GenerateSalt(pwdSize);
            var pwd = PasswordService.HashPassword(viewModel.Password, salt, pwdSize);

            dataService.AddUser(viewModel.Login, pwd, salt);

            return CreatedAtRoute(null, new { viewModel.Login });
        }


        // POST /api/users/login
        // login
        [HttpPost("login")]
        public IActionResult Login(CreateUserViewModel viewModel) {
            if (!CreateUserViewModel.Validate(viewModel))
                return BadRequest();

            var user = dataService.GetUserByLogin(viewModel.Login);
            if (user == null)
                return NotFound();

            if (!int.TryParse(configuration.GetSection("Auth:PasswordSize").Value, out int pwdSize) || pwdSize == 0)
                throw new ArgumentException("No Password Size");

            var secret = configuration.GetSection("Auth:Secret").Value;
            if (string.IsNullOrEmpty(secret))
                throw new ArgumentException("No Secret");

            var password = PasswordService.HashPassword(viewModel.Password, user.Salt, pwdSize);

            if (password != user.Password)
                return Unauthorized();

            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.UTF8.GetBytes(secret);
            var tokenDescription = new SecurityTokenDescriptor {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.UserId.ToString()) }),
                Expires = DateTime.Now.AddSeconds(45),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var securityToken = tokenHandler.CreateToken(tokenDescription);
            var token = tokenHandler.WriteToken(securityToken);

            return Ok(new { viewModel.Login, token });
        }

        [Authorization]
        [HttpPut("password")]
        //Ask for the login and the password to have more safety (so that you have to specify your login and password even if you are already login)
        public IActionResult ChangePassword(UserPasswordViewModel viewModel)
        {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!int.TryParse(configuration.GetSection("Auth:PasswordSize").Value, out int pwdSize) || pwdSize == 0)
                throw new ArgumentException("No Password Size");

            var oldpassword = PasswordService.HashPassword(viewModel.OldPassword, user.Salt, pwdSize);

            if (oldpassword != user.Password)
                return Unauthorized();

            var salt = PasswordService.GenerateSalt(pwdSize);
            var newPassword = PasswordService.HashPassword(viewModel.NewPassword, salt, pwdSize);

            dataService.UpdatePassword(userId, newPassword, salt);

            return Ok();
        }

        [Authorization]
        [HttpPut("login")]
        //Ask for the login and the password to have more safety (so that you have to specify your login and password even if you are already login)
        public IActionResult ChangeLogin(UpdateUserLoginViewModel viewModel)
        {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!int.TryParse(configuration.GetSection("Auth:PasswordSize").Value, out int pwdSize) || pwdSize == 0)
                throw new ArgumentException("No Password Size");

            var oldpassword = PasswordService.HashPassword(viewModel.Password, user.Salt, pwdSize);

            if (oldpassword != user.Password)
                return Unauthorized();

            if (dataService.GetUserByLogin(viewModel.NewLogin) != null)
                return Conflict("Login already used");

            dataService.UpdateLogin(userId, viewModel.NewLogin);

            return Ok();
        }


        // DELETE /api/users/<id>
        // delete their account
        [Authorization]
        [HttpDelete]
        public IActionResult DeleteUser() {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var found = dataService.DeleteUser(userId);

            return found ? Ok() : NotFound();
        }

        // GET /api/users/history
        // get user's full history
        [Authorization]
        [HttpGet("history")]
        public IActionResult GetSearchHistory([FromQuery] PagingParametersFilter pagingParametersFilter) {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (history, count) = dataService.GetSearchHistory(userId, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var historyViewModels = history
                .Select(x => GetSearchHistoryViewModel.FromSearchHistory(x))
                .ToList();

            return history == null ? NotFound() : (count == 0 ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<List<GetSearchHistoryViewModel>>(historyViewModels, count, pagingParametersFilter, uriService, route, header)));
        }


        // POST /api/users/history
        // get user's history starting with the querry
        [Authorization]
        [HttpPost("history")]
        public IActionResult GetSearchHistoryQuery([FromBody] SearchStringViewModel searchModel, [FromQuery] PagingParametersFilter pagingParametersFilter)
        {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            (ICollection<SearchHistory> history, int count) = dataService.GetSearchHistoryQuery(userId, searchModel.StringSearch, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var historyViewModels = history
                .Select(x => GetSearchHistoryViewModel.FromSearchHistory(x))
                .ToList();

            return history == null ? NotFound() : (count == 0 ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<List<GetSearchHistoryViewModel>>(historyViewModels, count, pagingParametersFilter, uriService, route, header)));
        }

        // GET /api/users/history/{searchType}
        // get user's history by category (title, person...)
        [Authorization]
        [HttpGet("history/{searchType}")]
        public IActionResult GetSearchHistory(char searchType, [FromQuery] PagingParametersFilter pagingParametersFilter) {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (history, count) = dataService.GetSearchHistory(userId, searchType, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var historyViewModels = history
                .Select(x => GetSearchHistoryViewModel.FromSearchHistory(x))
                .ToList();

            return history == null ? NotFound() : (count == 0 ? NoContent() : Ok(PaginationHelper.CreatePaginatedResponse<List<GetSearchHistoryViewModel>>(historyViewModels, count, pagingParametersFilter, uriService, route, header)));
        }

        // DELETE /api/users/history
        // delete user's history
        [Authorization]
        [HttpDelete("history")]
        public IActionResult DeleteSearchHistory() {
            var user = Request.HttpContext.Items["User"] as User;
            int userId = user.UserId;

            dataService.DeleteSearchHistory(userId);

            return Ok();
        }
    }
}
