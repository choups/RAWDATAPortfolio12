﻿using System;
using Microsoft.AspNetCore.Mvc;
using DataService.Interfaces;
using WebService.Models.TitleBookmarks;
using DataService.Domain;
using System.Linq;
using WebService.Attributes;
using WebService.Models;
using WebService.Helpers;
using WebService.Services;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebService.Controllers {

    [Route("api/users/bookmark/title")]
    public class TitleBookmarksController : Controller {
        private readonly IBookmarkDataService dataService;
        private readonly IUriService uriService;

        public TitleBookmarksController(IBookmarkDataService dataService, IUriService uriService) {
            this.dataService = dataService;
            this.uriService = uriService;
        }


        // GET /api/users/bookmarks/title/
        // get user titlebookmarks
        [Authorization]
        [HttpGet]
        public IActionResult GetTitleBookmarks([FromQuery] PagingParametersFilter pagingParametersFilter) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (titleBookmarks, count) = dataService.GetTitleBookmarks(userId, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var titleBookmarkViewModels =
                titleBookmarks
                .Select(x => GetTitleBookmarkViewModel.FromTitleBookmark(x, urlContext));

            return titleBookmarks == null ? NotFound() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetTitleBookmarkViewModel>>(titleBookmarkViewModels, count, pagingParametersFilter, uriService, route, header));
        }


        // GET /api/users/bookmarks/title/<id>
        // get single user titlebookmark by id
        [Authorization]
        [HttpGet("{titleId}")]
        public IActionResult GetTitleBookmark([FromRoute] string titleId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;
            var urlContext = new UrlContext(HttpContext);

            var titleBookmark = dataService.GetTitleBookmark(userId, titleId);
            var found = titleBookmark != null;

            return found ? Ok(GetTitleBookmarkViewModel.FromTitleBookmark(titleBookmark, urlContext)) : NoContent();
        }

        // POST /api/users/bookmarks/title/
        // create single titlebookmark
        [Authorization]
        [HttpPost]
        public IActionResult CreateTitleBookmark([FromBody] CreateTitleBookmarkViewModel titleBookmarkViewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (dataService.GetTitleBookmark(userId, titleBookmarkViewModel.TitleId) != null)
                return Conflict();

            if (!CreateTitleBookmarkViewModel.Validate(titleBookmarkViewModel))
                return BadRequest();

            var urlContext = new UrlContext(HttpContext);
            var titleBookmark = dataService.AddTitleBookmark(userId, titleBookmarkViewModel.TitleId, titleBookmarkViewModel.Label);
            //var uri = $"{Request.Host}{Request.Path}/{titleBookmark.TitleId}";
            return titleBookmark != null ? Created("", GetTitleBookmarkViewModel.FromTitleBookmark(titleBookmark, urlContext)) : NotFound();
        }


        // PUT /api/users/bookmarks/title/<id>
        // update single titlebookmark by id
        [Authorization]
        [HttpPut("{titleId}")]
        public IActionResult UpdateTitleBookmark([FromRoute] string titleId, [FromBody] UpdateTitleBookmarkViewModel titleBookmarkViewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!UpdateTitleBookmarkViewModel.Validate(titleBookmarkViewModel))
                return BadRequest();

            var found = dataService.UpdateTitleBookmark(userId, titleId, titleBookmarkViewModel.Label);
            return found ? Ok() : NotFound();
        }


        // DELETE /api/users/bookmarks/title/<id>
        // delete single titlebookmark by id
        [Authorization]
        [HttpDelete("{titleId}")]
        public IActionResult DeleteTitleBookmark([FromRoute] string titleId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var found = dataService.DeleteTitleBookmark(userId, titleId);

            return found ? Ok() : NotFound();
        }
    }
}
