﻿using System;
using Microsoft.AspNetCore.Mvc;
using DataService.Interfaces;
using WebService.Models.PersonBookmarks;
using DataService.Domain;
using System.Linq;
using WebService.Attributes;
using WebService.Models;
using WebService.Helpers;
using WebService.Services;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebService.Controllers {

    [Route("api/users/bookmark/person")]
    public class PersonBookmarksController: Controller {
        private readonly IBookmarkDataService dataService;
        private readonly IUriService uriService;

        public PersonBookmarksController(IBookmarkDataService dataService, IUriService uriService) {
            this.dataService = dataService;
            this.uriService = uriService;
        }


        // GET /api/users/bookmark/person
        // get user personbookmarks
        [Authorization]
        [HttpGet]
        public IActionResult GetPersonBookmarks([FromQuery] PagingParametersFilter pagingParametersFilter) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var header = Response.Headers;
            var route = Request.Path.Value;
            var urlContext = new UrlContext(HttpContext);

            var (personBookmarks, count) = dataService.GetPersonBookmarks(userId, pagingParametersFilter.PageNumber, pagingParametersFilter.PageSize);
            var personBookmarkViewModels =
                personBookmarks
                .Select(x => GetPersonBookmarkViewModel.FromPersonBookmark(x, urlContext));

            return personBookmarks == null ? NotFound() : Ok(PaginationHelper.CreatePaginatedResponse<IEnumerable<GetPersonBookmarkViewModel>>(personBookmarkViewModels, count, pagingParametersFilter, uriService, route, header));
        }


        // GET /api/users/bookmark/person/<id>
        // get single user personbookmark by id
        [Authorization]
        [HttpGet("{personId}")]
        public IActionResult GetPersonBookmark([FromRoute] string personId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;
            var urlContext = new UrlContext(HttpContext);

            var personBookmark = dataService.GetPersonBookmark(userId, personId);
            var found = personBookmark != null;

            return found ? Ok(GetPersonBookmarkViewModel.FromPersonBookmark(personBookmark, urlContext)) : NoContent();
        }


        // POST /api/users/bookmark/person/
        // create single user personbookmark 
        [Authorization]
        [HttpPost]
        public IActionResult CreatePersonBookmark([FromBody] CreatePersonBookmarkViewModel personBookmarkViewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!CreatePersonBookmarkViewModel.Validate(personBookmarkViewModel))
                return BadRequest();

            var urlContext = new UrlContext(HttpContext);
            var personBookmark = dataService.AddPersonBookmark(userId, personBookmarkViewModel.PersonId, personBookmarkViewModel.Label);

            return personBookmark != null ? Created("", GetPersonBookmarkViewModel.FromPersonBookmark(personBookmark, urlContext)) : NotFound();
        }


        // PUT /api/users/bookmark/person/<id>
        // update single user personbookmark by id
        [Authorization]
        [HttpPut("{personId}")]
        public IActionResult UpdatePersonBookmark([FromRoute] string personId, [FromBody] UpdatePersonBookmarkViewModel personBookmarkViewModel) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            if (!UpdatePersonBookmarkViewModel.Validate(personBookmarkViewModel))
                return BadRequest();

            var found = dataService.UpdatePersonBookmark(userId, personId, personBookmarkViewModel.Label);
            return found ? Ok() : NotFound();
        }


        // DELETE /api/users/bookmark/person/<id>
        // delete single user personbookmark by id
        [Authorization]
        [HttpDelete("{personId}")]
        public IActionResult DeletePersonBookmark([FromRoute] string personId) {
            var user = Request.HttpContext.Items["User"] as User;
            var userId = user.UserId;

            var found = dataService.DeletePersonBookmark(userId, personId);

            return found ? Ok() : NotFound();
        }
    }
}
