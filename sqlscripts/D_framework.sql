/* ##### FUNCTIONALITIES ##### */

-- D.1 Basic framework functionality
--Function for changing the password
CREATE or REPLACE PROCEDURE change_password(user_id_ INT, new_password VARCHAR(100))
LANGUAGE PLPGSQL AS
$$
	BEGIN
		UPDATE _User SET "pwd" = new_password WHERE _User."user_id" = user_id_;
	END;
$$;

-- D.2 Simple Search
CREATE or REPLACE FUNCTION string_search(s VARCHAR)
RETURNS TABLE (
    tconst VARCHAR(10),
    originalTitle VARCHAR(200)
)
LANGUAGE PLPGSQL AS
$$
    BEGIN
        RETURN query
            SELECT Title.tconst, Title.originalTitle
            FROM Title
            NATURAL JOIN wi
            WHERE (wi.word LIKE '%' || LOWER(s) || '%' AND wi.field = 't') OR (wi.word LIKE '%' || LOWER(s) || '%' AND wi.field = 'p');
    END;
$$;


-- D.3 Title rating
CREATE or REPLACE PROCEDURE add_rating(user_id INT, title_id VARCHAR(10), rate INT, date TIMESTAMP)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		INSERT INTO Rating values(title_id, user_id, rate, date);
	END;
$$;

CREATE or REPLACE PROCEDURE add_rating(user_id INT, title_id VARCHAR(10), rate INT, date TIMESTAMP)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		INSERT INTO Rating values(title_id, user_id, rate, date);
	END;
$$;


-- D.4 Structured string search
CREATE or REPLACE FUNCTION structured_string_search(s1 VARCHAR, s2 VARCHAR, s3 VARCHAR, s4 VARCHAR)
RETURNS TABLE (
    tconst VARCHAR(10),
    originalTitle VARCHAR(200)
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
        RETURN QUERY
			SELECT DISTINCT Title.tconst, Title.originalTitle
			FROM Title
			INNER JOIN Synopsis
			ON Title.tconst = Synopsis.tconst
			INNER JOIN _Work
			ON Title.tconst = _Work.tconst
			LEFT OUTER JOIN Acting
			ON _Work.nconst = Acting.nconst AND _Work.tconst = Acting.tconst
			INNER JOIN Person
			ON _Work.nconst = Person.nconst
			WHERE LOWER(Title.originalTitle) LIKE '%' || LOWER(s1) || '%'
			AND LOWER(Synopsis.plot) LIKE '%' || LOWER(s2) || '%'
			AND LOWER(Acting.playing) LIKE '%' || LOWER(s3) || '%'
			AND (LOWER(Person.firstname) LIKE '%' || LOWER(s4) || '%' OR LOWER(Person.lastname) LIKE '%' || LOWER(s4) || '%');
    END;
$$;

-- D.5 Finding names
CREATE or REPLACE FUNCTION simple_find_name(s VARCHAR)
RETURNS TABLE (
    nconst VARCHAR(10),
    lastName VARCHAR(100),
	firstName VARCHAR(100)
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
        RETURN QUERY
            SELECT DISTINCT Person.nconst, Person.lastname, Person.firstname
			FROM Person
			WHERE LOWER(Person.lastname) LIKE '%' || LOWER(s) || '%' OR LOWER(Person.firstname) LIKE '%' || LOWER(s) || '%';
    END;
$$;

CREATE or REPLACE FUNCTION simple_find_name_from_character(s VARCHAR)
RETURNS TABLE (
    nconst VARCHAR(10),
    lastName VARCHAR(100),
	firstName VARCHAR(100)
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
        RETURN QUERY
            SELECT DISTINCT Person.nconst, Person.lastname, Person.firstname
			FROM Person
			NATURAL JOIN _Work
			NATURAL JOIN Acting
			WHERE LOWER(Acting.playing) LIKE '%' || LOWER(s) || '%';
    END;
$$;


-- D.6 Finding co-players
CREATE OR REPLACE FUNCTION coplayers(person_id VARCHAR(10))
RETURNS TABLE (
    nconst VARCHAR(10),
    lastName VARCHAR(100),
	firstName VARCHAR(100),
    frequency bigint
)
LANGUAGE PLPGSQL AS
$$
    BEGIN
		RETURN QUERY
			SELECT p.nconst, p.lastname, p.firstname, COUNT(p.nconst) frequency
			FROM _Work AS w
			INNER JOIN _Work AS w2
			ON w.tconst = w2.tconst
			INNER JOIN Person AS p
			ON w2.nconst = p.nconst
			WHERE w.nconst = person_id AND w.nconst != w2.nconst
			GROUP BY p.nconst, p.lastname, p.firstname
			ORDER BY frequency DESC;
	END;
$$;


-- D.7 Name Rating
CREATE OR REPLACE FUNCTION name_rating()
RETURNS TABLE (
	nconst VARCHAR(10),
	rate FLOAT
)
LANGUAGE PLPGSQL AS
$$
    BEGIN
		RETURN QUERY 
			SELECT _Work.nconst, sum(GeneralRating.averagerating * GeneralRating.numvotes) / sum(GeneralRating.numvotes)
			FROM _Work
			NATURAL JOIN GeneralRating
			GROUP BY _Work.nconst;
	END;
$$;

CREATE OR REPLACE FUNCTION name_rating(person_id VARCHAR(10))
RETURNS FLOAT
LANGUAGE PLPGSQL AS
$$
    BEGIN
		RETURN (
			SELECT sum(GeneralRating.averagerating * GeneralRating.numvotes) / sum(GeneralRating.numvotes)
			FROM _Work
			NATURAL JOIN GeneralRating
			WHERE _Work.nconst = person_id
			GROUP BY _Work.nconst );
	END;
$$;

-- Store the name rating in the table
INSERT INTO PersonRating
SELECT Person.nconst, nr.rate FROM Person INNER JOIN name_rating() AS nr ON Person.nconst = nr.nconst;


-- D.8 Popular actors
CREATE OR REPLACE FUNCTION popular_actor_from_movie(title_id VARCHAR(10))
RETURNS TABLE (
	nconst VARCHAR(10),
	rate FLOAT)
LANGUAGE PLPGSQL AS
$$
    BEGIN
			RETURN QUERY 
				SELECT _Work.nconst, PersonRating.rate
				FROM PersonRating
				INNER JOIN _Work
				ON _Work.nconst = PersonRating.nconst
				WHERE _Work.tconst = title_id
				ORDER BY rate DESC;
		end;
$$;

CREATE OR REPLACE FUNCTION popular_actor_from_coplayed(name_id VARCHAR(10))
RETURNS TABLE (
	nconst VARCHAR(10),
	rate FLOAT)
LANGUAGE PLPGSQL AS
$$
    BEGIN
			RETURN QUERY 
				SELECT co.nconst, PersonRating.rate
				FROM PersonRating
				INNER JOIN coplayers(name_id) AS co
				ON PersonRating.nconst = co.nconst
				ORDER BY rate DESC;
		end;
$$;


-- D.9 Similar movies
CREATE or REPLACE FUNCTION similar_movie(movie_id VARCHAR(10))
RETURNS TABLE (
	tconst VARCHAR(10),
	score FLOAT,
	title VARCHAR(100),
	releaseYear VARCHAR(4),
	mediaType VARCHAR(20),
	adult VARCHAR(10),
	posterUrl TEXT
)
LANGUAGE PLPGSQL AS
$$
	DECLARE
		rec record;
		w TEXT;
	BEGIN
		w = '';

		FOR rec IN SELECT word FROM wi WHERE wi.tconst = movie_id
		LOOP
			w = w || ' ' || rec.word;
		END LOOP;

		RETURN QUERY
			SELECT * FROM search_tfidf(VARIADIC regexp_split_to_array(w, ' '));
	END;
$$;


-- D.10 Inverted indexing and D.12 Best Match querying
CREATE or REPLACE FUNCTION inverted_indexing(VARIADIC s TEXT[])
RETURNS TABLE (
	tconst VARCHAR(10),
	rank bigint,
	title VARCHAR(100)
)
LANGUAGE PLPGSQL AS
$$
		DECLARE
				elem TEXT;
		BEGIN
				CREATE TEMP TABLE union_table (tconst VARCHAR(10), relevance INTEGER);
				FOREACH elem IN ARRAY s
				LOOP
						INSERT INTO union_table
						SELECT distinct wi.tconst, 1 relevance FROM wi WHERE wi.word LIKE '%' || LOWER(elem) || '%';
				END LOOP;
				
				RETURN QUERY
						SELECT t.tconst, sum(relevance) rank, originalTitle
						FROM Title t, union_table w
						WHERE t.tconst = w.tconst
						GROUP BY t.tconst, originalTitle
						ORDER BY rank DESC;
				DROP TABLE union_table;
		END;
$$;


-- D.11 Exact match querying
CREATE or REPLACE FUNCTION exact_match(VARIADIC s TEXT[])
RETURNS TABLE (
	tconst VARCHAR(10),
	title VARCHAR(100)
)
LANGUAGE PLPGSQL AS
$$
		BEGIN
		RETURN QUERY
				SELECT ta.tconst, ta.title
				FROM inverted_indexing(VARIADIC s) ta
				WHERE ta.rank = "cardinality"(s);
		END;
$$;


-- D.13 Word-to-words querying
CREATE or REPLACE FUNCTION word_to_words_1st(VARIADIC s TEXT[])
RETURNS TABLE (
	word TEXT,
	frequency FLOAT
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
	RETURN QUERY
		SELECT wi.word, cast(wi.occ AS FLOAT) / cast(count(wi.word) AS FLOAT) AS frequency
		FROM inverted_indexing(VARIADIC s) AS inv
		NATURAL JOIN wi
		GROUP BY wi.word, wi.occ
		ORDER BY frequency;
	END;
$$;

CREATE or REPLACE FUNCTION word_to_words(VARIADIC s TEXT[])
RETURNS TABLE (
	word TEXT,
	frequency FLOAT
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
	RETURN QUERY
		SELECT wi.word, cast(count(wi.word) AS FLOAT) / (SELECT count(distinct tconst) FROM inverted_indexing(VARIADIC s)) frequency
		FROM inverted_indexing(VARIADIC s) AS inv
		NATURAL JOIN wi
		GROUP BY wi.word
		ORDER BY frequency desc;
	END;
$$;


-- Function to get the lenght of the title, plot, caracter and related persons
CREATE or REPLACE FUNCTION number_word()
RETURNS TABLE (
	tconst VARCHAR(10),
	num bigint
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		RETURN QUERY
		SELECT Title.tconst, sum(wi.occ)
		FROM Title
		NATURAL JOIN wi
		GROUP BY Title.tconst;
	END;
$$;

-- Filling the WordsNumber table
INSERT INTO WordsNumber SELECT * FROM number_word();


-- D.14 TFIDF search
CREATE or REPLACE FUNCTION search_tfidf(VARIADIC s TEXT[])
RETURNS TABLE (
	tconst VARCHAR(10),
	score FLOAT,
	title VARCHAR(100),
	releaseYear VARCHAR(4),
	mediaType VARCHAR(20),
	adult VARCHAR(10),
	posterUrl TEXT
)
LANGUAGE PLPGSQL AS
$$
	DECLARE
		elem TEXT;
	BEGIN
		CREATE TEMP TABLE union_table (tconst character(10), tfidf_score FLOAT);
		FOREACH elem IN ARRAY s
		LOOP
			INSERT INTO union_table
			SELECT wi.tconst, LOG(1.0 + cast(wi.occ AS FLOAT)/WordsNumber.num)/cast((SELECT count(word) FROM wi WHERE word LIKE '%' || LOWER(elem) || '%') AS FLOAT) FROM wi NATURAL JOIN WordsNumber WHERE wi.word LIKE '%' || LOWER(elem) || '%';
		END LOOP;
		
		RETURN QUERY
			SELECT Title.tconst, sum(w.tfidf_score) score, Title.originalTitle, Title.releaseYear, Title.mediaType, a.tconst, p.url
			FROM Title
			NATURAL JOIN union_table w
			LEFT OUTER JOIN Adult AS a
			ON Title.tconst = a.tconst
			LEFT OUTER JOIN Poster as p
			ON Title.tconst = p.tconst
			GROUP BY Title.tconst, Title.originalTitle, Title.releaseYear, Title.mediaType, a.tconst, p.url
			ORDER BY score desc;

		DROP TABLE union_table;
	END;
$$;


-- Search Function
CREATE or REPLACE FUNCTION search_title(s TEXT)
RETURNS TABLE (
	tconst VARCHAR(10),
	score FLOAT,
	title VARCHAR(100),
	releaseYear VARCHAR(4),
	mediaType VARCHAR(20),
	adult VARCHAR(10),
	posterUrl TEXT
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		RETURN QUERY 
			SELECT * FROM search_tfidf(VARIADIC regexp_split_to_array(s, ' '));
	END;
$$;

CREATE or REPLACE FUNCTION search_title(s TEXT, userId INTEGER)
RETURNS TABLE (
	tconst VARCHAR(10),
	score FLOAT,
	title VARCHAR(100),
	releaseYear VARCHAR(4),
	mediaType VARCHAR(20),
	adult VARCHAR(10),
	posterUrl TEXT
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		INSERT INTO SearchHistory (user_id, searchType, searchedText) VALUES (userId, 't', s);
		RETURN QUERY 
			SELECT * FROM search_tfidf(VARIADIC regexp_split_to_array(s, ' '));
	END;
$$;



-- Search Functions for persons
CREATE or REPLACE FUNCTION search_person(s TEXT)
RETURNS TABLE (
	nconst VARCHAR(10),
	score FLOAT,
	firstname VARCHAR(100),
	lastname VARCHAR(100),
	birthYear VARCHAR(4),
	deathYear VARCHAR(4)
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		RETURN QUERY 
			SELECT * FROM find_person(VARIADIC regexp_split_to_array(s, ' '));
	END;
$$;

CREATE or REPLACE FUNCTION search_person(s TEXT, userId INTEGER)
RETURNS TABLE (
	nconst VARCHAR(10),
	score FLOAT,
	firstname VARCHAR(100),
	lastname VARCHAR(100),
	birthYear VARCHAR(4),
	deathYear VARCHAR(4)
)
LANGUAGE PLPGSQL AS
$$
	BEGIN
		INSERT INTO SearchHistory (user_id, searchType, searchedText) VALUES (userId, 'p', s);
		RETURN QUERY 
			SELECT * FROM find_person(VARIADIC regexp_split_to_array(s, ' '));
	END;
$$;


CREATE or REPLACE FUNCTION find_person(VARIADIC s TEXT[])
RETURNS TABLE (
	nconst VARCHAR(10),
	score FLOAT,
	firstname VARCHAR(100),
	lastname VARCHAR(100),
	birthYear VARCHAR(4),
	deathYear VARCHAR(4)
)
LANGUAGE PLPGSQL AS
$$
	DECLARE
		cur CURSOR for select * from search_tfidf(VARIADIC s);
		rec record;
		title_id TEXT;
		elem TEXT;

	BEGIN
		CREATE TEMP TABLE union_table_person (nconst varchar(10), score FLOAT);
		open cur;
		loop
			fetch cur into rec;
			exit when not found;
			insert into union_table_person
			select per.nconst, rec."score"*per."rate" from popular_actor_from_movie(rec."tconst") as per;
		end loop;
		close cur;

		CREATE TEMP TABLE temp_alike_names (nconst VARCHAR(10), score FLOAT, firstname VARCHAR(100), lastname VARCHAR(100), birthYear VARCHAR(4), deathYear VARCHAR(4));
		FOREACH elem IN ARRAY s
		LOOP
			INSERT INTO temp_alike_names
			SELECT p.nconst, 1.0 AS score, p.firstname, p.lastname, p.birthYear, d.deathYear
			FROM person AS p
			LEFT OUTER JOIN death AS d
			ON p.nconst = d.nconst
			WHERE LOWER(p.lastname) LIKE '%' || LOWER(elem) || '%'
			OR LOWER(p.firstname) LIKE '%' || LOWER(elem) || '%';
		END LOOP;
		
		RETURN QUERY
			SELECT * FROM temp_alike_names

			UNION

			select u.nconst, sum(u.score) score, per.firstname, per.lastname, per.birthyear, d.deathyear
			from union_table_person as u
			inner join person as per
			on u.nconst = per.nconst
			left outer join death as d
			on per.nconst = d.nconst
			group by u.nconst, per.firstname, per.lastname, per.birthyear, d.deathyear
			ORDER BY score desc;
		DROP TABLE union_table_person;
		DROP TABLE temp_alike_names;
	END;
$$;

