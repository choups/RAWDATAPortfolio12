/* ##### TABLES CREATION ##### */

CREATE TABLE _User (
	user_id SERIAL PRIMARY KEY,
	login VARCHAR(50) NOT NULL UNIQUE,
	pwd VARCHAR(350) NOT NULL,
	salt VARCHAR(350) NOT NULL
);

CREATE TABLE Rating (
	tconst VARCHAR(10) REFERENCES Title(tconst),
	user_id INTEGER REFERENCES _User(user_id),
	rate INTEGER NOT NULL,
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP::TIMESTAMP,
	CONSTRAINT rating_pk PRIMARY KEY (tconst, user_id)
);

CREATE TABLE _Comment (
	tconst VARCHAR(10),
	user_id INTEGER,
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP::TIMESTAMP,
	content TEXT NOT NULL,
	CONSTRAINT comment_pk PRIMARY KEY (tconst, user_id),
	CONSTRAINT comment_fk FOREIGN KEY (tconst, user_id) REFERENCES Rating(tconst, user_id)
);

CREATE TABLE SearchHistory (
	user_id INTEGER REFERENCES _User(user_id),
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP::TIMESTAMP,
	searchType VARCHAR(1),
	searchedText TEXT,
	CONSTRAINT history_pk PRIMARY KEY (user_id, date)
);

CREATE TABLE TitleBookmark (
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP::TIMESTAMP,
	user_id INTEGER,
	tconst VARCHAR(10) REFERENCES Title(tconst),
	label VARCHAR(100) NOT NULL,
	CONSTRAINT titlebookmark_pk PRIMARY KEY (user_id, tconst)
);

CREATE TABLE PersonBookmark (
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP::TIMESTAMP,
	user_id INTEGER,
	nconst VARCHAR(10) REFERENCES Person(nconst),
	label VARCHAR(100) NOT NULL,
	CONSTRAINT personbookmark_pk PRIMARY KEY (user_id, nconst)
);

CREATE TABLE StopWords (
	word VARCHAR(15),
	PRIMARY KEY (word)
);

-- Creation of the new wi table which count the occurrences of words
DROP TABLE IF EXISTS wi CASCADE;
CREATE TABLE wi (
	tconst VARCHAR(10) REFERENCES Title(tconst),
    field VARCHAR(1),
    word TEXT,
	occ INTEGER,
	PRIMARY KEY (tconst, field, word)
);

CREATE TABLE PersonRating (
	nconst VARCHAR(10) PRIMARY KEY REFERENCES Person(nconst),
	rate FLOAT NOT NULL
);

CREATE TABLE WordsNumber (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	num bigint
);


/* ##### DATA MIGRATION ##### */

-- Fill _User table (just one anonymous user for now)
INSERT INTO _User (login, pwd, salt)
VALUES ('anonymous', 'f6fc84c9f21c24907d6bee6eec38cabab5fa9a7be8c4a7827fe9e56f245bd2d5', 'f6fc84c9f21c24907d6bee6eec38cabab5fa9a7be8c4a7827fe9e56f245bd2d5'),
('choups', 'TJPNQmyFDByXURABflAU6oDlkR0WVYXJwPpibfCjBkjuJQNFVv1PTwelkNTHm59KR1CrUpFBbuPpCAGLCGu6VYL5p0kWZtILCSjPjkFjffum15EpmN1sE/ntY2RRNetLbwhr7abE9xlsLq/hFXlqdkAXqRChQJN+wMucRH9Y0s6kzd+QVHct4i6xcV3gfdwBPpCf9Bb+Tm60PKvcyJApPaKgBszVKOsMeOpyfuYtOlaJc1VDyiC6zAlczhpVCZhvU7YEp+xroyVfeLb6XMDeuhhV7k/P47mLJlTw/+7wPuHR/elOo2eUwWeHM1lHaQ8cGGuTak3Sh/K995XoH2i1Sw==', 'NoGQJ7y5tSPV0Nj0ww9hWKOMqCkuGgbeu2ToAACeJwU026G5oly5sPABtnWzsbyNJRFItaVI/B7kwaRKwGdR23djb/sQD5q+le6+cPe2qOnGrOPVgO8Mwa9zSrw6yCIMuOcWQI84DN9tnlKBwSzbUugglZGUZZsIyTCWX0SmAvflADk/s8UMGFoCgOdVoXiz1WcuFhVorZn3RC4I5EZXZr2u60DN9VdH/loeqn5wHnnZnDR6gJ/1zSRfvRqe2V1LlclYWa5qVNX8EquAU3QmwuHRs8ubUbYmXQgIItUJDjaSQmGdWzF0ykVq85ukPZD6R6Gf8nCKWZD5DXgbGUgfig=='),
('guillaume', 'TJPNQmyFDByXURABflAU6oDlkR0WVYXJwPpibfCjBkjuJQNFVv1PTwelkNTHm59KR1CrUpFBbuPpCAGLCGu6VYL5p0kWZtILCSjPjkFjffum15EpmN1sE/ntY2RRNetLbwhr7abE9xlsLq/hFXlqdkAXqRChQJN+wMucRH9Y0s6kzd+QVHct4i6xcV3gfdwBPpCf9Bb+Tm60PKvcyJApPaKgBszVKOsMeOpyfuYtOlaJc1VDyiC6zAlczhpVCZhvU7YEp+xroyVfeLb6XMDeuhhV7k/P47mLJlTw/+7wPuHR/elOo2eUwWeHM1lHaQ8cGGuTak3Sh/K995XoH2i1Sw==', 'NoGQJ7y5tSPV0Nj0ww9hWKOMqCkuGgbeu2ToAACeJwU026G5oly5sPABtnWzsbyNJRFItaVI/B7kwaRKwGdR23djb/sQD5q+le6+cPe2qOnGrOPVgO8Mwa9zSrw6yCIMuOcWQI84DN9tnlKBwSzbUugglZGUZZsIyTCWX0SmAvflADk/s8UMGFoCgOdVoXiz1WcuFhVorZn3RC4I5EZXZr2u60DN9VdH/loeqn5wHnnZnDR6gJ/1zSRfvRqe2V1LlclYWa5qVNX8EquAU3QmwuHRs8ubUbYmXQgIItUJDjaSQmGdWzF0ykVq85ukPZD6R6Gf8nCKWZD5DXgbGUgfig=='),
('jakub', 'TJPNQmyFDByXURABflAU6oDlkR0WVYXJwPpibfCjBkjuJQNFVv1PTwelkNTHm59KR1CrUpFBbuPpCAGLCGu6VYL5p0kWZtILCSjPjkFjffum15EpmN1sE/ntY2RRNetLbwhr7abE9xlsLq/hFXlqdkAXqRChQJN+wMucRH9Y0s6kzd+QVHct4i6xcV3gfdwBPpCf9Bb+Tm60PKvcyJApPaKgBszVKOsMeOpyfuYtOlaJc1VDyiC6zAlczhpVCZhvU7YEp+xroyVfeLb6XMDeuhhV7k/P47mLJlTw/+7wPuHR/elOo2eUwWeHM1lHaQ8cGGuTak3Sh/K995XoH2i1Sw==', 'NoGQJ7y5tSPV0Nj0ww9hWKOMqCkuGgbeu2ToAACeJwU026G5oly5sPABtnWzsbyNJRFItaVI/B7kwaRKwGdR23djb/sQD5q+le6+cPe2qOnGrOPVgO8Mwa9zSrw6yCIMuOcWQI84DN9tnlKBwSzbUugglZGUZZsIyTCWX0SmAvflADk/s8UMGFoCgOdVoXiz1WcuFhVorZn3RC4I5EZXZr2u60DN9VdH/loeqn5wHnnZnDR6gJ/1zSRfvRqe2V1LlclYWa5qVNX8EquAU3QmwuHRs8ubUbYmXQgIItUJDjaSQmGdWzF0ykVq85ukPZD6R6Gf8nCKWZD5DXgbGUgfig=='),
('benjamin', 'TJPNQmyFDByXURABflAU6oDlkR0WVYXJwPpibfCjBkjuJQNFVv1PTwelkNTHm59KR1CrUpFBbuPpCAGLCGu6VYL5p0kWZtILCSjPjkFjffum15EpmN1sE/ntY2RRNetLbwhr7abE9xlsLq/hFXlqdkAXqRChQJN+wMucRH9Y0s6kzd+QVHct4i6xcV3gfdwBPpCf9Bb+Tm60PKvcyJApPaKgBszVKOsMeOpyfuYtOlaJc1VDyiC6zAlczhpVCZhvU7YEp+xroyVfeLb6XMDeuhhV7k/P47mLJlTw/+7wPuHR/elOo2eUwWeHM1lHaQ8cGGuTak3Sh/K995XoH2i1Sw==', 'NoGQJ7y5tSPV0Nj0ww9hWKOMqCkuGgbeu2ToAACeJwU026G5oly5sPABtnWzsbyNJRFItaVI/B7kwaRKwGdR23djb/sQD5q+le6+cPe2qOnGrOPVgO8Mwa9zSrw6yCIMuOcWQI84DN9tnlKBwSzbUugglZGUZZsIyTCWX0SmAvflADk/s8UMGFoCgOdVoXiz1WcuFhVorZn3RC4I5EZXZr2u60DN9VdH/loeqn5wHnnZnDR6gJ/1zSRfvRqe2V1LlclYWa5qVNX8EquAU3QmwuHRs8ubUbYmXQgIItUJDjaSQmGdWzF0ykVq85ukPZD6R6Gf8nCKWZD5DXgbGUgfig==');

-- Fill the bookmarks table
INSERT INTO PersonBookmark (user_id, nconst, label)
VALUES (2, 'nm0000035', 'myPersonBookmark'),
(2, 'nm0000041', 'myPersonBookmark'),
(2, 'nm0000047', 'myPersonBookmark'),
(2, 'nm0000056', 'myPersonBookmark'),
(2, 'nm0000077', 'myPersonBookmark');

INSERT INTO TitleBookmark (user_id, tconst, label)
VALUES (2, 'tt8115528', 'myTitleBookmark'),
(2, 'tt7632970', 'myTitleBookmark'),
(2, 'tt7365576', 'myTitleBookmark'),
(2, 'tt7746984', 'myTitleBookmark'),
(2, 'tt9700778', 'myTitleBookmark');

INSERT INTO Rating (tconst, user_id, rate)
VALUES ('tt7632970', 2, 5),
('tt7293342', 3, 5),
('tt7293342', 2, 2);

-- Fill the StopWords table
INSERT INTO StopWords
VALUES (''),('i'),('me'),('my'),('myself'),('we'),('our'),('ours'),('ourselves'),('you'),('your'),('yours'),('yourself'),('yourselves'),('he'),('him'),('his'),('himself'),('she'),('her'),('hers'),('herself'),('it'),('its'),('itself'),('they'),('them'),('their'),('theirs'),('themselves'),('what'),('which'),('who'),('whom'),('this'),('that'),('these'),('those'),('am'),('is'),('are'),('was'),('were'),('be'),('been'),('being'),('have'),('has'),('had'),('having'),('do'),('does'),('did'),('doing'),('a'),('an'),('the'),('and'),('but'),('if'),('or'),('because'),('as'),('until'),('while'),('of'),('at'),('by'),('for'),('with'),('about'),('against'),('between'),('into'),('through'),('during'),('before'),('after'),('above'),('below'),('to'),('from'),('up'),('down'),('in'),('out'),('on'),('off'),('over'),('under'),('again'),('further'),('then'),('once'),('here'),('there'),('when'),('where'),('why'),('how'),('all'),('any'),('both'),('each'),('few'),('more'),('most'),('other'),('some'),('such'),('no'),('nor'),('not'),('only'),('own'),('same'),('so'),('than'),('too'),('very'),('s'),('t'),('can'),('will'),('just'),('don'),('should'),('now');

-- Fill the wi table with the information of the movies
DO $$
DECLARE
	w VARCHAR;
	rec record;
BEGIN		
	CREATE TEMP TABLE temp_wi(
    tconst varchar(10) NOT NULL,
    field varchar(1) NOT NULL,
    word text NOT NULL);
		
	FOR rec IN SELECT tconst, originalTitle FROM Title
	LOOP
		FOREACH w in ARRAY (regexp_split_to_array(rec.originalTitle, ' '))
		LOOP
			insert into temp_wi values(rec.tconst, 't', LOWER(w));
		END LOOP;
	END LOOP;
	
	FOR rec IN SELECT tconst, plot FROM Synopsis
	LOOP
		FOREACH w in ARRAY (regexp_split_to_array(rec.plot, ' '))
		LOOP
			insert into temp_wi values(rec.tconst, 'p', LOWER(TRIM( '.,"' FROM w)));
		END LOOP;
	END LOOP;
	
	FOR rec IN SELECT tconst, playing FROM Acting
	LOOP
		FOREACH w in ARRAY (regexp_split_to_array(rec.playing, ' '))
		LOOP
			insert into temp_wi values(rec.tconst, 'c', LOWER(TRIM( '.,' FROM w)));
		END LOOP;
	END LOOP;
	
	FOR rec IN SELECT tconst, firstname, lastname FROM _Work NATURAL JOIN Person
	LOOP
		FOREACH w in ARRAY (regexp_split_to_array(rec.firstname, ' '))
		LOOP
			insert into temp_wi values(rec.tconst, 'a', LOWER(TRIM( ',.' FROM w)));
		END LOOP;
		FOREACH w in ARRAY (regexp_split_to_array(rec.lastname, ' '))
		LOOP
			insert into temp_wi values(rec.tconst, 'a', LOWER(TRIM( ',.' FROM w)));
		END LOOP;
	END LOOP;
	
	INSERT INTO wi
	SELECT tconst, field, word, count(word)
	FROM temp_wi
	WHERE word NOT IN (SELECT word FROM stopwords)
	GROUP BY tconst, field, word;
	
	DROP TABLE temp_wi;
END $$;


/* ##### TRIGGERS ##### */

-- Remove associated tables rows when deleting an User
CREATE OR REPLACE FUNCTION delete_user_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		DELETE FROM Rating
		WHERE user_id = OLD.user_id;
		
		DELETE FROM TitleBookmark
		WHERE user_id = OLD.user_id;

		DELETE FROM PersonBookmark
		WHERE user_id = OLD.user_id;
		
		DELETE FROM SearchHistory
		WHERE user_id = OLD.user_id;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_user_associated_trigger BEFORE DELETE ON _User
FOR EACH ROW EXECUTE FUNCTION delete_user_associated_function();

-- Remove associated tables rows when deleting a Rating instance
CREATE OR REPLACE FUNCTION delete_rating_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		IF OLD.user_id IS NOT NULL THEN
			DELETE FROM _Comment
			WHERE user_id = OLD.user_id;
			
		ELSIF OLD.tconst IS NOT NULL THEN
			DELETE FROM _Comment
			WHERE tconst = OLD.tconst;
		END IF;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_rating_associated_trigger BEFORE DELETE ON Rating
FOR EACH ROW EXECUTE FUNCTION delete_rating_associated_function();

-- Compute a new GeneralRating after insert on Rating
CREATE OR REPLACE FUNCTION update_generalrating_function()
RETURNS TRIGGER AS
$$	
	BEGIN
		IF TG_OP = 'INSERT' THEN
			UPDATE GeneralRating
			SET averageRating = (numVotes * averageRating + NEW.rate) / (numVotes + 1),
				numVotes = numVotes + 1;

			UPDATE PersonRating
			SET rate = name_rating(nconst)
			WHERE nconst IN ( SELECT nconst FROM _Work WHERE _Work.tconst = NEW.tconst );
			
			RETURN NEW;

		ELSIF TG_OP = 'DELETE' THEN
			UPDATE GeneralRating
			SET averageRating = (numVotes * averageRating - OLD.rate) / (numVotes - 1),
				numVotes = numVotes - 1;

			UPDATE PersonRating
			SET rate = name_rating(nconst)
			WHERE nconst IN ( SELECT nconst FROM _Work WHERE _Work.tconst = OLD.tconst );
			
			RETURN OLD;
				
		ELSIF TG_OP = 'UPDATE' THEN
			UPDATE GeneralRating
			SET averageRating = (numVotes * averageRating - OLD.rate + NEW.rate) / numVotes;
			
			UPDATE PersonRating
			SET rate = name_rating(nconst)
			WHERE nconst IN ( SELECT nconst FROM _Work WHERE _Work.tconst = NEW.tconst );

			RETURN NEW;
		END IF;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER update_generalrating_trigger AFTER INSERT OR UPDATE OR DELETE ON Rating
FOR EACH ROW EXECUTE FUNCTION update_generalrating_function();



/* ##### useful Functions ##### */
-- Adding a TitleChild without endYear
CREATE OR REPLACE PROCEDURE add_titlechild(tconst VARCHAR(10), originalTitle VARCHAR(300), releaseYear VARCHAR(4), mediaType VARCHAR(20), parent VARCHAR(10))
AS $$
	BEGIN
		INSERT INTO Title VALUES (tconst, originalTitle, releaseYear, mediaType);
		INSERT INTO TitleChild VALUES (tconst, parent);
	END;
$$ LANGUAGE PLPGSQL;

-- Adding a TitleChild with endYear
CREATE OR REPLACE PROCEDURE add_titlechild(tconst VARCHAR(10), originalTitle VARCHAR(300), releaseYear VARCHAR(4), mediaType VARCHAR(20), parent VARCHAR(10), endYear VARCHAR(4))
AS $$
	BEGIN
		INSERT INTO Title VALUES (tconst, originalTitle, releaseYear, mediaType);
		INSERT INTO TitleChild VALUES (tconst, parent);
		INSERT INTO EndedTitle VALUES (tconst, endYear);
	END;
$$ LANGUAGE PLPGSQL;

-- Adding a TVEpisode without endYear
CREATE OR REPLACE PROCEDURE add_tvepisode(tconst VARCHAR(10), originalTitle VARCHAR(300), releaseYear VARCHAR(4), mediaType VARCHAR(20), parent VARCHAR(10), seasonNumber INT, episodeNumber INT)
AS $$
	BEGIN
		CALL add_titlechild(tconst, originalTitle, releaseYear, mediaType, parent);
		INSERT INTO TVEpisode VALUES (tconst, seasonNumber, episodeNumber);
	END;
$$ LANGUAGE PLPGSQL;

-- Adding a TVEpisode with endYear
CREATE OR REPLACE PROCEDURE add_tvepisode(tconst VARCHAR(10), originalTitle VARCHAR(300), releaseYear VARCHAR(4), mediaType VARCHAR(20), parent VARCHAR(10), endYear VARCHAR(4), seasonNumber INT, episodeNumber INT)
AS $$
	BEGIN
		CALL add_titlechild(tconst, originalTitle, releaseYear, mediaType, parent, endYear);
		INSERT INTO TVEpisode VALUES (tconst, seasonNumber, episodeNumber);
	END;
$$ LANGUAGE PLPGSQL;