/* ##### TABLES CREATION ##### */

CREATE TABLE Person (
	nconst VARCHAR(10) PRIMARY KEY,
	lastName VARCHAR(100) NOT NULL,
	firstName VARCHAR(100) NOT NULL,
	birthYear VARCHAR(4) NOT NULL
);

CREATE TABLE Death (
	nconst VARCHAR(10) PRIMARY KEY REFERENCES Person(nconst),
	deathYear VARCHAR(4) NOT NULL
);

CREATE TABLE Title (
	tconst VARCHAR(10) PRIMARY KEY,
	originalTitle VARCHAR(300) NOT NULL,
	releaseYear VARCHAR(4) NOT NULL,
	mediaType VARCHAR(20) NOT NULL
);

CREATE TABLE EndedTitle (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	endYear VARCHAR(4) NOT NULL
);

CREATE TABLE TitleChild (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	parent VARCHAR(10) REFERENCES Title(tconst),
	CONSTRAINT titlechild_not_equal CHECK (tconst != parent)
);

CREATE TABLE TVEpisode (
	tconst VARCHAR(10) REFERENCES TitleChild(tconst),
	seasonNumber INTEGER,
	episodeNumber INTEGER,
	CONSTRAINT tvepisode_pk PRIMARY KEY (tconst, seasonNumber, episodeNumber)
);

CREATE TABLE Genre (
	tconst VARCHAR(10) REFERENCES Title(tconst),
	label VARCHAR(50),
	CONSTRAINT genre_pk PRIMARY KEY (tconst, label)
);

CREATE TABLE Adult (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst)
);

CREATE TABLE Duration (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	runtimeMinutes INTEGER NOT NULL
);

CREATE TABLE TitleAKAs (
	tconst VARCHAR(10) REFERENCES Title(tconst),
	title VARCHAR(300),
	countryCode VARCHAR(4) NOT NULL,
	languageCode VARCHAR(4) NOT NULL,
	CONSTRAINT titleakas_pk PRIMARY KEY (tconst, title, countryCode, languageCode)
);

CREATE TABLE Poster (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	url TEXT NOT NULL
);

CREATE TABLE Synopsis (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	plot TEXT NOT NULL
);

CREATE TABLE Award (
	tconst VARCHAR(10) REFERENCES Title(tconst),
	label VARCHAR(100) NOT NULL,
	CONSTRAINT award_pk PRIMARY KEY (tconst, label)
);

CREATE TABLE _Work (
	tconst VARCHAR(10) REFERENCES Title(tconst),
	nconst VARCHAR(10) REFERENCES Person(nconst),
	job VARCHAR(200),
	CONSTRAINT work_pk PRIMARY KEY (tconst, nconst, job)
);

CREATE TABLE Acting (
	tconst VARCHAR(10),
	nconst VARCHAR(10),
	job VARCHAR(100),
	playing VARCHAR(200),
	CONSTRAINT actor_pk PRIMARY KEY (tconst, nconst, job, playing),
	CONSTRAINT actor_fk FOREIGN KEY (tconst, nconst, job) REFERENCES _Work(tconst, nconst, job)
);

CREATE TABLE GeneralRating (
	tconst VARCHAR(10) PRIMARY KEY REFERENCES Title(tconst),
	numVotes SERIAL NOT NULL,
	averagerating FLOAT NOT NULL
);


/* ##### ADDITIONAL CONSTRAINTS (TRIGGERS) ##### */

-- Make sure that a title release year cannot be superior than its end year

CREATE OR REPLACE FUNCTION check_years_endedtitle_function()
RETURNS TRIGGER AS
$$
	DECLARE
		releaseYearV VARCHAR(4);
	BEGIN
		SELECT releaseYear INTO releaseYearV
		FROM Title
		WHERE tconst = NEW.tconst;
		
		IF releaseYearV > NEW.endYear THEN
			RAISE EXCEPTION 'The release year cannot be superior than the end year';
			RETURN NULL;
		ELSE
			RETURN NEW;
		END IF;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER check_years_endedtitle_trigger BEFORE INSERT OR UPDATE ON EndedTitle
FOR EACH ROW EXECUTE FUNCTION check_years_endedtitle_function();


CREATE OR REPLACE FUNCTION check_years_title_function()
RETURNS TRIGGER AS
$$
	DECLARE
		releaseYearV VARCHAR(4);
		endYearV VARCHAR(4);
	BEGIN
		SELECT releaseYear INTO releaseYearV
		FROM Title
		WHERE tconst = NEW.tconst;

		SELECT endYear INTO endYearV
		FROM EndedTitle
		WHERE tconst = NEW.parent;
		
		IF releaseYearV > endYearV THEN
			RAISE EXCEPTION 'The children title''s release year cannot be superior than the parent title''s end year';
			RETURN NULL;
		ELSE
			RETURN NEW;
		END IF;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER check_years_title_trigger BEFORE INSERT OR UPDATE ON TitleChild
FOR EACH ROW EXECUTE FUNCTION check_years_title_function();


/* ##### DATA MIGRATION ##### */

-- Fill Person table
INSERT INTO Person
SELECT DISTINCT nconst, SPLIT_PART(primaryname, ' ', 2) AS lastname,	SPLIT_PART(primaryname, ' ', 1) AS firstname, birthyear
FROM name_basics;

-- Fill Death table
INSERT INTO Death
SELECT DISTINCT nconst, deathyear
FROM name_basics
WHERE deathyear != '   ';

-- Fill Title table
INSERT INTO Title
SELECT DISTINCT tconst, primarytitle AS originalTitle, startyear AS releaseYear, titletype AS mediaType
FROM title_basics;

-- Fill EndedTitle table
INSERT INTO EndedTitle
SELECT DISTINCT tconst, endyear
FROM title_basics
WHERE startyear != endyear
AND endyear != '    ';

-- Fill TitleChild table
INSERT INTO TitleChild
SELECT DISTINCT tb.tconst, ep.parenttconst AS parent
FROM title_basics AS tb
INNER JOIN title_episode AS ep
ON tb.tconst = ep.tconst;

-- Fill TVEpisode table
INSERT INTO TVEpisode
SELECT DISTINCT tb.tconst, ep.seasonNumber, ep.episodeNumber
FROM title_basics AS tb
INNER JOIN title_episode AS ep
ON tb.tconst = ep.tconst
WHERE ep.seasonNumber IS NOT NULL
AND ep.episodeNumber IS NOT NULL;

-- Fill Genre table
DO $$
	DECLARE
		w VARCHAR;
  		rec RECORD;
	BEGIN
  		FOR rec IN SELECT DISTINCT tconst, genres FROM title_basics
		LOOP
			FOREACH w IN ARRAY (regexp_split_to_array(rec.genres, ','))
			LOOP
		    	INSERT INTO Genre (tconst, label) VALUES (rec.tconst, w);
		    END LOOP;
		END LOOP;
	END
$$;

-- Fill Adult table
INSERT INTO Adult
SELECT DISTINCT tconst
FROM title_basics
WHERE isadult = TRUE;

-- Fill Duration table
INSERT INTO Duration
SELECT DISTINCT tconst, runtimeminutes
FROM title_basics 
WHERE runtimeminutes IS NOT NULL;

-- Fill TitleAKAs table
DO $$
	DECLARE
		rec RECORD;
	BEGIN	
		FOR rec IN SELECT DISTINCT tb.tconst, ta.title, ta.region AS countryCode, ta.language AS languageCode
			FROM title_akas AS ta
			INNER JOIN title_basics AS tb
			ON ta.titleid = tb.tconst
			WHERE tb.primarytitle != ta.title
			AND (region != '' OR language != '')
		LOOP
			
			IF rec.languageCode = '' THEN
				rec.languageCode = rec.countryCode;
			ELSIF rec.countryCode = '' THEN
				rec.countryCode = rec.languageCode;
			END IF;
			
			INSERT INTO TitleAKAs(tconst, title, countryCode, languageCode)
			VALUES (rec.tconst, rec.title, rec.countryCode, rec.languageCode);
		END LOOP;
	END
$$;

-- Fill Poster table
INSERT INTO Poster
SELECT DISTINCT tconst, poster AS url
FROM omdb_data
WHERE poster != 'N/A'
AND tconst IN (
	SELECT tconst
	FROM title_basics
);

-- Fill Synopsis table
INSERT INTO Synopsis
SELECT DISTINCT tconst, plot
FROM omdb_data
WHERE plot != 'N/A'
AND tconst IN (
	SELECT DISTINCT tconst
	FROM title_basics
);

-- Fill Award table
INSERT INTO Award
SELECT DISTINCT tconst, awards AS label
FROM omdb_data
WHERE awards != 'N/A'
AND tconst IN (
	SELECT DISTINCT tconst
	FROM title_basics
);

-- Fill _Work table
WITH directors AS (
	SELECT DISTINCT tconst, UNNEST(regexp_split_to_array(directors, ','))::VARCHAR(10) AS nconst, 'director' AS job
	FROM title_crew
	WHERE directors != ''
),
writers AS (
	SELECT DISTINCT tconst, UNNEST(regexp_split_to_array(writers, ','))::VARCHAR(10) AS nconst, 'writer' AS job
	FROM title_crew
	WHERE writers != ''
),
everyWorker AS (
	SELECT DISTINCT tconst, nconst, job
	FROM directors

	UNION

	SELECT DISTINCT tconst, nconst, job
	FROM writers

	UNION

	SELECT DISTINCT tconst, nconst, category AS job
	FROM title_principals
)
INSERT INTO _Work
SELECT DISTINCT tconst, nconst, job
FROM everyWorker
WHERE tconst IN (SELECT DISTINCT tconst FROM Title)
AND nconst IN (SELECT DISTINCT nconst FROM Person);


-- Fill Acting table
CREATE OR REPLACE FUNCTION remove_special_characters(fromText VARCHAR)
RETURNS VARCHAR AS
$$
	BEGIN
		fromText = LOWER(fromText);
		fromText = TRANSLATE(fromText, '\[', '');
		fromText = TRANSLATE(fromText, '''', '');
		fromText = TRANSLATE(fromText, '\]', '');
		fromText = REPLACE(fromText, '^\s', '');
		
		return fromText;
	END;
$$ LANGUAGE PLPGSQL;

DO $$
	DECLARE
  		rec RECORD;
	BEGIN
		FOR rec IN (
			WITH comma_space_separated AS (
				SELECT w.tconst, w.nconst, w.job, UNNEST(regexp_split_to_array(remove_special_characters(tp.characters), ', ')) AS character
				FROM _Work AS w
				INNER JOIN title_principals AS tp
				ON w.tconst = tp.tconst
				AND w.nconst = tp.nconst
				AND w.job = tp.category
				WHERE tp.category = 'actor'
				AND tp.characters != ''
			)
			SELECT DISTINCT tconst, nconst, job, UNNEST(regexp_split_to_array(character, ',')) AS character
			FROM comma_space_separated
		)
		LOOP
			INSERT INTO Acting (tconst, nconst, job, playing)
			VALUES (rec.tconst, rec.nconst, rec.job, rec.character);
		END LOOP;
	END
$$;

-- Fill GeneralRating table
INSERT INTO GeneralRating
SELECT DISTINCT tconst, numvotes, averagerating
FROM title_ratings;


/** ##### TRIGGERS ##### **/

-- Remove associated tables rows when deleting a Title
CREATE OR REPLACE FUNCTION delete_title_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		DELETE FROM TitleAKAs
		WHERE tconst = OLD.tconst;
		
		DELETE FROM Duration
		WHERE tconst = OLD.tconst;
		
		DELETE FROM Adult
		WHERE tconst = OLD.tconst;
		
		DELETE FROM EndedTitle
		WHERE tconst = OLD.tconst;
		
		DELETE FROM TitleChild
		WHERE parent = OLD.tconst
		OR tconst = OLD.tconst;
		
		DELETE FROM Award
		WHERE tconst = OLD.tconst;
		
		DELETE FROM Synopsis
		WHERE tconst = OLD.tconst;
		
		DELETE FROM Poster
		WHERE tconst = OLD.tconst;
		
		DELETE FROM GeneralRating
		WHERE tconst = OLD.tconst;
		
		DELETE FROM _Work
		WHERE tconst = OLD.tconst;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_title_associated_trigger BEFORE DELETE ON Title
FOR EACH ROW EXECUTE FUNCTION delete_title_associated_function();

-- Remove associated tables rows when deleting a Person
CREATE OR REPLACE FUNCTION delete_person_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		DELETE FROM Death
		WHERE nconst = OLD.nconst;
		
		DELETE FROM _Work
		WHERE nconst = OLD.nconst;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_person_associated_trigger BEFORE DELETE ON Person
FOR EACH ROW EXECUTE FUNCTION delete_person_associated_function();

-- Remove associated tables rows when deleting a _Work instance
CREATE OR REPLACE FUNCTION delete_work_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		IF OLD.nconst IS NOT NULL THEN
			DELETE FROM Acting
			WHERE nconst = OLD.nconst;
			
		ELSIF OLD.tconst IS NOT NULL THEN
			DELETE FROM Acting
			WHERE tconst = OLD.tconst;
		END IF;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_work_associated_trigger BEFORE DELETE ON _Work
FOR EACH ROW EXECUTE FUNCTION delete_work_associated_function();

-- Remove associated tables rows when deleting a TitleChild instance
CREATE OR REPLACE FUNCTION delete_titlechild_associated_function()
RETURNS TRIGGER AS
$$
	BEGIN
		DELETE FROM TVEpisode
		WHERE tconst = OLD.tconst;

		RETURN OLD;
	END
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER delete_titlechild_associated_trigger BEFORE DELETE ON TitleChild
FOR EACH ROW EXECUTE FUNCTION delete_titlechild_associated_function();


/* ##### CLEANING ##### */

-- Drop useless tables
DROP TABLE IF EXISTS omdb_data CASCADE;
DROP TABLE IF EXISTS name_basics CASCADE;
DROP TABLE IF EXISTS title_basics CASCADE;
DROP TABLE IF EXISTS title_principals CASCADE;
DROP TABLE IF EXISTS title_akas CASCADE;
DROP TABLE IF EXISTS title_crew CASCADE;
DROP TABLE IF EXISTS title_episode CASCADE;
DROP TABLE IF EXISTS title_ratings CASCADE;

-- Drop useless functions
DROP FUNCTION IF EXISTS remove_special_characters(VARCHAR);