/* ##### TitleChild check trigger ##### */
-- Given
SELECT t.*, (t.releaseYear < et.endYear) AS isValid
FROM Title AS t
INNER JOIN EndedTitle AS et
ON t.tconst = et.tconst
WHERE t.tconst = 'tt0944947';

-- When
INSERT INTO Title (tconst, originalTitle, releaseYear, mediaType)
VALUES ('tt9999999', 'Special Episode', '2021', 'tvEpisode');
INSERT INTO TitleChild (tconst, parent)
VALUES ('tt9999999', 'tt0944947');

-- Then
-- Should refuse the insert on TitleChild


/* ##### EndedTitle check trigger ##### */
-- Given
SELECT t.*, et.endYear
FROM Title AS t
LEFT OUTER JOIN EndedTitle AS et
ON t.tconst = et.tconst
WHERE t.tconst = 'tt6779398';

-- When
INSERT INTO EndedTitle (tconst, endYear)
VALUES ('tt6779398', '2015');

-- Then
-- Should refuse the insert on EndedTitle
