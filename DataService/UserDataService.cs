﻿using System;
using DataService.Interfaces;
using DataService.Domain;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataService {

    public class UserDataService: IUserDataService, IRatingDataService, IBookmarkDataService, ISearchDataService {

        // IUserDataService

        public IList<User> GetUsers() {
            return new DataContext().Users.ToList();
        }

        public User GetUserByLogin(string Login) {
            return new DataContext().Users.FirstOrDefault(x => x.Login == Login);
        }

        public User GetUser(int Id) {
            return new DataContext().Users
                .Include(x => x.PersonBookmarks)
                .ThenInclude(x => x.Person)
                .Include(x => x.TitleBookmarks)
                .ThenInclude(x => x.Title)
                .Include(x => x.Ratings)
                .ThenInclude(x => x.Comment)
                .Include(x => x.Searches)
                .FirstOrDefault(x => x.UserId == Id);
        }

        public User AddUser(string Login, string Password, string Salt) {
            var Context = new DataContext();

            var user = new User() {
                Login = Login,
                Password = Password,
                Salt = Salt
            };

            Context.Users.Add(user);
            Context.SaveChanges();

            Context.Entry(user).Reload();

            return user;
        }

        public bool DeleteUser(int Id) {
            var Context = new DataContext();

            var user = Context.Users.FirstOrDefault(x => x.UserId == Id);
            var found = user != null;

            if (found) {
                Context.Users.Remove(user);
                Context.SaveChanges();
            }

            return found;
        }

        public bool UpdateLogin(int Id, string Login) {
            var Context = new DataContext();

            var user = Context.Users.FirstOrDefault(x => x.UserId == Id);
            var found = user != null;

            if (found) {
                user.Login = Login;
                Context.Users.Update(user);
                Context.SaveChanges();
            }

            return found;
        }

        public bool UpdatePassword(int Id, string Password, string Salt) {
            var Context = new DataContext();

            var user = Context.Users.FirstOrDefault(x => x.UserId == Id);
            var found = user != null;

            if (found) {
                user.Password = Password;
                user.Salt = Salt;
                Context.Users.Update(user);
                Context.SaveChanges();
            }

            return found;
        }


        // IRatingDataService

        public (IList<Rating>, int) GetRatingsByUserId(int UserId, int CurrentPage, int PageSize) {
            var source =  new DataContext().Ratings
                .Include(x => x.Comment)
                .AsEnumerable()
                .Where(x => x.UserId == UserId)
                .TakeLast(50)
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public (IList<Rating>, int) GetRatingsByTitleId(string TitleId, int CurrentPage, int PageSize) {
            var source = new DataContext().Ratings
                .Include(x => x.Comment)
                .AsEnumerable()
                .Where(x => x.TitleId == TitleId)
                .TakeLast(50)
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public Rating GetRating(int UserId, string TitleId) {
            return new DataContext().Ratings
                .Include(x => x.Comment)
                .FirstOrDefault(x => x.TitleId == TitleId && x.UserId == UserId);
        }

        public Rating AddRating(int UserId, string TitleId, int Rate) {
            var Context = new DataContext();

            if (Context.Titles.FirstOrDefault(x => x.TitleId == TitleId) == null)
                return null;

            var rating = new Rating() {
                UserId = UserId,
                TitleId = TitleId,
                Rate = Rate
            };

            Context.Ratings.Add(rating);
            Context.SaveChanges();

            Context.Entry(rating).Reload();

            return rating;
        }

        public Rating AddRating(int UserId, string TitleId, int Rate, string Comment) {
            var Context = new DataContext();

            if (Context.Titles.FirstOrDefault(x => x.TitleId == TitleId) == null)
                return null;

            var rating = new Rating() {
                UserId = UserId,
                TitleId = TitleId,
                Rate = Rate
            };

            Context.Ratings.Add(rating);

            var comment = new Comment() {
                UserId = UserId,
                TitleId = TitleId,
                Content = Comment
            };

            Context.Comments.Add(comment);
            Context.SaveChanges();

            Context.Entry(rating).Reload();

            return rating;
        }

        public bool UpdateRating(int UserId, string TitleId, int Rate, string Comment = null) {
            var Context = new DataContext();

            var rating = Context.Ratings.FirstOrDefault(x => x.TitleId == TitleId && x.UserId == UserId);
            var found = rating != null;

            if (found) {
                if (Comment != null)
                    AddOrUpdateComment(UserId, TitleId, Comment);

                rating.Rate = Rate;
                Context.Ratings.Update(rating);
                Context.SaveChanges();
            }

            return found;
        }

        private static void AddOrUpdateComment(int UserId, string TitleId, string Comment) {
            var Context = new DataContext();

            var comment = Context.Comments.FirstOrDefault(x => x.TitleId == TitleId && x.UserId == UserId);

            if (comment == null) {
                Context.Comments.Add(new Comment() {
                    UserId = UserId,
                    TitleId = TitleId,
                    Content = Comment
                });

            } else {
                comment.Content = Comment;
                Context.Comments.Update(comment);
            }
            
            Context.SaveChanges();
        }

        public bool DeleteRating(int UserId, string TitleId) {
            var Context = new DataContext();

            var rating = Context.Ratings.FirstOrDefault(x => x.UserId == UserId && x.TitleId == TitleId);
            var found = rating != null;

            if (found) {
                Context.Ratings.Remove(rating);
                Context.SaveChanges();
            }

            return found;
        }


        // IBookmarkDataService

        public (IList<TitleBookmark>, int) GetTitleBookmarks(int UserId, int CurrentPage, int PageSize) {
            var source = new DataContext().TitleBookmarks
                .Include(x => x.User)
                .Include(x => x.Title)
                .Where(x => x.UserId == UserId)
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public (IList<PersonBookmark>, int) GetPersonBookmarks(int UserId, int CurrentPage, int PageSize) {
            var source = new DataContext().PersonBookmarks
                .Include(x => x.User)
                .Include(x => x.Person)
                .Where(x => x.UserId == UserId)
                .ToList();

            var count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public TitleBookmark AddTitleBookmark(int UserId, string TitleId, string Label) {
            var Context = new DataContext();

            if (Context.Titles.FirstOrDefault(x => x.TitleId == TitleId) == null)
                return null;

            var titleBookmark = new TitleBookmark() {
                UserId = UserId,
                TitleId = TitleId,
                Label = Label
            };

            Context.TitleBookmarks.Add(titleBookmark);
            Context.SaveChanges();

            Context.Entry(titleBookmark).Reload();

            return titleBookmark;
        }

        public PersonBookmark AddPersonBookmark(int UserId, string PersonId, string Label) {
            var Context = new DataContext();

            if (Context.Persons.FirstOrDefault(x => x.PersonId == PersonId) == null)
                return null;

            var personBookmark = new PersonBookmark() {
                UserId = UserId,
                PersonId = PersonId,
                Label = Label
            };

            Context.PersonBookmarks.Add(personBookmark);
            Context.SaveChanges();

            Context.Entry(personBookmark).Reload();

            return personBookmark;
        }

        public bool UpdateTitleBookmark(int UserId, string TitleId, string newLabel) {
            var Context = new DataContext();

            var titleBookmark = Context.TitleBookmarks.FirstOrDefault(x => x.TitleId == TitleId && x.UserId == UserId);
            var found = titleBookmark != null;
            
            if (found) {
                titleBookmark.Label = newLabel;
                Context.TitleBookmarks.Update(titleBookmark);
                Context.SaveChanges();
            }

            return found;
        }

        public bool UpdatePersonBookmark(int UserId, string PersonId, string newLabel) {
            var Context = new DataContext();

            var personBookmark = Context.PersonBookmarks.FirstOrDefault(x => x.PersonId == PersonId && x.UserId == UserId);
            var found = personBookmark != null;

            if (found) {
                personBookmark.Label = newLabel;
                Context.PersonBookmarks.Update(personBookmark);
                Context.SaveChanges();
            }

            return found;
        }

        public TitleBookmark GetTitleBookmark(int UserId, string TitleId) {
            return new DataContext().TitleBookmarks
                .Include(x => x.User)
                .Include(x => x.Title)
                .FirstOrDefault(x => x.TitleId == TitleId && x.UserId == UserId);
        }

        public PersonBookmark GetPersonBookmark(int UserId, string PersonId) {
            return new DataContext().PersonBookmarks
                .Include(x => x.User)
                .Include(x => x.Person)
                .FirstOrDefault(x => x.PersonId == PersonId && x.UserId == UserId);
        }

        public bool DeleteTitleBookmark(int UserId, string TitleId) {
            var Context = new DataContext();

            var titleBookmark = Context.TitleBookmarks.FirstOrDefault(x => x.UserId == UserId && x.TitleId == TitleId);
            var found = titleBookmark != null;

            if (found) {
                Context.TitleBookmarks.Remove(titleBookmark);
                Context.SaveChanges();
            }

            return found;
        }

        public bool DeletePersonBookmark(int UserId, string PersonId) {
            var Context = new DataContext();

            var personBookmark = Context.PersonBookmarks.FirstOrDefault(x => x.UserId == UserId && x.PersonId == PersonId);
            var found = personBookmark != null;

            if (found) {
                Context.PersonBookmarks.Remove(personBookmark);
                Context.SaveChanges();
            }

            return found;
        }


        // Search History

        public (ICollection<SearchHistory>, int) GetSearchHistory(int Id, int CurrentPage, int PageSize) {
            var source = new DataContext().SearchesHistory
                .Where(x => x.UserId == Id)
                .OrderByDescending(x => x.Date)
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }
        public (ICollection<SearchHistory>, int) GetSearchHistoryQuery(int Id, string query, int CurrentPage, int PageSize)
        {
            var source = new DataContext().SearchesHistory
                .Where(x => x.UserId == Id)
                .Where(x => x.SearchedText.ToLower().Contains(query.ToLower()))
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }
        public (ICollection<SearchHistory>, int) GetSearchHistory(int Id, char SearchType, int CurrentPage, int PageSize) {
            var source = new DataContext().SearchesHistory
                .Where(x => x.UserId == Id && x.SearchType == SearchType)
                .OrderByDescending(x => x.Date)
                .ToList();

            int count = source.Count;

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public void DeleteSearchHistory(int Id) {
            var Context = new DataContext();

            var history = Context.SearchesHistory
                .Where(x => x.UserId == Id)
                .ToList();

            foreach (SearchHistory entry in history)
                Context.SearchesHistory.Remove(entry);

            Context.SaveChanges();
        }


        //ISearchDataService

        public (IList<SearchTitleResult>, int) GetTitleFromString(string searchString, int currentPage, int pageSize, int userId = -1) {
            try {
                var Context = new DataContext();
                IQueryable<SearchTitleResult> titles;

                titles = Context.TitleSearchResults.FromSqlInterpolated($"select * from search_title({searchString})");
                if (userId != -1)
                {
                    var searchHist = new SearchHistory()
                    {
                        UserId = userId,
                        Date = DateTime.Now,
                        SearchType = 't',
                        SearchedText = searchString
                    };
                    Context.SearchesHistory.Add(searchHist);
                    Context.SaveChanges();
                }

                if (titles != null) {
                    var result = titles
                        .Skip((currentPage - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

                    return (result, titles.Count());
                }
                return (new List<SearchTitleResult>(), 0);
            
            } catch {
                return (new List<SearchTitleResult>(), 0);
            }
        }

        public (IList<SearchPersonResult>, int) GetPersonFromString(string searchString, int currentPage, int pageSize, int userId = -1) {
            try {
                var Context = new DataContext();
                IQueryable<SearchPersonResult> persons;

                persons = Context.PersonSearchResults.FromSqlInterpolated($"select * from search_person({searchString})");
                if (userId != -1)
                {
                    var searchHist = new SearchHistory()
                    {
                        UserId = userId,
                        Date = DateTime.Now,
                        SearchType = 'p',
                        SearchedText = searchString
                    };
                    Context.SearchesHistory.Add(searchHist);
                    Context.SaveChanges();
                }
                if (persons != null) {
                    var result = persons
                        .Skip((currentPage - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();
                    return (result, persons.Count());
                }
                return (new List<SearchPersonResult>(), 0);
                
            } catch {
                return (new List<SearchPersonResult>(), 0);
            }
        }
    }
}
