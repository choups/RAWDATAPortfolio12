﻿using System;
using System.Collections.Generic;
using DataService.Domain;

namespace DataService.Interfaces {

    public interface IUserDataService {
        public User GetUserByLogin(string Login);
        public User GetUser(int Id);
        public User AddUser(string Login, string Password, string Salt);
        public bool UpdateLogin(int Id, string Login);
        public bool UpdatePassword(int Id, string Password, string Salt);
        public bool DeleteUser(int Id);
        public (ICollection<SearchHistory>, int) GetSearchHistory(int Id, int CurrentPage, int PageSize);
        public (ICollection<SearchHistory>, int) GetSearchHistory(int Id, char SearchType, int CurrentPage, int PageSize);
        public (ICollection<SearchHistory>, int) GetSearchHistoryQuery(int Id, string query, int CurrentPage, int PageSize);
        public void DeleteSearchHistory(int Id);
    }
}
