﻿using System;
using DataService.Domain;
using System.Collections.Generic;

namespace DataService.Interfaces {

    public interface ITitleDataService {
        public (IList<Title>, int) GetPagedTitles(int CurrentPage, int PageSize);
        public Title GetTitle(string Id);
        public (ICollection<SearchTitleResult>, int) GetSimilarTitles(string Id, int CurrentPage, int PageSize);
    }
}
