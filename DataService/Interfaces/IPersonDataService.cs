﻿using System;
using System.Collections.Generic;
using DataService.Domain;

namespace DataService.Interfaces {

    public interface IPersonDataService {
        public Person GetPerson(string Id);
        public (IList<Person>, int) GetPagedPersons(int CurrentPage, int PageSize);
    }
}
