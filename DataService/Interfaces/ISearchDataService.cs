﻿using DataService.Domain;
using System.Collections.Generic;

namespace DataService.Interfaces
{
    public interface ISearchDataService
    {
        public (IList<SearchTitleResult>, int) GetTitleFromString(string searchString, int currentPage, int pageSize, int userId);

        public (IList<SearchPersonResult>, int) GetPersonFromString(string searchString, int currentPage, int pageSize, int userId);
    }
}
