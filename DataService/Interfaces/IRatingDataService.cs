﻿using System;
using DataService.Domain;
using System.Collections.Generic;

namespace DataService.Interfaces {

    public interface IRatingDataService {
        public (IList<Rating>, int) GetRatingsByUserId(int UserId, int CurrentPage, int PageSize);
        public (IList<Rating>, int) GetRatingsByTitleId(string TitleId, int CurrentPage, int PageSize);
        public Rating GetRating(int UserId, string TitleId);
        public Rating AddRating(int UserId, string TitleId, int Rate);
        public Rating AddRating(int UserId, string TitleId, int Rate, string Comment);
        public bool UpdateRating(int UserId, string TitleId, int Rate, string Comment = null);
        public bool DeleteRating(int UserId, string TitleId);
    }
}
