﻿using System;
using DataService.Domain;
using System.Collections.Generic;

namespace DataService.Interfaces {

    public interface IBookmarkDataService {
        public (IList<TitleBookmark>, int) GetTitleBookmarks(int UserId, int CurrentPage, int PageSize);
        public TitleBookmark GetTitleBookmark(int UserId, string TitleId);
        public TitleBookmark AddTitleBookmark(int UserId, string TitleId, string Label);
        public bool UpdateTitleBookmark(int UserId, string TitleId, string newLabel);
        public bool DeleteTitleBookmark(int UserId, string TitleId);

        public (IList<PersonBookmark>, int) GetPersonBookmarks(int UserId, int CurrentPage, int PageSize);
        public PersonBookmark GetPersonBookmark(int UserId, string PersonId);
        public PersonBookmark AddPersonBookmark(int UserId, string PersonId, string Label);
        public bool UpdatePersonBookmark(int UserId, string PersonId, string newLabel);
        public bool DeletePersonBookmark(int UserId, string PersonId);
    }
}
