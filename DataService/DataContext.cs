﻿using System;
using Microsoft.EntityFrameworkCore;
using DataService.Domain;

namespace DataService {

    public class DataContext: DbContext {

        public DbSet<Acting> Actings { get; set; }
        public DbSet<Adult> Adults { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Death> Deaths { get; set; }
        public DbSet<Duration> Durations { get; set; }
        public DbSet<EndedTitle> EndedTitles { get; set; }
        public DbSet<GeneralRating> GeneralRatings { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Synopsis> Synopsises { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<TitleAka> TitleAkas { get; set; }
        public DbSet<TitleChild> TitleChilds { get; set; }
        public DbSet<TVEpisode> TVEpisodes { get; set; }
        public DbSet<Work> Works { get; set; }
        public DbSet<Poster> Posters { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<PersonBookmark> PersonBookmarks { get; set; }
        public DbSet<TitleBookmark> TitleBookmarks { get; set; }
        public DbSet<SearchHistory> SearchesHistory { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DbSet<SearchTitleResult> TitleSearchResults { get; set; }
        public DbSet<SearchPersonResult> PersonSearchResults { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseNpgsql(Configuration.Shared.GetDatabaseString());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Title>(builder => {
                builder.ToTable("title");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.OriginalTitle).HasColumnName("originaltitle");
                builder.Property(x => x.ReleaseYear).HasColumnName("releaseyear");
                builder.Property(x => x.MediaType).HasColumnName("mediatype");
                builder.HasKey(x => x.TitleId);
                builder.HasMany(x => x.Akas).WithOne().HasForeignKey(x => x.TitleId);
                builder.HasMany(x => x.Staff).WithOne().HasForeignKey(x => x.TitleId);
                builder.HasMany(x => x.Genres).WithOne().HasForeignKey(x => x.TitleId);
                builder.HasMany(x => x.ReferencesTitleBookmarks).WithOne(x => x.Title).HasForeignKey(x => x.TitleId);
                builder.HasMany(x => x.Children).WithOne().HasForeignKey(x => x.ParentId);
                builder.HasOne(x => x.TitleChild).WithOne(x => x.Title).HasForeignKey<TitleChild>(x => x.TitleId);
                builder.HasOne(x => x.Duration).WithOne().HasForeignKey<Duration>(x => x.TitleId);
                builder.HasOne(x => x.Adult).WithOne().HasForeignKey<Adult>(x => x.TitleId);
                builder.HasOne(x => x.GeneralRating).WithOne().HasForeignKey<GeneralRating>(x => x.TitleId);
                builder.HasOne(x => x.Synopsis).WithOne().HasForeignKey<Synopsis>(x => x.TitleId);
                builder.HasOne(x => x.Award).WithOne().HasForeignKey<Award>(x => x.TitleId);
                builder.HasOne(x => x.EndedTitle).WithOne().HasForeignKey<EndedTitle>(x => x.TitleId);
                builder.HasOne(x => x.Poster).WithOne().HasForeignKey<Poster>(x => x.TitleId);
            });

            modelBuilder.Entity<Person>(builder => {
                builder.ToTable("person");
                builder.Property(x => x.PersonId).HasColumnName("nconst");
                builder.Property(x => x.FirstName).HasColumnName("firstname");
                builder.Property(x => x.LastName).HasColumnName("lastname");
                builder.Property(x => x.BirthYear).HasColumnName("birthyear");
                builder.HasKey(x => x.PersonId);
                builder.HasMany(x => x.Works).WithOne().HasForeignKey(x => x.PersonId);
                builder.HasOne(x => x.Death).WithOne().HasForeignKey<Death>(x => x.PersonId);
                builder.HasMany(x => x.ReferencedPersonBookmarks).WithOne(x => x.Person).HasForeignKey(x => x.PersonId);
            });

            modelBuilder.Entity<User>(builder => {
                builder.ToTable("_user");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Login).HasColumnName("login");
                builder.Property(x => x.Password).HasColumnName("pwd");
                builder.Property(x => x.Salt).HasColumnName("salt");
                builder.HasKey(x => x.UserId);
                builder.HasMany(x => x.Ratings).WithOne().HasForeignKey(x => x.UserId);
                builder.HasMany(x => x.Searches).WithOne().HasForeignKey(x => x.UserId);
                builder.HasMany(x => x.TitleBookmarks).WithOne(x => x.User).HasForeignKey(x => x.UserId);
                builder.HasMany(x => x.PersonBookmarks).WithOne(x => x.User).HasForeignKey(x => x.UserId);
            });

            modelBuilder.Entity<Adult>(builder => {
                builder.ToTable("adult");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.HasKey(x => x.TitleId);
            });

            modelBuilder.Entity<Award>(builder => {
                builder.ToTable("award");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.Label).HasColumnName("label");
                builder.HasKey(x => x.TitleId);
            });

            modelBuilder.Entity<Death>(builder => {
                builder.ToTable("death");
                builder.Property(x => x.PersonId).HasColumnName("nconst");
                builder.Property(x => x.DeathYear).HasColumnName("deathyear");
                builder.HasKey(x => x.PersonId);
            });

            modelBuilder.Entity<Duration>(builder => {
                builder.ToTable("duration");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.RuntimeMinutes).HasColumnName("runtimeminutes");
                builder.HasKey(x => x.TitleId);
            });

            modelBuilder.Entity<EndedTitle>(builder => {
                builder.ToTable("endedtitle");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.EndYear).HasColumnName("endyear");
                builder.HasKey(x => x.TitleId);
            });
            
            modelBuilder.Entity<GeneralRating>(builder => {
                builder.ToTable("generalrating");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.NumVotes).HasColumnName("numvotes");
                builder.Property(x => x.AverageRating).HasColumnName("averagerating");
                builder.HasKey(x => x.TitleId);
            });

            modelBuilder.Entity<Genre>(builder => {
                builder.ToTable("genre");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.Label).HasColumnName("label");
                builder.HasKey(x => new { x.TitleId, x.Label });
            });
            
            modelBuilder.Entity<Poster>(builder => {
                builder.ToTable("poster");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.Url).HasColumnName("url");
                builder.HasKey(x => x.TitleId);
            });
            
            modelBuilder.Entity<Rating>(builder => {
                builder.ToTable("rating");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Rate).HasColumnName("rate");
                builder.Property(x => x.Date).HasColumnName("date");
                builder.HasKey(x => new { x.TitleId, x.UserId });
                builder.HasOne(x => x.Comment).WithOne().HasForeignKey<Comment>(x => new { x.TitleId, x.UserId });
            });

            modelBuilder.Entity<Comment>(builder => {
                builder.ToTable("_comment");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Date).HasColumnName("date");
                builder.Property(x => x.Content).HasColumnName("content");
                builder.HasKey(x => new { x.TitleId, x.UserId });
            });

            modelBuilder.Entity<SearchHistory>(builder => {
                builder.ToTable("searchhistory");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Date).HasColumnName("date");
                builder.Property(x => x.SearchType).HasColumnName("searchtype");
                builder.Property(x => x.SearchedText).HasColumnName("searchedtext");
                builder.HasKey(x => new { x.UserId, x.Date });
            });

            modelBuilder.Entity<Synopsis>(builder => {
                builder.ToTable("synopsis");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.Plot).HasColumnName("plot");
                builder.HasKey(x => x.TitleId);
            });
            
            modelBuilder.Entity<TitleAka>(builder => {
                builder.ToTable("titleakas");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.OtherTitle).HasColumnName("title");
                builder.Property(x => x.CountryCode).HasColumnName("countrycode");
                builder.Property(x => x.LanguageCode).HasColumnName("languagecode");
                builder.HasKey(x => new { x.TitleId, x.OtherTitle, x.CountryCode, x.LanguageCode });
            });

            modelBuilder.Entity<TitleChild>(builder => {
                builder.ToTable("titlechild");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.ParentId).HasColumnName("parent");
                builder.HasKey(x => x.TitleId);
                builder.HasOne(x => x.TVEpisode).WithOne().HasForeignKey<TVEpisode>(x => x.TitleId);
                //builder.HasOne(x => x.Parent).WithOne().HasForeignKey<Title>(x => x.TitleId);
            });
            
            modelBuilder.Entity<TVEpisode>(builder => {
                builder.ToTable("tvepisode");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.SeasonNumber).HasColumnName("seasonnumber");
                builder.Property(x => x.EpisodeNumber).HasColumnName("episodenumber");
                builder.HasKey(x => x.TitleId);
            });

            modelBuilder.Entity<Work>(builder => {
                builder.ToTable("_work");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.PersonId).HasColumnName("nconst");
                builder.Property(x => x.Job).HasColumnName("job");
                builder.HasKey(x => new { x.TitleId, x.PersonId, x.Job });
                builder.HasOne(x => x.Acting).WithOne(x => x.Work).HasForeignKey<Acting>(x => new { x.TitleId, x.PersonId, x.Job });
            });

            modelBuilder.Entity<Acting>(builder => {
                builder.ToTable("acting");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.PersonId).HasColumnName("nconst");
                builder.Property(x => x.Job).HasColumnName("job");
                builder.Property(x => x.Playing).HasColumnName("playing");
                builder.HasKey(x => new { x.TitleId, x.PersonId, x.Job, x.Playing });
            });

            modelBuilder.Entity<PersonBookmark>(builder => {
                builder.ToTable("personbookmark");
                builder.Property(x => x.PersonId).HasColumnName("nconst");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Date).HasColumnName("date");
                builder.Property(x => x.Label).HasColumnName("label");
                builder.HasKey(x => new { x.UserId, x.PersonId });
            });

            modelBuilder.Entity<TitleBookmark>(builder => {
                builder.ToTable("titlebookmark");
                builder.Property(x => x.TitleId).HasColumnName("tconst");
                builder.Property(x => x.UserId).HasColumnName("user_id");
                builder.Property(x => x.Date).HasColumnName("date");
                builder.Property(x => x.Label).HasColumnName("label");
                builder.HasKey(x => new { x.UserId, x.TitleId });
            });

            modelBuilder.Entity<SearchTitleResult>(builder => {
                builder.HasNoKey();
                builder.Property(x => x.Id).HasColumnName("tconst");
                builder.Property(x => x.Score).HasColumnName("score");
                builder.Property(x => x.Title).HasColumnName("title");
                builder.Property(x => x.ReleaseYear).HasColumnName("releaseyear");
                builder.Property(x => x.MediaType).HasColumnName("mediatype");
                builder.Property(x => x.Adult).HasColumnName("adult");
                builder.Property(x => x.PosterUrl).HasColumnName("posterurl");
            });

            modelBuilder.Entity<SearchPersonResult>(builder => {
                builder.HasNoKey();
                builder.Property(x => x.Id).HasColumnName("nconst");
                builder.Property(x => x.Score).HasColumnName("score");
                builder.Property(x => x.FirstName).HasColumnName("firstname");
                builder.Property(x => x.LastName).HasColumnName("lastname");
                builder.Property(x => x.BirthYear).HasColumnName("birthyear");
                builder.Property(x => x.DeathYear).HasColumnName("deathyear");
            });
        }
    }
}
