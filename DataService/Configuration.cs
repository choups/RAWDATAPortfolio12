﻿using System;
using Microsoft.Extensions.Configuration;
using System.Resources;
using System.Reflection;

namespace DataService {

    public class Configuration {
        private readonly string Hostname;
        private readonly string Database;
        private readonly string Username;
        private readonly string Password;

        private static Configuration instance;

        public static Configuration Shared {
            get {
                if (instance == null)
                    instance = new Configuration();

                return instance;
            }
        }

        private Configuration() {
            try {
                var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DataService.config.json");

                var config = new ConfigurationBuilder()
                    .AddJsonStream(stream)
                    .AddEnvironmentVariables()
                    .Build();

                Hostname = config["hostname"];
                Database = config["database"];
                Username = config["username"];
                Password = config["password"];

            } catch (Exception e) {
                Console.WriteLine("You need to setup the config.json file. See README file. > " + e.Message);
            }
        }
    public string GetDatabaseString() {
            return $"host={Hostname};db={Database};uid={Username};pwd={Password};CommandTimeout={(int)TimeSpan.FromMinutes(2).TotalSeconds}";
        }
    }
}
