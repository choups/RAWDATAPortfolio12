﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace DataService.Domain {

    public class User {

        public User() {
            Ratings = new HashSet<Rating>();
            TitleBookmarks = new HashSet<TitleBookmark>();
            PersonBookmarks = new HashSet<PersonBookmark>();
            Searches = new HashSet<SearchHistory>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [StringLength(50, MinimumLength = 1)]
        public string Login { get; set; }

        [StringLength(350, MinimumLength = 10)]
        public string Password { get; set; }

        [StringLength(350, MinimumLength = 10)]
        public string Salt { get; set; }


        [NotMapped]
        public virtual ICollection<Rating> Ratings { get; set; }

        [NotMapped]
        public virtual ICollection<SearchHistory> Searches { get; set; }

        [NotMapped]
        public virtual ICollection<TitleBookmark> TitleBookmarks { get; set; }

        [NotMapped]
        public virtual ICollection<PersonBookmark> PersonBookmarks { get; set; }
    }
}
