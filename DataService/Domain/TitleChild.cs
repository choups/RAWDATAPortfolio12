﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Domain {

    public class TitleChild {

#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }

        [StringLength(10, MinimumLength = 10)]
    public string? ParentId { get; set; }
#nullable disable

        [NotMapped]
        public virtual Title Title { get; set; }

        [NotMapped]
        public virtual TVEpisode TVEpisode { get; set; }
    }
}
