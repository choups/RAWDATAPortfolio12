﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class TitleBookmark {

        public int UserId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Date { get; set; }

        [StringLength(100, MinimumLength = 1)]
        public string Label { get; set; }

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }


        [NotMapped]
        [ForeignKey("TitleId")]
        public virtual Title Title { get; set; }

        [NotMapped]
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
