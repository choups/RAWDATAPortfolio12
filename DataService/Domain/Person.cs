﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace DataService.Domain {

    public class Person {

        public Person() {
            Works = new HashSet<Work>();
        }

        [StringLength(10, MinimumLength = 10)]
        public string PersonId { get; set; }

        [StringLength(100, MinimumLength = 1)]
        public string LastName { get; set; }

        [StringLength(100, MinimumLength = 1)]
        public string FirstName { get; set; }

        [StringLength(4, MinimumLength = 4)]
        public string BirthYear { get; set; }


        [NotMapped]
        public virtual ICollection<Work> Works { get; set; }

        [NotMapped]
        public virtual Death Death { get; set; }

        [NotMapped]
        public virtual ICollection<PersonBookmark> ReferencedPersonBookmarks { get; set; }
    }
}
