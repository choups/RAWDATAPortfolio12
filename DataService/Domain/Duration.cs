﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class Duration {

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }

        public int RuntimeMinutes { get; set; }
    }
}
