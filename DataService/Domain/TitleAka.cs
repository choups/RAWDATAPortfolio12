﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Domain {

    public class TitleAka {
#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }
#nullable disable
        [StringLength(300, MinimumLength = 1)]
        public string OtherTitle { get; set; }

        [StringLength(4, MinimumLength = 1)]
        public string CountryCode { get; set; }

        [StringLength(4, MinimumLength = 1)]
        public string LanguageCode { get; set; }
    }
}
