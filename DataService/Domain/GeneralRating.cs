﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class GeneralRating {

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }

        public int NumVotes { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double AverageRating { get; set; }
    }
}
