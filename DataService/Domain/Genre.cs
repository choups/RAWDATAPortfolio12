﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class Genre {

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }

        [StringLength(50, MinimumLength = 1)]
        public string Label { get; set; }
    }
}
