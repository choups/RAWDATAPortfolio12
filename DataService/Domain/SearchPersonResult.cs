﻿using System;

namespace DataService.Domain
{
    public class SearchPersonResult
    {
        public string Id { get; set; }
        public double Score { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthYear { get; set; }
        public string DeathYear { get; set; }
    }
}
