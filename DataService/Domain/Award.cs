﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class Award {
#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }
#nullable disable
        [StringLength(100, MinimumLength = 1)]
        public string Label { get; set; }
    }
}
