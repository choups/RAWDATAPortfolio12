﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class EndedTitle {
#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }
#nullable disable
        [StringLength(4, MinimumLength = 4)]
        public string EndYear { get; set; }
    }
}
