﻿using System;

namespace DataService.Domain
{
    public class SearchTitleResult
    {
        public string Id { get; set; }
        public double Score { get; set; }
        public string Title { get; set; }
        public string ReleaseYear { get; set; }
        public string MediaType { get; set; }
        public string Adult { get; set; }
        public string PosterUrl { get; set; }
    }
}
