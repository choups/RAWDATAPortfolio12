﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace DataService.Domain {

    public class Title {

        public Title() {
            Akas = new HashSet<TitleAka>();
            Genres = new HashSet<Genre>();
            Staff = new HashSet<Work>();
            Children = new HashSet<TitleChild>();
        }

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }

        [StringLength(300, MinimumLength = 1)]
        public string OriginalTitle { get; set; }

        [StringLength(4, MinimumLength = 4)]
        public string ReleaseYear { get; set; }

        [StringLength(20, MinimumLength = 1)]
        public string MediaType { get; set; }


        [NotMapped]
        public virtual ICollection<Work> Staff { get; set; }

        [NotMapped]
        public virtual ICollection<TitleAka> Akas { get; set; }

        [NotMapped]
        public virtual ICollection<Genre> Genres { get; set; }

        [NotMapped]
        public virtual ICollection<TitleBookmark> ReferencesTitleBookmarks { get; set; }

        [NotMapped]
        public virtual ICollection<TitleChild> Children { get; set; }

        [NotMapped]
        public virtual TitleChild TitleChild { get; set; }

        [NotMapped]
        public virtual Duration Duration { get; set; }

        [NotMapped]
        public virtual GeneralRating GeneralRating { get; set; }

        [NotMapped]
        public virtual Adult Adult { get; set; }

        [NotMapped]
        public virtual Synopsis Synopsis{ get; set; }

        [NotMapped]
        public virtual Award Award { get; set; }

        [NotMapped]
        public virtual EndedTitle EndedTitle { get; set; }

        [NotMapped]
        public virtual Poster Poster { get; set; }
    }
}
