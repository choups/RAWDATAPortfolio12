﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class Acting {

#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }

        [StringLength(10, MinimumLength = 10)]
        public string? PersonId { get; set; }

        [StringLength(100, MinimumLength = 1)]
        public string? Job { get; set; }
#nullable disable

        [StringLength(200, MinimumLength = 1)]
        public string Playing { get; set; }


        [NotMapped]
        public virtual Work Work { get; set; }
    }
}
