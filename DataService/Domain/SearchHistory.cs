﻿using System;

namespace DataService.Domain {

    public class SearchHistory {

        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public char SearchType { get; set; }

        public string SearchedText { get; set; }

        public SearchHistory()
        {

        }

        public SearchHistory(int userId, DateTime date, char searchType, string searchedText)
        {
            UserId = userId;
            Date = date;
            SearchType = searchType;
            SearchedText = searchedText;
        }
    }
}
