﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Domain {

    public class TVEpisode {

#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }
#nullable disable

        public int SeasonNumber { get; set; }

        public int EpisodeNumber { get; set; }
    }
}
