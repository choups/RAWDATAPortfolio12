﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataService.Domain {

    public class Work {

#nullable enable
        [StringLength(10, MinimumLength = 10)]
        public string? TitleId { get; set; }
#nullable disable
        [StringLength(10, MinimumLength = 10)]
        public string PersonId { get; set; }

        [StringLength(100, MinimumLength = 1)]
        public string Job { get; set; }


        [NotMapped]
        public virtual Title Title { get; set; }

        [NotMapped]
        public virtual Person Person { get; set; }

        [NotMapped]
        public virtual Acting Acting { get; set; }
    }
}
