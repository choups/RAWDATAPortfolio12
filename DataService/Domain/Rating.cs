﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Domain {

    public class Rating {

        [StringLength(10, MinimumLength = 10)]
        public string TitleId { get; set; }

        public int UserId { get; set; }

        public int Rate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Date { get; set; }

        [NotMapped]
        public virtual Comment Comment { get; set; }
    }
}
