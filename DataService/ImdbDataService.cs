﻿using System;
using System.Collections.Generic;
using DataService.Domain;
using DataService.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataService {

    public class ImdbDataService: ITitleDataService, IPersonDataService
    {

        // ITitleDataService
        public (IList<Title>, int) GetPagedTitles(int CurrentPage, int PageSize) {
            // list all titles
            var source = new DataContext().Titles
                .Include(x => x.Adult)
                .Include(x => x.Poster)
                .Include(x => x.TitleChild)
                .OrderByDescending(x => x.ReleaseYear)
                .AsQueryable();

            int count = source.Count();

            // result titles after applying paging
            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            result.ForEach(title => {
                if (title.TitleChild != null) {
                    var parent = GetTitle(title.TitleChild.ParentId);
                    title.OriginalTitle = $"{parent.OriginalTitle} - {title.OriginalTitle}";

                    if (title.Poster == null && parent.Poster != null)
                        title.Poster = parent.Poster;
                }
            });

            return (result, count);
        }

        public (ICollection<SearchTitleResult>, int) GetSimilarTitles(string Id, int CurrentPage, int PageSize)
        {
            // list all similar titles
            var source = new DataContext().TitleSearchResults
                .FromSqlInterpolated($"select * from similar_movie({Id})")
                .OrderByDescending(x => x.Score)
                .AsQueryable();

            int count = source.Count();

            // return titles after applying paging
            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return (result, count);
        }

        public Title GetTitle(string Id) {
            return new DataContext().Titles
                .Include(x => x.Adult)
                .Include(x => x.Akas)
                .Include(x => x.Award)
                .Include(x => x.Duration)
                .Include(x => x.EndedTitle)
                .Include(x => x.GeneralRating)
                .Include(x => x.Genres)
                .Include(x => x.Poster)
                .Include(x => x.Staff)
                .ThenInclude(x => x.Acting)
                .Include(x => x.Synopsis)
                .Include(x => x.Children)
                .ThenInclude(x => x.Title)
                .Include(x => x.TitleChild)
                .FirstOrDefault(x => x.TitleId == Id);
        }

        // IPersonDataService
        public (IList<Person>, int) GetPagedPersons(int CurrentPage, int PageSize) {
            var source = new DataContext().Persons
                .Include(x => x.Works)
                .ThenInclude(x => x.Acting)
                .Include(x => x.Death)
                .AsQueryable();

            int count = source.Count();

            var result = source
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();
            return (result, count);
        }

        public Person GetPerson(string Id) {
            return new DataContext().Persons
                .Include(x => x.Works)
                .ThenInclude(x => x.Acting)
                .Include(x => x.Death)
                .FirstOrDefault(x => x.PersonId == Id);
        }
    }
}