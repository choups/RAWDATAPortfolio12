﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class TitleBookmarksTests {

        private const string TitleBookmarksApi = "https://localhost:5001/api/users/bookmark/title";


        /***** Get title bookmarks by user id *****/

        [Fact]
        public void Test_GetTitleBookmarksByUserId_Ok() {
            var (data, statusCode) = QueryHelper.GetObject(TitleBookmarksApi, LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(data.Value<JArray>("result"));
        }

        [Fact]
        public void Test_GetTitleBookmarksByUserId_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject(TitleBookmarksApi, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Get title bookmark *****/

        [Fact]
        public void Test_GetTitleBookmark_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{TitleBookmarksApi}/azertyuiop", LoginFirst: true);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }


        [Fact]
        public void Test_GetTitleBookmark_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject($"{TitleBookmarksApi}/tt11495656", LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Post title bookmark *****/

        [Fact]
        public void Test_PostGetDeleteTitleBookmark_Created() {
            var newTitleBookmark = new {
                TitleId = "tt0944947",
                Label = "my personal bookmark"
            };

            var (titlebookmark, statusCode) = QueryHelper.PostData(TitleBookmarksApi, newTitleBookmark, LoginFirst: true);

            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(titlebookmark.Value<string>("titleUrl"));
            Assert.Equal(newTitleBookmark.Label, titlebookmark.Value<string>("label"));

            (_, statusCode) = QueryHelper.GetObject($"{TitleBookmarksApi}/tt0944947", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            statusCode = QueryHelper.DeleteData($"{TitleBookmarksApi}/tt0944947", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.GetObject($"{TitleBookmarksApi}/tt0944947", LoginFirst: true);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_PostTitleBookmark_BadRequest() {
            var newTitleBookmark = new {};

            var (_, statusCode) = QueryHelper.PostData(TitleBookmarksApi, newTitleBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.BadRequest, statusCode);
        }

        [Fact]
        public void Test_PostTitleBookmark_Unauthorized() {
            var newTitleBookmark = new {
                TitleId = "tt10939636",
                Label = "my personal bookmark"
            };

            var (_, statusCode) = QueryHelper.PostData(TitleBookmarksApi, newTitleBookmark, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Put title bookmark *****/

        [Fact]
        public void Test_PutGetDeleteTitleBookmark_Ok() {
            var newTitleBookmark = new {
                TitleId = "tt1754826",
                Label = "my personal bookmark"
            };

            var (titleBookmark, statusCode) = QueryHelper.PostData(TitleBookmarksApi, newTitleBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(titleBookmark.Value<string>("titleUrl"));
            Assert.Equal(newTitleBookmark.Label, titleBookmark.Value<string>("label"));

            var updatedTitleBookmark = new {
                Label = "Worst show!"
            };

            statusCode = QueryHelper.PutData($"{TitleBookmarksApi}/tt1754826", updatedTitleBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (titleBookmark, statusCode) = QueryHelper.GetObject($"{TitleBookmarksApi}/tt1754826", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(updatedTitleBookmark.Label, titleBookmark.Value<string>("label"));

            statusCode = QueryHelper.DeleteData($"{TitleBookmarksApi}/tt1754826", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
        }

        [Fact]
        public void Test_PutTitleBookmark_Unauthorized() {
            var statusCode = QueryHelper.PutData($"{TitleBookmarksApi}/tt1329100", new { }, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }
    }
}
