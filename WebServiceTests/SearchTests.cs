﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class SearchTests {
        private const string SearchApi = "https://localhost:5001/api";
        private const string SearchHistoryApi = "https://localhost:5001/api/users/history";

        [Fact]
        public void Test_SearchTitleOk_SearchPersonNoContent_HistoryOk() {
            var (_, statusCode) = QueryHelper.PostData($"{SearchApi}/titles", new { StringSearch = "Game Thrones" }, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.PostData($"{SearchApi}/persons", new { StringSearch = "azerty" }, LoginFirst: true);
            Assert.Equal(HttpStatusCode.NoContent, statusCode);

            JObject data;
            (data, statusCode) = QueryHelper.GetObject($"{SearchHistoryApi}/t", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Single(data.Value<JArray>("result"));

            (data, statusCode) = QueryHelper.GetObject($"{SearchHistoryApi}/p", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Single(data.Value<JArray>("result"));

            (data, statusCode) = QueryHelper.GetObject(SearchHistoryApi, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(2, data.Value<JArray>("result").Count);

            statusCode = QueryHelper.DeleteData(SearchHistoryApi, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.GetArray(SearchHistoryApi, LoginFirst: true);
            Assert.Equal(HttpStatusCode.NoContent, statusCode);
        }
    }
}
