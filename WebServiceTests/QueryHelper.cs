﻿using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http.Headers;

namespace WebServiceTests {

    public class QueryHelper {
        private const string defaultUsername = "choups";
        private const string defaultPassword = "azertyuiopqsdfghjklmwxcvbn";

        public static (string, HttpStatusCode) Login(string Username, string Password) {
            var url = "https://localhost:5001/api/users/login";

            var (responseObject, statusCode) = PostData(url, new {
                Login = Username,
                Password
            }, LoginFirst: false);

            return (responseObject["token"]?.ToString(), statusCode);
        }

        public static (JArray, HttpStatusCode) GetArray(string url, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return (null, statusCode);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.GetAsync(url).Result;
            var data = response.Content.ReadAsStringAsync().Result;


            return ((JArray)JsonConvert.DeserializeObject(data), response.StatusCode);
        }


        public static (JArray, HttpStatusCode) GetArray(string url, object content, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return (null, statusCode);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var requestContent = new StringContent(
                JsonConvert.SerializeObject(content),
                Encoding.UTF8,
                "application/json"
            );

            var response = client.GetAsync(url).Result;
            var data = response.Content.ReadAsStringAsync().Result;


            return ((JArray)JsonConvert.DeserializeObject(data), response.StatusCode);
        }


        public static (JObject, HttpStatusCode) GetObject(string url, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return (null, statusCode);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.GetAsync(url).Result;
            var data = response.Content.ReadAsStringAsync().Result;
            return ((JObject)JsonConvert.DeserializeObject(data), response.StatusCode);
        }


        public static (JObject, HttpStatusCode) PostData(string url, object content, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();
            var requestContent = new StringContent(
                JsonConvert.SerializeObject(content),
                Encoding.UTF8,
                "application/json"
            );

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return (null, statusCode);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.PostAsync(url, requestContent).Result;
            var data = response.Content.ReadAsStringAsync().Result;

            return ((JObject)JsonConvert.DeserializeObject(data), response.StatusCode);
        }


        public static (JArray, HttpStatusCode) PostDataReturnsArray(string url, object content, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();
            var requestContent = new StringContent(
                JsonConvert.SerializeObject(content),
                Encoding.UTF8,
                "application/json"
            );

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return (null, statusCode);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.PostAsync(url, requestContent).Result;
            var data = response.Content.ReadAsStringAsync().Result;

            return ((JArray)JsonConvert.DeserializeObject(data), response.StatusCode);
        }


        public static HttpStatusCode PutData(string url, object content, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return statusCode;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.PutAsync(
                url,
                new StringContent(
                    JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json")).Result;

            return response.StatusCode;
        }


        public static HttpStatusCode DeleteData(string url, bool LoginFirst, string Username = defaultUsername, string Password = defaultPassword) {
            var client = new HttpClient();

            if (LoginFirst) {
                var (token, statusCode) = Login(Username, Password);

                if (statusCode != HttpStatusCode.OK)
                    return statusCode;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.DeleteAsync(url).Result;
            return response.StatusCode;
        }
    }
}
