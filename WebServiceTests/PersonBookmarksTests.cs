﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class PersonBookmarksTests {

        private const string PersonBookmarksApi = "https://localhost:5001/api/users/bookmark/person";


        /***** Get person bookmarks by user id *****/

        [Fact]
        public void Test_GetPersonBookmarksByUserId_Ok() {
            var (data, statusCode) = QueryHelper.GetObject(PersonBookmarksApi, LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(data.Value<JArray>("result"));

        }

        [Fact]
        public void Test_GetPersonBookmarksByUserId_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject(PersonBookmarksApi, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Get person bookmark *****/

        [Fact]
        public void Test_GetPersonBookmark_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{PersonBookmarksApi}/azertyuiop", LoginFirst: true);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }


        [Fact]
        public void Test_GetPersonBookmark_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject($"{PersonBookmarksApi}/nm0012904", LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Post person bookmark *****/

        [Fact]
        public void Test_PostGetDeletePersonBookmark_Created() {
            var newPersonBookmark = new {
                PersonId = "nm0012904",
                Label = "my personal bookmark"
            };

            var (personbookmark, statusCode) = QueryHelper.PostData(PersonBookmarksApi, newPersonBookmark, LoginFirst: true);

            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(personbookmark.Value<string>("personUrl"));
            Assert.Equal(newPersonBookmark.Label, personbookmark.Value<string>("label"));

            (_, statusCode) = QueryHelper.GetObject($"{PersonBookmarksApi}/nm0012904", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            statusCode = QueryHelper.DeleteData($"{PersonBookmarksApi}/nm0012904", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.GetObject($"{PersonBookmarksApi}/nm0012904", LoginFirst: true);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_PostPersonBookmark_BadRequest() {
            var newPersonBookmark = new {};

            var (_, statusCode) = QueryHelper.PostData(PersonBookmarksApi, newPersonBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.BadRequest, statusCode);
        }

        [Fact]
        public void Test_PostPersonBookmark_Unauthorized() {
            var newPersonBookmark = new {
                PersonId = "nm0002021",
                Label = "my personal bookmark"
            };

            var (_, statusCode) = QueryHelper.PostData(PersonBookmarksApi, newPersonBookmark, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Put person bookmark *****/

        [Fact]
        public void Test_PutGetDeletePersonBookmark_Ok() {
            var newPersonBookmark = new {
                PersonId = "nm0002021",
                Label = "my personal bookmark"
            };

            var (personBookmark, statusCode) = QueryHelper.PostData(PersonBookmarksApi, newPersonBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(personBookmark.Value<string>("personUrl"));
            Assert.Equal(newPersonBookmark.Label, personBookmark.Value<string>("label"));

            var updatedPersonBookmark = new {
                Label = "Worst show!"
            };

            statusCode = QueryHelper.PutData($"{PersonBookmarksApi}/nm0002021", updatedPersonBookmark, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (personBookmark, statusCode) = QueryHelper.GetObject($"{PersonBookmarksApi}/nm0002021", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(updatedPersonBookmark.Label, personBookmark.Value<string>("label"));

            statusCode = QueryHelper.DeleteData($"{PersonBookmarksApi}/nm0002021", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
        }

        [Fact]
        public void Test_PutPersonBookmark_Unauthorized() {
            var statusCode = QueryHelper.PutData($"{PersonBookmarksApi}/nm0002021", new { }, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }
    }
}
