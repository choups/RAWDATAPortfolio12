﻿using System;
using Xunit;
using System.Net;

namespace WebServiceTests {

    public class UserTests {
        private const string UsersApi = "https://localhost:5001/api/users";

        [Fact]
        public void Test_CreateLoginDeleteUser_Ok() {
            var newUser = new {
                Login = "azertyuiop",
                Password = "qsdfghjklm"
            };

            var (data, statusCode) = QueryHelper.PostData($"{UsersApi}/register", newUser, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.Equal(newUser.Login, data.Value<string>("login"));

            statusCode = QueryHelper.DeleteData(UsersApi, LoginFirst: true, newUser.Login, newUser.Password);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.Login(newUser.Login, newUser.Password);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }


        [Fact]
        public void Test_CreateUser_BadRequest() {
            var newUser = new {
                Login = "azertyuiop"
            };

            var (_, statusCode) = QueryHelper.PostData($"{UsersApi}/register", newUser, LoginFirst: false);

            Assert.Equal(HttpStatusCode.BadRequest, statusCode);

            statusCode = QueryHelper.DeleteData(UsersApi, LoginFirst: true, newUser.Login, "azertyuiop");
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }


        [Fact]
        public void Test_CreateUser_Conflict() {
            var newUser = new {
                Login = "choups",
                Password = "azertyuiop"
            };

            var (_, statusCode) = QueryHelper.PostData($"{UsersApi}/register", newUser, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Conflict, statusCode);
        }


        [Fact]
        public void Test_DeleteUser_Unauthorized() {
            var statusCode = QueryHelper.DeleteData(UsersApi, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        [Fact]
        public void Test_Login_Ok() {
            var user = new {
                Login = "choups",
                Password = "azertyuiopqsdfghjklmwxcvbn"
            };

            var (token, statusCode) = QueryHelper.Login(user.Login, user.Password);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(token);
        }


        [Fact]
        public void Test_Login_NotFound() {
            var user = new {
                Login = "azertyuiop",
                Password = "zertyuio"
            };

            var (token, statusCode) = QueryHelper.Login(user.Login, user.Password);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
            Assert.Null(token);
        }


        [Fact]
        public void Test_Login_Unauthorized() {
            var user = new {
                Login = "choups",
                Password = "azerty"
            };

            var (token, statusCode) = QueryHelper.Login(user.Login, user.Password);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
            Assert.Null(token);
        }

        [Fact]
        public void Test_ChangePassword_Ok() {
            var username = "jakub";
            var data = new {
                OldPassword = "azertyuiopqsdfghjklmwxcvbn",
                NewPassword = "wxcvbn"
            };

            Assert.Equal(HttpStatusCode.OK, QueryHelper.PutData($"{UsersApi}/password", data, LoginFirst: true, Username: username, Password: data.OldPassword));

            var (token, statusCode) = QueryHelper.Login(username, data.OldPassword);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
            Assert.Null(token);

            (token, statusCode) = QueryHelper.Login(username, data.NewPassword);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(token);


            // Just to reset the data
            var originalData = new {
                OldPassword = data.NewPassword,
                NewPassword = data.OldPassword
            };

            statusCode = QueryHelper.PutData($"{UsersApi}/password", originalData, LoginFirst: true, Username: username, Password: data.NewPassword);
            Assert.Equal(HttpStatusCode.OK, statusCode);
        }

        [Fact]
        public void Test_ChangePassword_Unauthorized() {
            var data = new {
                oldPassword = "azertyuiopqsdfghjklmwxcvbn",
                newPassword = "wxcvbn"
            };

            var statusCode = QueryHelper.PutData($"{UsersApi}/password", data, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }

        [Fact]
        public void Test_ChangeLogin_Ok() {
            var login = "guillaume";
            var data = new {
                NewLogin = "guillaume02",
                Password = "azertyuiopqsdfghjklmwxcvbn"
            };

            Assert.Equal(HttpStatusCode.OK, QueryHelper.PutData($"{UsersApi}/login", data, LoginFirst: true, Username: login));

            var (token, statusCode) = QueryHelper.Login(data.NewLogin, data.Password);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(token);

            (token, statusCode) = QueryHelper.Login(login, data.Password);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
            Assert.Null(token);

            // Just to reset the data
            var originalData = new {
                NewLogin = login,
                Password = data.Password
            };

            statusCode = QueryHelper.PutData($"{UsersApi}/login", originalData, LoginFirst: true, Username: data.NewLogin);
            Assert.Equal(HttpStatusCode.OK, statusCode);
        }

        [Fact]
        public void Test_ChangeLogin_Conflict() {
            var login = "guillaume";
            var data = new {
                NewLogin = "choups",
                Password = "azertyuiopqsdfghjklmwxcvbn"
            };

            var statusCode = QueryHelper.PutData($"{UsersApi}/login", data, LoginFirst: true, Username: login);
            Assert.Equal(HttpStatusCode.Conflict, statusCode);
        }

        [Fact]
        public void Test_ChangeLogin_Unauthorized() {
            var data = new {
                NewLogin = "choups",
                Password = "azertyuiopqsdfghjklmwxcvbn"
            };

            var statusCode = QueryHelper.PutData($"{UsersApi}/login", data, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }
    }
}
