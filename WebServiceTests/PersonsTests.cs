﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class PersonsTests {
        private const string PersonsApi = "https://localhost:5001/api/persons";


        [Fact]
        public void Test_Logged_GetPersonsPaging_Ok() {
            var (data, statusCode) = QueryHelper.GetObject($"{PersonsApi}?pageNumber=2&pageSize=25", LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(25, data.Value<JArray>("result").Count);
        }

        [Fact]
        public void Test_NotLogged_GetPersonsPaging_Ok() {
            var (data, statusCode) = QueryHelper.GetObject($"{PersonsApi}?pageNumber=2&pageSize=25", LoginFirst: false);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(25, data.Value<JArray>("result").Count);
        }


        [Fact]
        public void Test_Logged_GetPerson_Ok() {
            var personId = "nm0407153";
            var (person, statusCode) = QueryHelper.GetObject($"{PersonsApi}/{personId}", LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(personId, person.Value<string>("personId"));
            Assert.Equal("Iglesias", person.Value<string>("lastName"));
            Assert.Equal("Sergio", person.Value<string>("firstName"));
            Assert.Equal("1964", person.Value<string>("birthYear"));
            Assert.Equal(2, person.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_Logged_GetPerson_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{PersonsApi}/azerty", LoginFirst: true);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_NotLogged_GetPerson_Ok() {
            var personId = "nm0407153";
            var (person, statusCode) = QueryHelper.GetObject($"{PersonsApi}/{personId}", LoginFirst: false);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(personId, person.Value<string>("personId"));
            Assert.Equal("Iglesias", person.Value<string>("lastName"));
            Assert.Equal("Sergio", person.Value<string>("firstName"));
            Assert.Equal("1964", person.Value<string>("birthYear"));
            Assert.Equal(2, person.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_NotLogged_GetPerson_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{PersonsApi}/azerty", LoginFirst: false);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }
    }
}
