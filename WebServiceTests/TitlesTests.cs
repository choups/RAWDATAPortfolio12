﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class TitlesTests {
        private const string TitlesApi = "https://localhost:5001/api/titles";


        [Fact]
        public void Test_Logged_GetTitlesPaging_Ok() {
            var (data, statusCode) = QueryHelper.GetObject($"{TitlesApi}?pageNumber=2&pageSize=25", LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(25, data.Value<JArray>("result").Count);
        }

        [Fact]
        public void Test_NotLogged_GetTitlesPaging() {
            var (data, statusCode) = QueryHelper.GetObject($"{TitlesApi}?pageNumber=2&pageSize=25", LoginFirst: false);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(25, data.Value<JArray>("result").Count);
        }


        [Fact]
        public void Test_Logged_GetParent_Ok() {
            var titleId = "tt0944947";
            var (title, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}", LoginFirst: true);
            var posterUrl = "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg";
            var award = "Won 1 Golden Globe. Another 374 wins & 602 nominations.";
            var synopsis = "In the mythical continent of Westeros, several powerful families fight for control of the Seven Kingdoms. As conflict erupts in the kingdoms of men, an ancient enemy rises once again to threaten them all. Meanwhile, the last heirs of a recently usurped dynasty plot to take back their homeland from across the Narrow Sea.";

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(title.Value<string>("url"));
            Assert.Equal("Game of Thrones", title.Value<string>("originalTitle"));
            Assert.Equal("2011", title.Value<string>("releaseYear"));
            Assert.False(title.Value<bool>("isAdult"));
            Assert.Equal("2019", title.Value<string>("endYear"));
            Assert.Equal(posterUrl, title.Value<string>("posterUrl"));
            Assert.Equal(award, title.Value<string>("award"));
            Assert.Equal(synopsis, title.Value<string>("synopsis"));
            Assert.Equal(57, title.Value<int>("runtimeMinutes"));
            Assert.Equal(9.3, Math.Round(title.Value<double>("generalRating"), 1));
            Assert.Equal("tvSeries", title.Value<string>("mediaType"));
            Assert.Null(title.Value<string>("parent"));
            Assert.Equal(3, title.Value<JArray>("genres").Count);
            Assert.Equal(73, title.Value<JArray>("children").Count);
            Assert.Equal(37, title.Value<JArray>("titleAkas").Count);
            Assert.Equal(36, title.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_Logged_GetChild_Ok() {
            var titleId = "tt6027920";
            var (title, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}", LoginFirst: true);
            var posterUrl = "https://m.media-amazon.com/images/M/MV5BMzJkZWU5MTEtYmIwMi00M2VhLWI0MjQtYzFiZDc1YzgwNjFlXkEyXkFqcGdeQXVyMTExNDQ2MTI@._V1_SX300.jpg";
            var synopsis = "In the aftermath of the devastating attack on King''s Landing, Daenerys must face the survivors.";

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(title.Value<string>("url"));
            Assert.Equal("The Iron Throne", title.Value<string>("originalTitle"));
            Assert.Equal("2019", title.Value<string>("releaseYear"));
            Assert.False(title.Value<bool>("isAdult"));
            Assert.Null(title.Value<string>("endYear"));
            Assert.Equal(posterUrl, title.Value<string>("posterUrl"));
            Assert.Null(title.Value<string>("award"));
            Assert.Equal(synopsis, title.Value<string>("synopsis"));
            Assert.Equal(80, title.Value<int>("runtimeMinutes"));
            Assert.Equal(4.1, Math.Round(title.Value<double>("generalRating"), 1));
            Assert.Equal("tvEpisode", title.Value<string>("mediaType"));
            Assert.NotNull(title.Value<string>("parent"));
            Assert.Equal(3, title.Value<JArray>("genres").Count);
            Assert.Empty(title.Value<JArray>("children"));
            Assert.Empty(title.Value<JArray>("titleAkas"));
            Assert.Equal(12, title.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_Logged_GetTitle_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{TitlesApi}/azerty", LoginFirst: true);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_NotLogged_GetParent_Ok() {
            var titleId = "tt0944947";
            var (title, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}", LoginFirst: false);
            var posterUrl = "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg";
            var award = "Won 1 Golden Globe. Another 374 wins & 602 nominations.";
            var synopsis = "In the mythical continent of Westeros, several powerful families fight for control of the Seven Kingdoms. As conflict erupts in the kingdoms of men, an ancient enemy rises once again to threaten them all. Meanwhile, the last heirs of a recently usurped dynasty plot to take back their homeland from across the Narrow Sea.";

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(title.Value<string>("url"));
            Assert.Equal("Game of Thrones", title.Value<string>("originalTitle"));
            Assert.Equal("2011", title.Value<string>("releaseYear"));
            Assert.Equal("2019", title.Value<string>("endYear"));
            Assert.False(title.Value<bool>("isAdult"));
            Assert.Equal(posterUrl, title.Value<string>("posterUrl"));
            Assert.Equal(award, title.Value<string>("award"));
            Assert.Equal(synopsis, title.Value<string>("synopsis"));
            Assert.Equal(57, title.Value<int>("runtimeMinutes"));
            Assert.Equal(9.3, Math.Round(title.Value<double>("generalRating"), 1));
            Assert.Equal("tvSeries", title.Value<string>("mediaType"));
            Assert.Null(title.Value<string>("parent"));
            Assert.Equal(3, title.Value<JArray>("genres").Count);
            Assert.Equal(73, title.Value<JArray>("children").Count);
            Assert.Equal(37, title.Value<JArray>("titleAkas").Count);
            Assert.Equal(36, title.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_NotLogged_GetChild_Ok() {
            var titleId = "tt6027920";
            var (title, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}", LoginFirst: false);
            var posterUrl = "https://m.media-amazon.com/images/M/MV5BMzJkZWU5MTEtYmIwMi00M2VhLWI0MjQtYzFiZDc1YzgwNjFlXkEyXkFqcGdeQXVyMTExNDQ2MTI@._V1_SX300.jpg";
            var synopsis = "In the aftermath of the devastating attack on King''s Landing, Daenerys must face the survivors.";

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(title.Value<string>("url"));
            Assert.Equal("The Iron Throne", title.Value<string>("originalTitle"));
            Assert.Equal("2019", title.Value<string>("releaseYear"));
            Assert.False(title.Value<bool>("isAdult"));
            Assert.Null(title.Value<string>("endYear"));
            Assert.Equal(posterUrl, title.Value<string>("posterUrl"));
            Assert.Null(title.Value<string>("award"));
            Assert.Equal(synopsis, title.Value<string>("synopsis"));
            Assert.Equal(80, title.Value<int>("runtimeMinutes"));
            Assert.Equal(4.1, Math.Round(title.Value<double>("generalRating"), 1));
            Assert.Equal("tvEpisode", title.Value<string>("mediaType"));
            Assert.NotNull(title.Value<string>("parent"));
            Assert.Equal(3, title.Value<JArray>("genres").Count);
            Assert.Empty(title.Value<JArray>("children"));
            Assert.Empty(title.Value<JArray>("titleAkas"));
            Assert.Equal(12, title.Value<JArray>("works").Count);
        }

        [Fact]
        public void Test_NotLogged_GetTitle_NotFound() {
            var (_, statusCode) = QueryHelper.GetObject($"{TitlesApi}/azerty", LoginFirst: false);

            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }


        [Fact]
        public void Test_GetSimilarTitles_Ok() {
            var titleId = "tt0650998";
            var (data, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}/similar", LoginFirst: false);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(55075, data.Value<int>("totalResults"));

            var titles = data.Value<JArray>("result");
            Assert.Equal(25, titles.Count);
            Assert.Equal("The Naked Ant", titles.First.Value<string>("title"));
            Assert.NotNull(titles.First.Value<string>("url"));
        }


        [Fact]
        public void Test_GetSimilarTitles_NotFound() {
            var titleId = "azerty";
            var (_, statusCode) = QueryHelper.GetObject($"{TitlesApi}/{titleId}/similar", LoginFirst: false);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }
    }
}
