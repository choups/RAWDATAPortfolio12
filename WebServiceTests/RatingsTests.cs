﻿using System;
using Xunit;
using System.Net;
using Newtonsoft.Json.Linq;

namespace WebServiceTests {

    public class RatingsTests {

        private const string RatingsApi = "https://localhost:5001/api/users/rating";
        private const string TitleRatingsApi = "https://localhost:5001/api/titles";


        /***** Get rating by user id *****/

        [Fact]
        public void Test_GetRatingByUserId_Ok() {
            var (data, statusCode) = QueryHelper.GetObject(RatingsApi, LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(data.Value<JArray>("result"));
        }

        [Fact]
        public void Test_GetRatingsByUserId_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject(RatingsApi, LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Get rating by title id *****/

        [Fact]
        public void Test_GetRatingsByTitleId_Ok() {
            var (data, statusCode) = QueryHelper.GetObject($"{TitleRatingsApi}/tt7293342/rating", LoginFirst: true);

            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.NotNull(data.Value<JArray>("result"));
        }


        /***** Get rating *****/

        [Fact]
        public void Test_GetRating_Unauthorized() {
            var (_, statusCode) = QueryHelper.GetObject($"{RatingsApi}/tt9914644", LoginFirst: false);

            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Post rating *****/

        [Fact]
        public void Test_PostGetDeleteRating_Ok() {
            var newRating = new {
                TitleId = "tt9916380",
                Rate = 4,
                Comment = "Great show!"
            };

            var (rating, statusCode) = QueryHelper.PostData(RatingsApi, newRating, LoginFirst: true);

            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(rating.Value<string>("titleUrl"));
            Assert.Equal(newRating.Rate, rating.Value<int>("rate"));
            Assert.Equal(newRating.Comment, rating.Value<string>("comment"));

            (_, statusCode) = QueryHelper.GetObject($"{RatingsApi}/tt9916380", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            statusCode = QueryHelper.DeleteData($"{RatingsApi}/tt9916380", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.GetObject($"{RatingsApi}/tt9916380", LoginFirst: true);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_PostRating_BadRequest() {
            var newRating = new {
                TitleId = "tt9914642",
                Comment = "Great show!"
            };

            var (_, statusCode) = QueryHelper.PostData(RatingsApi, newRating, LoginFirst: true);
            Assert.Equal(HttpStatusCode.BadRequest, statusCode);
        }

        [Fact]
        public void Test_PostRating_Unauthorized() {
            var newRating = new {
                TitleId = "tt9915674",
                Rate = 4,
                Comment = "Great show!"
            };

            var (_, statusCode) = QueryHelper.PostData(RatingsApi, newRating, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }


        /***** Put rating *****/

        [Fact]
        public void Test_PutGetDeleteRating_Ok() {
            var newRating = new {
                TitleId = "tt0944947",
                Rate = 4,
                Comment = "Great show!"
            };

            var (rating, statusCode) = QueryHelper.PostData(RatingsApi, newRating, LoginFirst: true);
            Assert.Equal(HttpStatusCode.Created, statusCode);
            Assert.NotNull(rating.Value<string>("titleUrl"));
            Assert.Equal(newRating.Rate, rating.Value<int>("rate"));
            Assert.Equal(newRating.Comment, rating.Value<string>("comment"));

            var updatedRating = new {
                Rate = 1,
                Comment = "Worst show!"
            };

            statusCode = QueryHelper.PutData($"{RatingsApi}/tt0944947", updatedRating, LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (rating, statusCode) = QueryHelper.GetObject($"{RatingsApi}/tt0944947", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);
            Assert.Equal(updatedRating.Rate, rating.Value<int>("rate"));
            Assert.Equal(updatedRating.Comment, rating.Value<string>("comment"));

            statusCode = QueryHelper.DeleteData($"{RatingsApi}/tt0944947", LoginFirst: true);
            Assert.Equal(HttpStatusCode.OK, statusCode);

            (_, statusCode) = QueryHelper.GetObject($"{RatingsApi}/tt9916380", LoginFirst: true);
            Assert.Equal(HttpStatusCode.NotFound, statusCode);
        }

        [Fact]
        public void Test_PutRating_Unauthorized() {
            var statusCode = QueryHelper.PutData($"{RatingsApi}/tt0944947", new { }, LoginFirst: false);
            Assert.Equal(HttpStatusCode.Unauthorized, statusCode);
        }
    }
}
